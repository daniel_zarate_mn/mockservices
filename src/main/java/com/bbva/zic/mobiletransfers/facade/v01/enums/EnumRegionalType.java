package com.bbva.zic.mobiletransfers.facade.v01.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumRegionalType {

	REGIONAL("X"), TERRITORIAL("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumRegionalType.class);

	private String key;

	EnumRegionalType(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumRegionalType getEnumValue(final String key) {
		for (final EnumRegionalType regionalType : values()) {
			if (regionalType.key.equals(key)) {
				return regionalType;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumRegionalType regionalType : values()) {
			if (regionalType.name().equalsIgnoreCase(key)) {
				return regionalType.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}
}
