package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumAgileOperationType;

@XmlRootElement(name = "listAgileOperationsIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "listAgileOperationsIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoListAgileOperationsIn {

	@NotNull
	private DtoPagination pagination;

	@NotNull
	private EnumAgileOperationType agileOperationType;

	@NotNull
	private String codigoFuncion;

	public DtoPagination getPagination() {
		return pagination;
	}

	public void setPagination(final DtoPagination pagination) {
		this.pagination = pagination;
	}

	public EnumAgileOperationType getAgileOperationType() {
		return agileOperationType;
	}

	public void setAgileOperationType(final EnumAgileOperationType agileOperationType) {
		this.agileOperationType = agileOperationType;
	}

	public String getCodigoFuncion() {
		return codigoFuncion;
	}

	public void setCodigoFuncion(final String codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}

}
