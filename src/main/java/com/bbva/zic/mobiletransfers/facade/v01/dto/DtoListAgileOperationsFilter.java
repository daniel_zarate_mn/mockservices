package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "listAgileOperationsFilter", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "listAgileOperationsFilter", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoListAgileOperationsFilter {

	@NotNull
	private String agileOperationType;

	@NotNull
	private String businessFlow;

	public String getAgileOperationType() {
		return agileOperationType;
	}

	public void setAgileOperationType(final String agileOperationType) {
		this.agileOperationType = agileOperationType;
	}

	public String getBusinessFlow() {
		return businessFlow;
	}

	public void setBusinessFlow(final String businessFlow) {
		this.businessFlow = businessFlow;
	}

}
