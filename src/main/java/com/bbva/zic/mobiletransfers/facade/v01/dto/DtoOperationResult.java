package com.bbva.zic.mobiletransfers.facade.v01.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumInternationalStatus;

@XmlRootElement(name = "operationResult", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "operationResult", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoOperationResult {

	@NotNull
	private Date operationDate;

	@NotNull
	private Date executionDate;

	@NotNull
	private Date expirationDate;

	@NotNull
	private String id;

	@NotNull
	private DtoErrorInfo errorInfo;

	@NotNull
	private DtoBalance finalBalanceSender;

	@NotNull
	private DtoBalance finalBalanceReceiver;

	@NotNull
	private DtoAuthorizationInfo authorizationInfo;

	@NotNull
	private DtoAdditionalInfoTransfer additionalInfo;

	@NotNull
	private DtoEntityId entityId;

	@NotNull
	private EnumInternationalStatus internationalStatus;

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(final Date operationDate) {
		this.operationDate = operationDate;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(final Date executionDate) {
		this.executionDate = executionDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(final Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public DtoErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(final DtoErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public DtoBalance getFinalBalanceSender() {
		return finalBalanceSender;
	}

	public void setFinalBalanceSender(final DtoBalance finalBalanceSender) {
		this.finalBalanceSender = finalBalanceSender;
	}

	public DtoBalance getFinalBalanceReceiver() {
		return finalBalanceReceiver;
	}

	public void setFinalBalanceReceiver(final DtoBalance finalBalanceReceiver) {
		this.finalBalanceReceiver = finalBalanceReceiver;
	}

	public DtoAuthorizationInfo getAuthorizationInfo() {
		return authorizationInfo;
	}

	public void setAuthorizationInfo(final DtoAuthorizationInfo authorizationInfo) {
		this.authorizationInfo = authorizationInfo;
	}

	public DtoAdditionalInfoTransfer getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(final DtoAdditionalInfoTransfer additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public DtoEntityId getEntityId() {
		return entityId;
	}

	public void setEntityId(final DtoEntityId entityId) {
		this.entityId = entityId;
	}

	public EnumInternationalStatus getInternationalStatus() {
		return internationalStatus;
	}

	public void setInternationalStatus(final EnumInternationalStatus internationalStatus) {
		this.internationalStatus = internationalStatus;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("operationDate", operationDate).append("executionDate", executionDate)
				.append("expirationDate", expirationDate).append("id", id).append("errorInfo", errorInfo)
				.append("finalBalanceSender", finalBalanceSender).append("finalBalanceReceiver", finalBalanceReceiver)
				.append("authorizationInfo", authorizationInfo).append("additionalInfo", additionalInfo).append("entityId", entityId)
				.append("internationalStatus", internationalStatus).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoOperationResult)) {
			return false;
		}
		final DtoOperationResult castOther = (DtoOperationResult) other;
		return new EqualsBuilder().append(operationDate, castOther.operationDate).append(executionDate, castOther.executionDate)
				.append(expirationDate, castOther.expirationDate).append(id, castOther.id).append(errorInfo, castOther.errorInfo)
				.append(finalBalanceSender, castOther.finalBalanceSender).append(finalBalanceReceiver, castOther.finalBalanceReceiver)
				.append(authorizationInfo, castOther.authorizationInfo).append(additionalInfo, castOther.additionalInfo)
				.append(entityId, castOther.entityId).append(internationalStatus, castOther.internationalStatus).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(operationDate).append(executionDate).append(expirationDate).append(id).append(errorInfo)
				.append(finalBalanceSender).append(finalBalanceReceiver).append(authorizationInfo).append(additionalInfo).append(entityId)
				.append(internationalStatus).toHashCode();
	}

}
