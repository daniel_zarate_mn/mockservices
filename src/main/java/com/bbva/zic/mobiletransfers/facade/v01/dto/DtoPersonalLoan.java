package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbva.zic.mobiletransfers.facade.v01.ILoan;

@XmlRootElement(name = "personalLoan", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "personalLoan", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoPersonalLoan implements ILoan {

	@NotNull
	private DtoProductBase productBase;

	@NotNull
	private String id;

	@NotNull
	private String accountNumber;

	@NotNull
	private String shortName;

	public String getId() {
		return id;
	}

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoPersonalLoan)) {
			return false;
		}
		final DtoPersonalLoan castOther = (DtoPersonalLoan) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(accountNumber, castOther.accountNumber).append(shortName, castOther.shortName).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(accountNumber).append(shortName).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("accountNumber", accountNumber)
				.append("shortName", shortName).toString();
	}

}
