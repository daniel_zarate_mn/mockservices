package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@XmlRootElement(name = "refund", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "refund", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoRefund {

	@NotNull
	private String reason;

	@NotNull
	private String description;

	public String getReason() {

		return reason;
	}

	public void setReason(final String reason) {

		this.reason = reason;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(final String description) {

		this.description = description;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoRefund)) {
			return false;
		}
		final DtoRefund castOther = (DtoRefund) other;
		return new EqualsBuilder().append(reason, castOther.reason).append(description, castOther.description).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(reason).append(description).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("reason", reason).append("description", description).toString();
	}

}
