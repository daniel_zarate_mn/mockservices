package com.bbva.zic.mobiletransfers.facade.v01.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@XmlRootElement(name = "cancelInternalTransferIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "cancelInternalTransferIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoCancelInternalTransferIn {

	@NotNull
	private Date fechaAplicacion;

	@NotNull
	private String folioOperacion;

	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(final Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	public String getFolioOperacion() {
		return folioOperacion;
	}

	public void setFolioOperacion(final String folioOperacion) {
		this.folioOperacion = folioOperacion;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoCancelInternalTransferIn)) {
			return false;
		}
		final DtoCancelInternalTransferIn castOther = (DtoCancelInternalTransferIn) other;
		return new EqualsBuilder().append(fechaAplicacion, castOther.fechaAplicacion).append(folioOperacion, castOther.folioOperacion)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(fechaAplicacion).append(folioOperacion).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("fechaAplicacion", fechaAplicacion).append("folioOperacion", folioOperacion).toString();
	}

}
