package com.bbva.zic.mobiletransfers.facade.v01.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@XmlRootElement(name = "agileOperations", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "agileOperations", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAgileOperations {

	@NotNull
	private List<DtoAgileOperation> listAgileOperation;

	public List<DtoAgileOperation> getListAgileOperation() {
		return listAgileOperation;
	}

	public void setListAgileOperation(final List<DtoAgileOperation> listAgileOperation) {
		this.listAgileOperation = listAgileOperation;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoAgileOperations)) {
			return false;
		}
		final DtoAgileOperations castOther = (DtoAgileOperations) other;
		return new EqualsBuilder().append(listAgileOperation, castOther.listAgileOperation).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(listAgileOperation).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("listAgileOperation", listAgileOperation).toString();
	}

}
