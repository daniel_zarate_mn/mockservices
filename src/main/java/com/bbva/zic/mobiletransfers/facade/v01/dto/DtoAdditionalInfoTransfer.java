package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@XmlRootElement(name = "additionalInfoTransfer", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "additionalInfoTransfer", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAdditionalInfoTransfer {

	@NotNull
	private DtoCommision commisionWithdrawalCash;

	@NotNull
	private Integer percentCommissionWithdrawal;

	@NotNull
	private DtoMoney chargeAmount;

	@NotNull
	private DtoMoney depositAmount;

	@NotNull
	private String extractNumber;

	@NotNull
	private String movementNumber;

	@NotNull
	private String cancellationReference;

	public DtoCommision getCommisionWithdrawalCash() {
		return commisionWithdrawalCash;
	}

	public void setCommisionWithdrawalCash(final DtoCommision commisionWithdrawalCash) {
		this.commisionWithdrawalCash = commisionWithdrawalCash;
	}

	public Integer getPercentCommissionWithdrawal() {
		return percentCommissionWithdrawal;
	}

	public void setPercentCommissionWithdrawal(final Integer percentCommissionWithdrawal) {
		this.percentCommissionWithdrawal = percentCommissionWithdrawal;
	}

	public DtoMoney getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(final DtoMoney chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public DtoMoney getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(final DtoMoney depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getExtractNumber() {
		return extractNumber;
	}

	public void setExtractNumber(final String extractNumber) {
		this.extractNumber = extractNumber;
	}

	public String getMovementNumber() {
		return movementNumber;
	}

	public void setMovementNumber(final String movementNumber) {
		this.movementNumber = movementNumber;
	}

	public String getCancellationReference() {
		return cancellationReference;
	}

	public void setCancellationReference(final String cancellationReference) {
		this.cancellationReference = cancellationReference;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoAdditionalInfoTransfer)) {
			return false;
		}
		final DtoAdditionalInfoTransfer castOther = (DtoAdditionalInfoTransfer) other;
		return new EqualsBuilder().append(commisionWithdrawalCash, castOther.commisionWithdrawalCash)
				.append(percentCommissionWithdrawal, castOther.percentCommissionWithdrawal).append(chargeAmount, castOther.chargeAmount)
				.append(depositAmount, castOther.depositAmount).append(extractNumber, castOther.extractNumber)
				.append(movementNumber, castOther.movementNumber).append(cancellationReference, castOther.cancellationReference).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(commisionWithdrawalCash).append(percentCommissionWithdrawal).append(chargeAmount)
				.append(depositAmount).append(extractNumber).append(movementNumber).append(cancellationReference).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("commisionWithdrawalCash", commisionWithdrawalCash)
				.append("percentCommissionWithdrawal", percentCommissionWithdrawal).append("chargeAmount", chargeAmount)
				.append("depositAmount", depositAmount).append("extractNumber", extractNumber).append("movementNumber", movementNumber)
				.append("cancellationReference", cancellationReference).toString();
	}

}
