package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumRegionalType;

@XmlRootElement(name = "branch", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "branch", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoBranch {

	@NotNull
	private String id;

	@NotNull
	private String name;

	@NotNull
	private EnumRegionalType type;

	@NotNull
	private DtoBank bank;

	@NotNull
	private DtoRegion region;

	public String getId() {

		return id;
	}

	public void setId(final String id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(final String name) {

		this.name = name;
	}

	public EnumRegionalType getType() {

		return type;
	}

	public void setType(final EnumRegionalType type) {

		this.type = type;
	}

	public DtoBank getBank() {

		return bank;
	}

	public void setBank(final DtoBank bank) {

		this.bank = bank;
	}

	public DtoRegion getRegion() {

		return region;
	}

	public void setRegion(final DtoRegion region) {

		this.region = region;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoBranch)) {
			return false;
		}
		final DtoBranch castOther = (DtoBranch) other;
		return new EqualsBuilder().append(id, castOther.id).append(name, castOther.name).append(type, castOther.type)
				.append(bank, castOther.bank).append(region, castOther.region).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(name).append(type).append(bank).append(region).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("type", type).append("bank", bank)
				.append("region", region).toString();
	}

}
