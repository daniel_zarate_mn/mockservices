package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@XmlRootElement(name = "agileOperationPayment", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "agileOperationPayment", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAgileOperationPayment {

	@NotNull
	private String corporationName;

	@NotNull
	private DtoReceiver receiver;

	@NotNull
	private DtoSender sender;

	public String getCorporationName() {
		return corporationName;
	}

	public void setCorporationName(final String corporationName) {
		this.corporationName = corporationName;
	}

	public DtoReceiver getReceiver() {
		return receiver;
	}

	public void setReceiver(final DtoReceiver receiver) {
		this.receiver = receiver;
	}

	public DtoSender getSender() {
		return sender;
	}

	public void setSender(final DtoSender sender) {
		this.sender = sender;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoAgileOperationPayment)) {
			return false;
		}
		final DtoAgileOperationPayment castOther = (DtoAgileOperationPayment) other;
		return new EqualsBuilder().append(corporationName, castOther.corporationName).append(receiver, castOther.receiver)
				.append(sender, castOther.sender).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corporationName).append(receiver).append(sender).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corporationName", corporationName).append("receiver", receiver).append("sender", sender)
				.toString();
	}

}
