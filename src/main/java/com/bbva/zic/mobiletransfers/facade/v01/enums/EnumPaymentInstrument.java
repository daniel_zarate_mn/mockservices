package com.bbva.zic.mobiletransfers.facade.v01.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumPaymentInstrument {

	CELULAR("10"), CUENTA_CLABE("40"), TARJETA_DEBITO("03"), TARJETA_CREDITO("TC"), TARJETA_PREPAGO("TP"), CONVENIO_CIE("CV"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumPaymentInstrument.class);
	private String key;

	EnumPaymentInstrument(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumPaymentInstrument getEnumValue(final String key) {
		for (final EnumPaymentInstrument paymentInstrument : values()) {
			if (paymentInstrument.key.equals(key)) {
				return paymentInstrument;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumPaymentInstrument paymentInstrument : values()) {
			if (paymentInstrument.name().equalsIgnoreCase(key)) {
				return paymentInstrument.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
