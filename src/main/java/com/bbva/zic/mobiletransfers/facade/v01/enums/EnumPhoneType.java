package com.bbva.zic.mobiletransfers.facade.v01.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumPhoneType {

	HOMEPHONE("X"), OFFICE("X"), MOBILE("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumPhoneType.class);

	private String key;

	EnumPhoneType(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumPhoneType getEnumValue(final String key) {
		for (final EnumPhoneType phoneType : values()) {
			if (phoneType.key.equals(key)) {
				return phoneType;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumPhoneType phoneType : values()) {
			if (phoneType.name().equalsIgnoreCase(key)) {
				return phoneType.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
