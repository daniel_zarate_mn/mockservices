package com.bbva.zic.mobiletransfers.facade.v01.enums;

public enum EnumCurrencyCode {

	MXP, USD, EUR

}
