package com.bbva.zic.mobiletransfers.facade.v01.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbva.zic.mobiletransfers.facade.v01.IProduct;
import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumFormat;
import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumStatus;
import com.medianetsoftware.dto.servicepayments.Product;

@XmlRootElement(name = "contract", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "contract", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoContract {

	@NotNull
	private String contractNumber;

//	@XmlJavaTypeAdapter(IProductAdapter.class)
//	@NotNull
//	private IProduct product;

	@NotNull
	private Product product;
	
	@NotNull
	private DtoCountry country;

	@NotNull
	private String marketerBank;

	@NotNull
	private DtoBranch branch;

	@NotNull
	private DtoContract parentContract;

	@NotNull
	private String joinType;

	@NotNull
	private DtoAddress address;

	@NotNull
	private String conditions;

	@NotNull
	private String description;

	@NotNull
	private DtoCurrency currency;

	@NotNull
	private EnumStatus status;

	@NotNull
	private EnumFormat format;

	@NotNull
	private Date openingDate;

	@NotNull
	private Date cancelationDate;

	@NotNull
	private Date expirationDate;

	@NotNull
	private List<String> documents;

	@NotNull
	private List<String> participants;

	@NotNull
	private String userCustomization;

	@NotNull
	private List<String> relatedContracts;

	@NotNull
	private Date operationDate;

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(final String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(final Product product) {
		this.product = product;
	}

	public DtoCountry getCountry() {
		return country;
	}

	public void setCountry(final DtoCountry country) {
		this.country = country;
	}

	public String getMarketerBank() {
		return marketerBank;
	}

	public void setMarketerBank(final String marketerBank) {
		this.marketerBank = marketerBank;
	}

	public DtoBranch getBranch() {
		return branch;
	}

	public void setBranch(final DtoBranch branch) {
		this.branch = branch;
	}

	public DtoContract getParentContract() {
		return parentContract;
	}

	public void setParentContract(final DtoContract parentContract) {
		this.parentContract = parentContract;
	}

	public String getJoinType() {
		return joinType;
	}

	public void setJoinType(final String joinType) {
		this.joinType = joinType;
	}

	public DtoAddress getAddress() {
		return address;
	}

	public void setAddress(final DtoAddress address) {
		this.address = address;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(final String conditions) {
		this.conditions = conditions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public DtoCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(final DtoCurrency currency) {
		this.currency = currency;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(final EnumStatus status) {
		this.status = status;
	}

	public EnumFormat getFormat() {
		return format;
	}

	public void setFormat(final EnumFormat format) {
		this.format = format;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(final Date openingDate) {
		this.openingDate = openingDate;
	}

	public Date getCancelationDate() {
		return cancelationDate;
	}

	public void setCancelationDate(final Date cancelationDate) {
		this.cancelationDate = cancelationDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(final Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public List<String> getDocuments() {
		return documents;
	}

	public void setDocuments(final List<String> documents) {
		this.documents = documents;
	}

	public List<String> getParticipants() {
		return participants;
	}

	public void setParticipants(final List<String> participants) {
		this.participants = participants;
	}

	public String getUserCustomization() {
		return userCustomization;
	}

	public void setUserCustomization(final String userCustomization) {
		this.userCustomization = userCustomization;
	}

	public List<String> getRelatedContracts() {
		return relatedContracts;
	}

	public void setRelatedContracts(final List<String> relatedContracts) {
		this.relatedContracts = relatedContracts;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(final Date operationDate) {
		this.operationDate = operationDate;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoContract)) {
			return false;
		}
		final DtoContract castOther = (DtoContract) other;
		return new EqualsBuilder().append(contractNumber, castOther.contractNumber).append(product, castOther.product)
				.append(country, castOther.country).append(marketerBank, castOther.marketerBank).append(branch, castOther.branch)
				.append(parentContract, castOther.parentContract).append(joinType, castOther.joinType).append(address, castOther.address)
				.append(conditions, castOther.conditions).append(description, castOther.description).append(currency, castOther.currency)
				.append(status, castOther.status).append(format, castOther.format).append(openingDate, castOther.openingDate)
				.append(cancelationDate, castOther.cancelationDate).append(expirationDate, castOther.expirationDate)
				.append(documents, castOther.documents).append(participants, castOther.participants)
				.append(userCustomization, castOther.userCustomization).append(relatedContracts, castOther.relatedContracts)
				.append(operationDate, castOther.operationDate).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(contractNumber).append(product).append(country).append(marketerBank).append(branch)
				.append(parentContract).append(joinType).append(address).append(conditions).append(description).append(currency)
				.append(status).append(format).append(openingDate).append(cancelationDate).append(expirationDate).append(documents)
				.append(participants).append(userCustomization).append(relatedContracts).append(operationDate).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("contractNumber", contractNumber).append("product", product).append("country", country)
				.append("marketerBank", marketerBank).append("branch", branch).append("parentContract", parentContract)
				.append("joinType", joinType).append("address", address).append("conditions", conditions)
				.append("description", description).append("currency", currency).append("status", status).append("format", format)
				.append("openingDate", openingDate).append("cancelationDate", cancelationDate).append("expirationDate", expirationDate)
				.append("documents", documents).append("participants", participants).append("userCustomization", userCustomization)
				.append("relatedContracts", relatedContracts).append("operationDate", operationDate).toString();
	}

}
