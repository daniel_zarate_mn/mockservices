package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbva.zic.mobiletransfers.facade.v01.IAccount;

@XmlRootElement(name = "mobileAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "mobileAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoMobileAccount implements IAccount {

	@NotNull
	private DtoProductBase productBase;

	@NotNull
	private String id;

	@NotNull
	private String shortName;

	@NotNull
	private DtoCellphoneCompany cellphoneCompany;

	@NotNull
	private String cellphoneNumber;

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public DtoCellphoneCompany getCellphoneCompany() {
		return cellphoneCompany;
	}

	public void setCellphoneCompany(final DtoCellphoneCompany cellphoneCompany) {
		this.cellphoneCompany = cellphoneCompany;
	}

	public String getCellphoneNumber() {
		return cellphoneNumber;
	}

	public void setCellphoneNumber(final String cellphoneNumber) {
		this.cellphoneNumber = cellphoneNumber;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoMobileAccount)) {
			return false;
		}
		final DtoMobileAccount castOther = (DtoMobileAccount) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(shortName, castOther.shortName).append(cellphoneCompany, castOther.cellphoneCompany)
				.append(cellphoneNumber, castOther.cellphoneNumber).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(shortName).append(cellphoneCompany).append(cellphoneNumber)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("shortName", shortName)
				.append("cellphoneCompany", cellphoneCompany).append("cellphoneNumber", cellphoneNumber).toString();
	}

}
