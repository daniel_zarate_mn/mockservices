package com.bbva.zic.mobiletransfers.facade.v01.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@XmlRootElement(name = "transfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "transfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoTransfers {

	@NotNull
	private List<DtoTransfer> listTransfer;

	@NotNull
	private DtoPagination pagination;

	public List<DtoTransfer> getListTransfer() {
		return listTransfer;
	}

	public void setListTransfer(final List<DtoTransfer> listTransfer) {
		this.listTransfer = listTransfer;
	}

	public void setPagination(final DtoPagination pagination) {
		this.pagination = pagination;
	}

	public DtoPagination getPagination() {
		return pagination;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoTransfers)) {
			return false;
		}
		final DtoTransfers castOther = (DtoTransfers) other;
		return new EqualsBuilder().append(listTransfer, castOther.listTransfer).append(pagination, castOther.pagination).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(listTransfer).append(pagination).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("listTransfer", listTransfer).append("pagination", pagination).toString();
	}

}
