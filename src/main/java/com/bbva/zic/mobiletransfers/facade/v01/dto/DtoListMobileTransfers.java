package com.bbva.zic.mobiletransfers.facade.v01.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author IEUser
 *
 */

@XmlRootElement(name = "listMobileTransfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "listMobileTransfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoListMobileTransfers {

	@NotNull
	private String transferStatus;

	/**
	 * @return the transferStatus
	 */
	public String getTransferStatus() {
		return transferStatus;
	}

	/**
	 * @param transferStatus
	 *            the transferStatus to set
	 */
	public void setTransferStatus(final String transferStatus) {
		this.transferStatus = transferStatus;
	}
}
