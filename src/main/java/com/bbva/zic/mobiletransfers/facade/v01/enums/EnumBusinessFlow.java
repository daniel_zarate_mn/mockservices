package com.bbva.zic.mobiletransfers.facade.v01.enums;

public enum EnumBusinessFlow {

	INTERNATIONAL, INTERBANK, MOBILEMONEY, THIRD_PARTY, MY_ACCOUNTS, SERVICEPAYMENT, EXPRESS, PREPAID;

}
