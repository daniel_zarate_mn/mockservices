package com.bbva.zic.mobiletransfers.facade.v01.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumOrigin {

	AUTO("X"), BNET("X"), MBAN("X"), NETCASH("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumOrigin.class);

	private String key;

	EnumOrigin(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumOrigin getEnumValue(final String key) {
		for (final EnumOrigin origin : values()) {
			if (origin.key.equals(key)) {
				return origin;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumOrigin origin : values()) {
			if (origin.name().equalsIgnoreCase(key)) {
				return origin.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}
}
