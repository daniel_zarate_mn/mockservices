package com.bbva.zic.mobiletransfers.facade.v01.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumAgileOperationType;

@XmlRootElement(name = "agileOperation", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "agileOperation", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAgileOperation {

	@NotNull
	private EnumAgileOperationType agileOperationType;

	@NotNull
	private DtoEntityId entityId;

	@NotNull
	private String id;

	@NotNull
	private Date operationDate;

	@NotNull
	private String reference;

	@NotNull
	private String shortName;

	@NotNull
	private String user;

	@NotNull
	private DtoAgileOperationTransfer operation;

	public EnumAgileOperationType getAgileOperationType() {
		return agileOperationType;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(agileOperationType).append(entityId).append(id).append(operationDate).append(reference)
				.append(shortName).append(user).append(operation).toHashCode();
	}

	public void setAgileOperationType(final EnumAgileOperationType agileOperationType) {
		this.agileOperationType = agileOperationType;
	}

	public DtoEntityId getEntityId() {
		return entityId;
	}

	public void setEntityId(final DtoEntityId entityId) {
		this.entityId = entityId;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(final Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(final String reference) {
		this.reference = reference;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(final String user) {
		this.user = user;
	}

	public DtoAgileOperationTransfer getOperation() {
		return operation;
	}

	public void setOperation(final DtoAgileOperationTransfer operation) {
		this.operation = operation;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoAgileOperation)) {
			return false;
		}
		final DtoAgileOperation castOther = (DtoAgileOperation) other;
		return new EqualsBuilder().append(agileOperationType, castOther.agileOperationType).append(entityId, castOther.entityId)
				.append(id, castOther.id).append(operationDate, castOther.operationDate).append(reference, castOther.reference)
				.append(shortName, castOther.shortName).append(user, castOther.user).append(operation, castOther.operation).isEquals();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("agileOperationType", agileOperationType).append("entityId", entityId).append("id", id)
				.append("operationDate", operationDate).append("reference", reference).append("shortName", shortName).append("user", user)
				.append("operation", operation).toString();
	}

}
