package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "enterpriseAgreement", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "enterpriseAgreement", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnterpriseAgreement implements IAgreement {

    @NotNull
    private ProductBase productBase;

    @NotNull
    private String cashIndicator;

    @NotNull
    private City city;

    @NotNull
    private String clasification;

    @NotNull
    private String commercialName;

    @NotNull
    private String conceptFormat;

    @NotNull
    private String conceptIndicator;

    @NotNull
    private String conceptValidationRoutine;

    @NotNull
    private String enterpriseAgreementOwner;

    @NotNull
    private String creditCardIndicator;

    @NotNull
    private Currency currency;

    @NotNull
    private String formalName;

    @NotNull
    private String helpImageUrl;

    @NotNull
    private String helpPaymentType;

    @NotNull
    private String helpReference;

    @NotNull
    private String helpConcept;

    @NotNull
    private String helpThirdMessage;

    @NotNull
    private String authorization;

    @NotNull
    private String id;

    @NotNull
    private String immediatePaymentIndicator;

    @NotNull
    private BusinessLine businessLine;

    @NotNull
    private String logoUrl;

    @NotNull
    private Number maxChecksLimit;

    @NotNull
    private Number maxConceptLength;

    @NotNull
    private Number maxReferenceLength;

    @NotNull
    private Number minConceptLength;

    @NotNull
    private Number minReferenceLength;

    @NotNull
    private String onlinePaymentIndicator;

    @NotNull
    private String paymentType;

    @NotNull
    private String referenceFormat;

    @NotNull
    private String referenceIndicator;

    @NotNull
    private String referenceValidationRoutine;

    @NotNull
    private Region region;

    @NotNull
    private String remittanceIndicator;

    @NotNull
    private String rupCode;

    @NotNull
    private String rupIndicator;

    @NotNull
    private String status;

    @NotNull
    private String serviceIndicator;

    @NotNull
    private String shortName;

    @NotNull
    private String issuingCompany;

    @NotNull
    private String theme;

    public ProductBase getProductBase() {
        return productBase;
    }

    public void setProductBase(final ProductBase productBase) {
        this.productBase = productBase;
    }

    public String getCashIndicator() {
        return cashIndicator;
    }

    public void setCashIndicator(final String cashIndicator) {
        this.cashIndicator = cashIndicator;
    }

    public City getCity() {
        return city;
    }

    public void setCity(final City city) {
        this.city = city;
    }

    public String getClasification() {
        return clasification;
    }

    public void setClasification(final String clasification) {
        this.clasification = clasification;
    }

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(final String commercialName) {
        this.commercialName = commercialName;
    }

    public String getConceptFormat() {
        return conceptFormat;
    }

    public void setConceptFormat(final String conceptFormat) {
        this.conceptFormat = conceptFormat;
    }

    public String getConceptIndicator() {
        return conceptIndicator;
    }

    public void setConceptIndicator(final String conceptIndicator) {
        this.conceptIndicator = conceptIndicator;
    }

    public String getConceptValidationRoutine() {
        return conceptValidationRoutine;
    }

    public void setConceptValidationRoutine(final String conceptValidationRoutine) {
        this.conceptValidationRoutine = conceptValidationRoutine;
    }

    public String getEnterpriseAgreementOwner() {
        return enterpriseAgreementOwner;
    }

    public void setEnterpriseAgreementOwner(final String enterpriseAgreementOwner) {
        this.enterpriseAgreementOwner = enterpriseAgreementOwner;
    }

    public String getCreditCardIndicator() {
        return creditCardIndicator;
    }

    public void setCreditCardIndicator(final String creditCardIndicator) {
        this.creditCardIndicator = creditCardIndicator;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    public String getFormalName() {
        return formalName;
    }

    public void setFormalName(final String formalName) {
        this.formalName = formalName;
    }

    public String getHelpImageUrl() {
        return helpImageUrl;
    }

    public void setHelpImageUrl(final String helpImageUrl) {
        this.helpImageUrl = helpImageUrl;
    }

    public String getHelpPaymentType() {
        return helpPaymentType;
    }

    public void setHelpPaymentType(final String helpPaymentType) {
        this.helpPaymentType = helpPaymentType;
    }

    public String getHelpReference() {
        return helpReference;
    }

    public void setHelpReference(final String helpReference) {
        this.helpReference = helpReference;
    }

    public String getHelpConcept() {
        return helpConcept;
    }

    public void setHelpConcept(final String helpConcept) {
        this.helpConcept = helpConcept;
    }

    public String getHelpThirdMessage() {
        return helpThirdMessage;
    }

    public void setHelpThirdMessage(final String helpThirdMessage) {
        this.helpThirdMessage = helpThirdMessage;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(final String authorization) {
        this.authorization = authorization;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getImmediatePaymentIndicator() {
        return immediatePaymentIndicator;
    }

    public void setImmediatePaymentIndicator(final String immediatePaymentIndicator) {
        this.immediatePaymentIndicator = immediatePaymentIndicator;
    }

    public BusinessLine getBusinessLine() {
        return businessLine;
    }

    public void setBusinessLine(final BusinessLine businessLine) {
        this.businessLine = businessLine;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(final String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Number getMaxChecksLimit() {
        return maxChecksLimit;
    }

    public void setMaxChecksLimit(final Number maxChecksLimit) {
        this.maxChecksLimit = maxChecksLimit;
    }

    public Number getMaxConceptLength() {
        return maxConceptLength;
    }

    public void setMaxConceptLength(final Number maxConceptLength) {
        this.maxConceptLength = maxConceptLength;
    }

    public Number getMaxReferenceLength() {
        return maxReferenceLength;
    }

    public void setMaxReferenceLength(final Number maxReferenceLength) {
        this.maxReferenceLength = maxReferenceLength;
    }

    public Number getMinConceptLength() {
        return minConceptLength;
    }

    public void setMinConceptLength(final Number minConceptLength) {
        this.minConceptLength = minConceptLength;
    }

    public Number getMinReferenceLength() {
        return minReferenceLength;
    }

    public void setMinReferenceLength(final Number minReferenceLength) {
        this.minReferenceLength = minReferenceLength;
    }

    public String getOnlinePaymentIndicator() {
        return onlinePaymentIndicator;
    }

    public void setOnlinePaymentIndicator(final String onlinePaymentIndicator) {
        this.onlinePaymentIndicator = onlinePaymentIndicator;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }

    public String getReferenceFormat() {
        return referenceFormat;
    }

    public void setReferenceFormat(final String referenceFormat) {
        this.referenceFormat = referenceFormat;
    }

    public String getReferenceIndicator() {
        return referenceIndicator;
    }

    public void setReferenceIndicator(final String referenceIndicator) {
        this.referenceIndicator = referenceIndicator;
    }

    public String getReferenceValidationRoutine() {
        return referenceValidationRoutine;
    }

    public void setReferenceValidationRoutine(final String referenceValidationRoutine) {
        this.referenceValidationRoutine = referenceValidationRoutine;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(final Region region) {
        this.region = region;
    }

    public String getRemittanceIndicator() {
        return remittanceIndicator;
    }

    public void setRemittanceIndicator(final String remittanceIndicator) {
        this.remittanceIndicator = remittanceIndicator;
    }

    public String getRupCode() {
        return rupCode;
    }

    public void setRupCode(final String rupCode) {
        this.rupCode = rupCode;
    }

    public String getRupIndicator() {
        return rupIndicator;
    }

    public void setRupIndicator(final String rupIndicator) {
        this.rupIndicator = rupIndicator;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getServiceIndicator() {
        return serviceIndicator;
    }

    public void setServiceIndicator(final String serviceIndicator) {
        this.serviceIndicator = serviceIndicator;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    public String getIssuingCompany() {
        return issuingCompany;
    }

    public void setIssuingCompany(final String issuingCompany) {
        this.issuingCompany = issuingCompany;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(final String theme) {
        this.theme = theme;
    }

    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof EnterpriseAgreement)) {
            return false;
        }
        final EnterpriseAgreement castOther = (EnterpriseAgreement) other;
        return new EqualsBuilder().append(productBase, castOther.productBase).append(cashIndicator, castOther.cashIndicator)
                .append(city, castOther.city).append(clasification, castOther.clasification)
                .append(commercialName, castOther.commercialName).append(conceptFormat, castOther.conceptFormat)
                .append(conceptIndicator, castOther.conceptIndicator).append(conceptValidationRoutine, castOther.conceptValidationRoutine)
                .append(enterpriseAgreementOwner, castOther.enterpriseAgreementOwner)
                .append(creditCardIndicator, castOther.creditCardIndicator).append(currency, castOther.currency)
                .append(formalName, castOther.formalName).append(helpImageUrl, castOther.helpImageUrl)
                .append(helpPaymentType, castOther.helpPaymentType).append(helpReference, castOther.helpReference)
                .append(helpConcept, castOther.helpConcept).append(helpThirdMessage, castOther.helpThirdMessage)
                .append(authorization, castOther.authorization).append(id, castOther.id)
                .append(immediatePaymentIndicator, castOther.immediatePaymentIndicator).append(businessLine, castOther.businessLine)
                .append(logoUrl, castOther.logoUrl).append(maxChecksLimit, castOther.maxChecksLimit)
                .append(maxConceptLength, castOther.maxConceptLength).append(maxReferenceLength, castOther.maxReferenceLength)
                .append(minConceptLength, castOther.minConceptLength).append(minReferenceLength, castOther.minReferenceLength)
                .append(onlinePaymentIndicator, castOther.onlinePaymentIndicator).append(paymentType, castOther.paymentType)
                .append(referenceFormat, castOther.referenceFormat).append(referenceIndicator, castOther.referenceIndicator)
                .append(referenceValidationRoutine, castOther.referenceValidationRoutine).append(region, castOther.region)
                .append(remittanceIndicator, castOther.remittanceIndicator).append(rupCode, castOther.rupCode)
                .append(rupIndicator, castOther.rupIndicator).append(status, castOther.status)
                .append(serviceIndicator, castOther.serviceIndicator).append(shortName, castOther.shortName)
                .append(issuingCompany, castOther.issuingCompany).append(theme, castOther.theme).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(productBase).append(cashIndicator).append(city).append(clasification).append(commercialName)
                .append(conceptFormat).append(conceptIndicator).append(conceptValidationRoutine).append(enterpriseAgreementOwner)
                .append(creditCardIndicator).append(currency).append(formalName).append(helpImageUrl).append(helpPaymentType)
                .append(helpReference).append(helpConcept).append(helpThirdMessage).append(authorization).append(id)
                .append(immediatePaymentIndicator).append(businessLine).append(logoUrl).append(maxChecksLimit).append(maxConceptLength)
                .append(maxReferenceLength).append(minConceptLength).append(minReferenceLength).append(onlinePaymentIndicator)
                .append(paymentType).append(referenceFormat).append(referenceIndicator).append(referenceValidationRoutine).append(region)
                .append(remittanceIndicator).append(rupCode).append(rupIndicator).append(status).append(serviceIndicator).append(shortName)
                .append(issuingCompany).append(theme).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productBase", productBase).append("cashIndicator", cashIndicator).append("city", city)
                .append("clasification", clasification).append("commercialName", commercialName).append("conceptFormat", conceptFormat)
                .append("conceptIndicator", conceptIndicator).append("conceptValidationRoutine", conceptValidationRoutine)
                .append("enterpriseAgreementOwner", enterpriseAgreementOwner).append("creditCardIndicator", creditCardIndicator)
                .append("currency", currency).append("formalName", formalName).append("helpImageUrl", helpImageUrl)
                .append("helpPaymentType", helpPaymentType).append("helpReference", helpReference).append("helpConcept", helpConcept)
                .append("helpThirdMessage", helpThirdMessage).append("authorization", authorization).append("id", id)
                .append("immediatePaymentIndicator", immediatePaymentIndicator).append("businessLine", businessLine)
                .append("logoUrl", logoUrl).append("maxChecksLimit", maxChecksLimit).append("maxConceptLength", maxConceptLength)
                .append("maxReferenceLength", maxReferenceLength).append("minConceptLength", minConceptLength)
                .append("minReferenceLength", minReferenceLength).append("onlinePaymentIndicator", onlinePaymentIndicator)
                .append("paymentType", paymentType).append("referenceFormat", referenceFormat)
                .append("referenceIndicator", referenceIndicator).append("referenceValidationRoutine", referenceValidationRoutine)
                .append("region", region).append("remittanceIndicator", remittanceIndicator).append("rupCode", rupCode)
                .append("rupIndicator", rupIndicator).append("status", status).append("serviceIndicator", serviceIndicator)
                .append("shortName", shortName).append("issuingCompany", issuingCompany).append("theme", theme).toString();
    }

}
