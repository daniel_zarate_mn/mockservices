package com.medianetsoftware.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@XmlRootElement(name = "tax", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "tax", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tax {


	@NotNull
	private Money value;

	@NotNull
	private String description;

	public Money getValue() {
		return value;
	}

	public void setValue(final Money value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Tax)) {
			return false;
		}
		final Tax castOther = (Tax) other;
		return new EqualsBuilder().append(value, castOther.value).append(description, castOther.description).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(value).append(description).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("value", value).append("description", description).toString();
	}

}
