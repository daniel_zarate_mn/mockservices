package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "autoLoan", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "autoLoan", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class AutoLoan implements ILoan{

	@NotNull
	private ProductBase productBase;

	@NotNull
	private String id;

	@NotNull
	private String shortName;

	@NotNull
	private String loanNumber;

	@NotNull
	private String productDescription;

	@NotNull
	private String channelIndicator;

	@NotNull
	private Integer creditIndice;

	public ProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final ProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(final String loanNumber) {
		this.loanNumber = loanNumber;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(final String productDescription) {
		this.productDescription = productDescription;
	}

	public String getChannelIndicator() {
		return channelIndicator;
	}

	public void setChannelIndicator(final String channelIndicator) {
		this.channelIndicator = channelIndicator;
	}

	public Integer getCreditIndice() {
		return creditIndice;
	}

	public void setCreditIndice(final Integer creditIndice) {
		this.creditIndice = creditIndice;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AutoLoan)) {
			return false;
		}
		final AutoLoan castOther = (AutoLoan) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(shortName, castOther.shortName).append(loanNumber, castOther.loanNumber)
				.append(productDescription, castOther.productDescription).append(channelIndicator, castOther.channelIndicator)
				.append(creditIndice, castOther.creditIndice).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(shortName).append(loanNumber).append(productDescription)
				.append(channelIndicator).append(creditIndice).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("shortName", shortName)
				.append("loanNumber", loanNumber).append("productDescription", productDescription)
				.append("channelIndicator", channelIndicator).append("creditIndice", creditIndice).toString();
	}

}
