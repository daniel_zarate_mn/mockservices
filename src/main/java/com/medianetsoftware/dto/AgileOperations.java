package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(name = "agileOperations", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "agileOperations", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgileOperations {


	@NotNull
	private List<AgileOperation> listAgileOperation;

	public List<AgileOperation> getListAgileOperation() {
		return listAgileOperation;
	}

	public void setListAgileOperation(final List<AgileOperation> listAgileOperation) {
		this.listAgileOperation = listAgileOperation;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AgileOperations)) {
			return false;
		}
		final AgileOperations castOther = (AgileOperations) other;
		return new EqualsBuilder().append(listAgileOperation, castOther.listAgileOperation).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(listAgileOperation).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("listAgileOperation", listAgileOperation).toString();
	}

}
