package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "entityId", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "entityId", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class EntityId {

	@NotNull
	private String applicationId;

	@NotNull
	private String mainTransactionId;

	@NotNull
	private String trackingId;

	@NotNull
	private String securityCode;

	@NotNull
	private String servicePaymentId;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(final String applicationId) {
		this.applicationId = applicationId;
	}

	public String getMainTransactionId() {
		return mainTransactionId;
	}

	public void setMainTransactionId(final String mainTransactionId) {
		this.mainTransactionId = mainTransactionId;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(final String trackingId) {
		this.trackingId = trackingId;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(final String securityCode) {
		this.securityCode = securityCode;
	}

	public String getServicePaymentId() {
		return servicePaymentId;
	}

	public void setServicePaymentId(final String servicePaymentId) {
		this.servicePaymentId = servicePaymentId;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof EntityId)) {
			return false;
		}
		final EntityId castOther = (EntityId) other;
		return new EqualsBuilder().append(applicationId, castOther.applicationId).append(mainTransactionId, castOther.mainTransactionId)
				.append(trackingId, castOther.trackingId).append(securityCode, castOther.securityCode)
				.append(servicePaymentId, castOther.servicePaymentId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(applicationId).append(mainTransactionId).append(trackingId).append(securityCode)
				.append(servicePaymentId).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("applicationId", applicationId).append("mainTransactionId", mainTransactionId)
				.append("trackingId", trackingId).append("securityCode", securityCode).append("servicePaymentId", servicePaymentId)
				.toString();
	}

}
