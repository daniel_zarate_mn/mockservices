package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "financialValues", namespace = "http://bbva.com/zic/agileoperations/V01")
@XmlType(name = "financialValues", namespace = "http://bbva.com/zic/agileoperations/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class FinancialValues {

	@NotNull
	private Money exchangeValue;

	@NotNull
	private Money value;

	public Money getValue() {
		return value;
	}

	public void setValue(final Money value) {
		this.value = value;
	}

	public Money getExchangeValue() {
		return exchangeValue;
	}

	public void setExchangeValue(final Money exchangeValue) {
		this.exchangeValue = exchangeValue;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof FinancialValues)) {
			return false;
		}
		final FinancialValues castOther = (FinancialValues) other;
		return new EqualsBuilder().append(exchangeValue, castOther.exchangeValue).append(value, castOther.value).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(exchangeValue).append(value).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("exchangeValue", exchangeValue).append("value", value).toString();
	}

}
