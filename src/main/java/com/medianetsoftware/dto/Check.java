package com.medianetsoftware.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@XmlRootElement(name = "check", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "check", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Check {

	@NotNull
	private Money amount;
	
	@NotNull
	private String checkNumber;
	
	@NotNull
	private String documentData;
	
	@NotNull
	private String availability;
	
	@NotNull
	private String securityCode;
	
	@NotNull
	private String BR6digits;

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getDocumentData() {
		return documentData;
	}

	public void setDocumentData(String documentData) {
		this.documentData = documentData;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getBR6digits() {
		return BR6digits;
	}

	public void setBR6digits(String bR6digits) {
		BR6digits = bR6digits;
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Check)) {
			return false;
		}
		final Check castOther = (Check) other;
		return new EqualsBuilder().append(amount, castOther.amount).append(checkNumber, castOther.checkNumber)
				.append(documentData, castOther.documentData).append(availability, castOther.availability)
				.append(securityCode, castOther.securityCode).append(BR6digits, castOther.BR6digits).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(amount).append(checkNumber)
				.append(documentData).append(availability)
				.append(securityCode).append(BR6digits).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("amount", amount).append("checkNumber", checkNumber)
				.append("documentData", documentData).append("availability", availability).append("securityCode", securityCode)
				.append("BR6digits", BR6digits).toString();
	}
}
