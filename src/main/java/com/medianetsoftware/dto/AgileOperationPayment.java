package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "agileOperationPayment", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "agileOperationPayment", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgileOperationPayment {

	@NotNull
	private String corporationName;

	@NotNull
	private Receiver receiver;

	@NotNull
	private Sender sender;

	public String getCorporationName() {
		return corporationName;
	}

	public void setCorporationName(final String corporationName) {
		this.corporationName = corporationName;
	}

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(final Receiver receiver) {
		this.receiver = receiver;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(final Sender sender) {
		this.sender = sender;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AgileOperationPayment)) {
			return false;
		}
		final AgileOperationPayment castOther = (AgileOperationPayment) other;
		return new EqualsBuilder().append(corporationName, castOther.corporationName).append(receiver, castOther.receiver)
				.append(sender, castOther.sender).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corporationName).append(receiver).append(sender).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corporationName", corporationName).append("receiver", receiver).append("sender", sender)
				.toString();
	}

}
