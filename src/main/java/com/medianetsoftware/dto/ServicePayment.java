package com.medianetsoftware.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@XmlRootElement(name = "servicePayment", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "servicePayment", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServicePayment {
	
	@NotNull
	private Money amount;
	
	@NotNull
	private List<Check> checksNumber;
	
	@NotNull
	private Commision commission;
	
	@NotNull
	private String companyID;
	
	@NotNull
	private String concept;
	
	@NotNull
	private DigitalTaxCertificate digitalTaxCertificate;
	
	@NotNull
	private ErrorInfo errorInfo;
	
	@NotNull
	private Date expirationDate;
	
	@NotNull
	private String id;
	
	@NotNull
	private Date notificationDate;
	
	@NotNull
	private Date operationDate;
	
	@NotNull
	private String paymentStatus;
	
	@NotNull
	private String paymentType;
	
	@NotNull
	private Receiver receiver;
	
	@NotNull
	private String reference;
	
	@NotNull
	private Scheduler scheduler;
	
	@NotNull
	private Sender sender;
	
	@NotNull
	private Money tax;
	
	@NotNull
	private Money taxableCash;
	
	@NotNull
	private String uniquePaymentReceipt;

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public List<Check> getChecksNumber() {
		return checksNumber;
	}

	public void setChecksNumber(List<Check> checksNumber) {
		this.checksNumber = checksNumber;
	}

	public Commision getCommission() {
		return commission;
	}

	public void setCommission(Commision commission) {
		this.commission = commission;
	}

	public String getCompanyID() {
		return companyID;
	}

	public void setCompanyID(String companyID) {
		this.companyID = companyID;
	}

	public String getConcept() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	public DigitalTaxCertificate getDigitalTaxCertificate() {
		return digitalTaxCertificate;
	}

	public void setDigitalTaxCertificate(DigitalTaxCertificate digitalTaxCertificate) {
		this.digitalTaxCertificate = digitalTaxCertificate;
	}

	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public Money getTax() {
		return tax;
	}

	public void setTax(Money tax) {
		this.tax = tax;
	}

	public Money getTaxableCash() {
		return taxableCash;
	}

	public void setTaxableCash(Money taxableCash) {
		this.taxableCash = taxableCash;
	}

	public String getUniquePaymentReceipt() {
		return uniquePaymentReceipt;
	}

	public void setUniquePaymentReceipt(String uniquePaymentReceipt) {
		this.uniquePaymentReceipt = uniquePaymentReceipt;
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ServicePayment)) {
			return false;
		}
		final ServicePayment castOther = (ServicePayment) other;
		return new EqualsBuilder().append(amount, castOther.amount).append(checksNumber, castOther.checksNumber)
				.append(commission, castOther.commission).append(companyID, castOther.companyID)
				.append(concept, castOther.concept).append(digitalTaxCertificate, castOther.digitalTaxCertificate)
				.append(errorInfo, castOther.errorInfo).append(expirationDate, castOther.expirationDate)
				.append(id, castOther.id).append(notificationDate, castOther.notificationDate)
				.append(operationDate, castOther.operationDate).append(paymentStatus, castOther.paymentStatus)
				.append(paymentType, castOther.paymentType).append(receiver, castOther.receiver)
				.append(reference, castOther.reference).append(scheduler, castOther.scheduler)
				.append(sender, castOther.sender).append(tax, castOther.tax)
				.append(taxableCash, castOther.taxableCash).append(uniquePaymentReceipt, castOther.uniquePaymentReceipt).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(amount).append(checksNumber)
				.append(commission).append(companyID)
				.append(concept).append(digitalTaxCertificate)
				.append(errorInfo).append(expirationDate)
				.append(id).append(notificationDate)
				.append(operationDate).append(paymentStatus)
				.append(paymentType).append(receiver)
				.append(reference).append(scheduler)
				.append(sender).append(tax)
				.append(taxableCash).append(uniquePaymentReceipt)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("amount", amount).append("checksNumber", checksNumber)
				.append("commission", commission).append("companyID", companyID)
				.append("concept", concept).append("digitalTaxCertificate", digitalTaxCertificate)
				.append("errorInfo", errorInfo).append("expirationDate", expirationDate)
				.append("id", id).append("notificationDate", notificationDate)
				.append("operationDate", operationDate).append("paymentStatus", paymentStatus)
				.append("paymentType", paymentType).append("receiver", receiver)
				.append("reference", reference).append("scheduler", scheduler)
				.append("sender", sender).append("tax", tax)
				.append("taxableCash", taxableCash).append("uniquePaymentReceipt", uniquePaymentReceipt)
				.toString();
	}
}