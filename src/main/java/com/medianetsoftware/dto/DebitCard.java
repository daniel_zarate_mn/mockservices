package com.medianetsoftware.dto;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "debitCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "debitCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DebitCard implements ICard {

	@NotNull
	private ProductBase productBase;

	@NotNull
	private String id;

	@NotNull
	private String accountNumber;

	@NotNull
	private String cardNumber;

	@NotNull
	private String shortName;

	@NotNull
	private Commision commision;

	@NotNull
	private Money minimumPayment;

	@NotNull
	private String interbankCode;

	public ProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final ProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public Commision getCommision() {
		return commision;
	}

	public void setCommision(final Commision commision) {
		this.commision = commision;
	}

	public Money getMinimumPayment() {
		return minimumPayment;
	}

	public void setMinimumPayment(final Money minimumPayment) {
		this.minimumPayment = minimumPayment;
	}

	public String getInterbankCode() {
		return interbankCode;
	}

	public void setInterbankCode(final String interbankCode) {
		this.interbankCode = interbankCode;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DebitCard)) {
			return false;
		}
		final DebitCard castOther = (DebitCard) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(accountNumber, castOther.accountNumber).append(cardNumber, castOther.cardNumber)
				.append(shortName, castOther.shortName).append(commision, castOther.commision)
				.append(minimumPayment, castOther.minimumPayment).append(interbankCode, castOther.interbankCode).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(accountNumber).append(cardNumber).append(shortName)
				.append(commision).append(minimumPayment).append(interbankCode).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("accountNumber", accountNumber)
				.append("cardNumber", cardNumber).append("shortName", shortName).append("commision", commision)
				.append("minimumPayment", minimumPayment).append("interbankCode", interbankCode).toString();
	}

}
