package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "prepaidCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "prepaidCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class PrepaidCard implements ICard {

	@NotNull
	private ProductBase productBase;

	@NotNull
	private String id;

	@NotNull
	private String cardNumber;

	@NotNull
	private String shortName;

	public ProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final ProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PrepaidCard)) {
			return false;
		}
		final PrepaidCard castOther = (PrepaidCard) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(cardNumber, castOther.cardNumber).append(shortName, castOther.shortName).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(cardNumber).append(shortName).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("cardNumber", cardNumber)
				.append("shortName", shortName).toString();
	}

}
