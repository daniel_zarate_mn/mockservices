package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "authorizationInfo", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "authorizationInfo", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthorizationInfo {

	@NotNull
	private String receiverAuthNumber;

	@NotNull
	private String senderAuthNumber;

	public String getReceiverAuthNumber() {

		return receiverAuthNumber;
	}

	public void setReceiverAuthNumber(final String receiverAuthNumber) {

		this.receiverAuthNumber = receiverAuthNumber;
	}

	public String getSenderAuthNumber() {

		return senderAuthNumber;
	}

	public void setSenderAuthNumber(final String senderAuthNumber) {

		this.senderAuthNumber = senderAuthNumber;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AuthorizationInfo)) {
			return false;
		}
		final AuthorizationInfo castOther = (AuthorizationInfo) other;
		return new EqualsBuilder().append(receiverAuthNumber, castOther.receiverAuthNumber)
				.append(senderAuthNumber, castOther.senderAuthNumber).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(receiverAuthNumber).append(senderAuthNumber).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("receiverAuthNumber", receiverAuthNumber).append("senderAuthNumber", senderAuthNumber)
				.toString();
	}

}
