package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "balance", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "balance", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Balance {

	@NotNull
	private Money currentBalance;

	@NotNull
	private Money grantedBalance;

	@NotNull
	private Money totalBalance;

	@NotNull
	private Money balance;

	@NotNull
	private Money pendingBalance;

	@NotNull
	private Money cuttingBalance;

	@NotNull
	private Money balanceAtDate;

	@NotNull
	private Money availableBalance;

	public Money getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(final Money currentBalance) {
		this.currentBalance = currentBalance;
	}

	public Money getGrantedBalance() {
		return grantedBalance;
	}

	public void setGrantedBalance(final Money grantedBalance) {
		this.grantedBalance = grantedBalance;
	}

	public Money getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(final Money totalBalance) {
		this.totalBalance = totalBalance;
	}

	public Money getBalance() {
		return balance;
	}

	public void setBalance(final Money balance) {
		this.balance = balance;
	}

	public Money getPendingBalance() {
		return pendingBalance;
	}

	public void setPendingBalance(final Money pendingBalance) {
		this.pendingBalance = pendingBalance;
	}

	public Money getCuttingBalance() {
		return cuttingBalance;
	}

	public void setCuttingBalance(final Money cuttingBalance) {
		this.cuttingBalance = cuttingBalance;
	}

	public Money getBalanceAtDate() {
		return balanceAtDate;
	}

	public void setBalanceAtDate(final Money balanceAtDate) {
		this.balanceAtDate = balanceAtDate;
	}

	public Money getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(final Money availableBalance) {
		this.availableBalance = availableBalance;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Balance)) {
			return false;
		}
		final Balance castOther = (Balance) other;
		return new EqualsBuilder().append(currentBalance, castOther.currentBalance).append(grantedBalance, castOther.grantedBalance)
				.append(totalBalance, castOther.totalBalance).append(balance, castOther.balance)
				.append(pendingBalance, castOther.pendingBalance).append(cuttingBalance, castOther.cuttingBalance)
				.append(balanceAtDate, castOther.balanceAtDate).append(availableBalance, castOther.availableBalance).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(currentBalance).append(grantedBalance).append(totalBalance).append(balance)
				.append(pendingBalance).append(cuttingBalance).append(balanceAtDate).append(availableBalance).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("currentBalance", currentBalance).append("grantedBalance", grantedBalance)
				.append("totalBalance", totalBalance).append("balance", balance).append("pendingBalance", pendingBalance)
				.append("cuttingBalance", cuttingBalance).append("balanceAtDate", balanceAtDate)
				.append("availableBalance", availableBalance).toString();
	}

}
