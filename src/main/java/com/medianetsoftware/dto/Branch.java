package com.medianetsoftware.dto;

import com.medianetsoftware.dto.enums.EnumRegionalType;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "branch", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "branch", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Branch {

	@NotNull
	private String id;

	@NotNull
	private String name;

	@NotNull
	private EnumRegionalType type;

	@NotNull
	private Bank bank;

	@NotNull
	private Region region;

	public String getId() {

		return id;
	}

	public void setId(final String id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(final String name) {

		this.name = name;
	}

	public EnumRegionalType getType() {

		return type;
	}

	public void setType(final EnumRegionalType type) {

		this.type = type;
	}

	public Bank getBank() {

		return bank;
	}

	public void setBank(final Bank bank) {

		this.bank = bank;
	}

	public Region getRegion() {

		return region;
	}

	public void setRegion(final Region region) {

		this.region = region;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Branch)) {
			return false;
		}
		final Branch castOther = (Branch) other;
		return new EqualsBuilder().append(id, castOther.id).append(name, castOther.name).append(type, castOther.type)
				.append(bank, castOther.bank).append(region, castOther.region).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(name).append(type).append(bank).append(region).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("type", type).append("bank", bank)
				.append("region", region).toString();
	}

}
