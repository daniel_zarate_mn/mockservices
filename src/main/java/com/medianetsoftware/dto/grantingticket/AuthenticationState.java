package com.medianetsoftware.dto.grantingticket;

import java.util.List;

public class AuthenticationState {

	private String authenticationState;
	private List<AuthenticationData> authenticationData;
	private String userType;
	private String userStatus;
	private String clientStatus;
	private String countableTerminal;

	public String getAuthenticationState() {
		return authenticationState;
	}

	public void setAuthenticationState(String authenticationState) {
		this.authenticationState = authenticationState;
	}

	public List<AuthenticationData> getAuthenticationData() {
		return authenticationData;
	}

	public void setAuthenticationData(
			List<AuthenticationData> authenticationData) {
		this.authenticationData = authenticationData;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public String getCountableTerminal() {
		return countableTerminal;
	}

	public void setCountableTerminal(String countableTerminal) {
		this.countableTerminal = countableTerminal;
	}
}
