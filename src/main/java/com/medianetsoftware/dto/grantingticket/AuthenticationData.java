package com.medianetsoftware.dto.grantingticket;

public class AuthenticationData {

	private String idAuthenticationData;
	private String[] authenticationData;

	public String getIdAuthenticationData() {
		return idAuthenticationData;
	}

	public void setIdAuthenticationData(String idAuthenticationData) {
		this.idAuthenticationData = idAuthenticationData;
	}

	public String[] getAuthenticationData() {
		return authenticationData;
	}

	public void setAuthenticationData(String[] authenticationData) {
		this.authenticationData = authenticationData;
	}
}
