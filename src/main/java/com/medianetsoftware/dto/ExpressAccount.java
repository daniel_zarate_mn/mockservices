package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "expressAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "expressAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExpressAccount implements IAccount {

	@NotNull
	private ProductBase productBase;

	@NotNull
	private String id;

	@NotNull
	private String shortName;

	@NotNull
	private String cellphoneNumber;

	@NotNull
	private String cardNumber;

	@NotNull
	private CellphoneCompany cellphoneCompany;

	public ProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final ProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getCellphoneNumber() {
		return cellphoneNumber;
	}

	public void setCellphoneNumber(final String cellphoneNumber) {
		this.cellphoneNumber = cellphoneNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public CellphoneCompany getCellphoneCompany() {
		return cellphoneCompany;
	}

	public void setCellphoneCompany(final CellphoneCompany cellphoneCompany) {
		this.cellphoneCompany = cellphoneCompany;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ExpressAccount)) {
			return false;
		}
		final ExpressAccount castOther = (ExpressAccount) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(shortName, castOther.shortName).append(cellphoneNumber, castOther.cellphoneNumber)
				.append(cardNumber, castOther.cardNumber).append(cellphoneCompany, castOther.cellphoneCompany).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(shortName).append(cellphoneNumber).append(cardNumber)
				.append(cellphoneCompany).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("shortName", shortName)
				.append("cellphoneNumber", cellphoneNumber).append("cardNumber", cardNumber).append("cellphoneCompany", cellphoneCompany)
				.toString();
	}

}
