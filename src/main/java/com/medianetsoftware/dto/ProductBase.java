package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "productBase", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "productBase", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductBase {

	@NotNull
	private String name;

	@NotNull
	private Branch branch;

	@NotNull
	private Currency currency;

	@NotNull
	private String type;

	@NotNull
	private Balance balance;

	@NotNull
	private String shaftAccountIndicator;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(final Branch branch) {
		this.branch = branch;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(final Currency currency) {
		this.currency = currency;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public Balance getBalance() {
		return balance;
	}

	public void setBalance(final Balance balance) {
		this.balance = balance;
	}

	public String getShaftAccountIndicator() {
		return shaftAccountIndicator;
	}

	public void setShaftAccountIndicator(final String shaftAccountIndicator) {
		this.shaftAccountIndicator = shaftAccountIndicator;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ProductBase)) {
			return false;
		}
		final ProductBase castOther = (ProductBase) other;
		return new EqualsBuilder().append(name, castOther.name).append(branch, castOther.branch).append(currency, castOther.currency)
				.append(type, castOther.type).append(balance, castOther.balance)
				.append(shaftAccountIndicator, castOther.shaftAccountIndicator).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(name).append(branch).append(currency).append(type).append(balance)
				.append(shaftAccountIndicator).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("branch", branch).append("currency", currency).append("type", type)
				.append("balance", balance).append("shaftAccountIndicator", shaftAccountIndicator).toString();
	}

}
