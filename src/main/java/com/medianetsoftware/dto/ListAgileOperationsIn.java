package com.medianetsoftware.dto;


import com.medianetsoftware.dto.enums.EnumAgileOperationType;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "listAgileOperationsIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "listAgileOperationsIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListAgileOperationsIn {

	@NotNull
	private Pagination pagination;

	@NotNull
	private EnumAgileOperationType agileOperationType;

	@NotNull
	private String codigoFuncion;

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(final Pagination pagination) {
		this.pagination = pagination;
	}

	public EnumAgileOperationType getAgileOperationType() {
		return agileOperationType;
	}

	public void setAgileOperationType(final EnumAgileOperationType agileOperationType) {
		this.agileOperationType = agileOperationType;
	}

	public String getCodigoFuncion() {
		return codigoFuncion;
	}

	public void setCodigoFuncion(final String codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}

}
