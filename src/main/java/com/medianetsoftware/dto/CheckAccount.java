package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "checkAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "checkAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckAccount implements IAccount{


	@NotNull
	private ProductBase productBase;

	@NotNull
	private String id;

	@NotNull
	private String accountNumber;

	@NotNull
	private String shortName;

	@NotNull
	private String indicatorJointAccount;

	@NotNull
	private Boolean isAditionalAccount;

	@NotNull
	private Region region;

	public ProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final ProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getIndicatorJointAccount() {
		return indicatorJointAccount;
	}

	public void setIndicatorJointAccount(final String indicatorJointAccount) {
		this.indicatorJointAccount = indicatorJointAccount;
	}

	public Boolean getIsAditionalAccount() {
		return isAditionalAccount;
	}

	public void setIsAditionalAccount(final Boolean isAditionalAccount) {
		this.isAditionalAccount = isAditionalAccount;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(final Region region) {
		this.region = region;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CheckAccount)) {
			return false;
		}
		final CheckAccount castOther = (CheckAccount) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(accountNumber, castOther.accountNumber).append(shortName, castOther.shortName)
				.append(indicatorJointAccount, castOther.indicatorJointAccount).append(isAditionalAccount, castOther.isAditionalAccount)
				.append(region, castOther.region).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(accountNumber).append(shortName).append(indicatorJointAccount)
				.append(isAditionalAccount).append(region).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("accountNumber", accountNumber)
				.append("shortName", shortName).append("indicatorJointAccount", indicatorJointAccount)
				.append("isAditionalAccount", isAditionalAccount).append("region", region).toString();
	}

}
