package com.medianetsoftware.dto.servicepayments;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "currency", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "currency", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Currency {

	@NotNull
	private String code;

	@NotNull
	private String id;

	@NotNull
	private String name;

	public String getId() {

		return id;
	}

	public void setId(final String id) {

		this.id = id;
	}

	public String getCode() {

		return code;
	}

	public void setCode(final String code) {

		this.code = code;
	}

	public String getName() {

		return name;
	}

	public void setName(final String name) {

		this.name = name;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Currency)) {
			return false;
		}
		final Currency castOther = (Currency) other;
		return new EqualsBuilder().append(code, castOther.code).append(id, castOther.id).append(name, castOther.name).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(code).append(id).append(name).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("code", code).append("id", id).append("name", name).toString();
	}

}
