package com.medianetsoftware.dto.servicepayments;

import java.io.Serializable;

import com.medianetsoftware.dto.CheckAccount;
import com.medianetsoftware.dto.ExpressAccount;
import com.medianetsoftware.dto.IAccount;

public class Account implements IAccount, Serializable{

	private static final long serialVersionUID = -663139785736836578L;

	private CheckAccount checkAccount;

	private ExpressAccount expressAccount;

	public CheckAccount getCheckAccount() {
		return checkAccount;
	}

	public void setCheckAccount(CheckAccount checkAccount) {
		this.checkAccount = checkAccount;
	}

	public ExpressAccount getExpressAccount() {
		return expressAccount;
	}

	public void setExpressAccount(ExpressAccount expressAccount) {
		this.expressAccount = expressAccount;
	}
	
	
	
}
