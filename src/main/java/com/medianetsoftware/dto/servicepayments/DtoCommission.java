package com.medianetsoftware.dto.servicepayments;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@XmlRootElement(name = "commission", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "commission", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoCommission {

	@NotNull
	private String creditLineCommissionWithdrawingCash;
	
	@NotNull
	private Money amount;
	
	@NotNull
	private Tax tax;
	
	public String getCreditLineCommissionWithdrawingCash() {
		return creditLineCommissionWithdrawingCash;
	}

	public void setCreditLineCommissionWithdrawingCash(
			String creditLineCommissionWithdrawingCash) {
		this.creditLineCommissionWithdrawingCash = creditLineCommissionWithdrawingCash;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoCommission)) {
			return false;
		}
		final DtoCommission castOther = (DtoCommission) other;
		return new EqualsBuilder().append(creditLineCommissionWithdrawingCash, castOther.creditLineCommissionWithdrawingCash)
				.append(amount, castOther.amount).append(tax, castOther.tax).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(creditLineCommissionWithdrawingCash).append(amount)
				.append(tax).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("creditLineCommissionWithdrawingCash", creditLineCommissionWithdrawingCash)
				.append("amount", amount).append("tax", tax).toString();
	}
}