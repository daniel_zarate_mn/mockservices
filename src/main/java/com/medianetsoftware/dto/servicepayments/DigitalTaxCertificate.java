package com.medianetsoftware.dto.servicepayments;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "digitalTaxCertificate", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "digitalTaxCertificate", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DigitalTaxCertificate {

	@NotNull
	private Tax tax;

	@NotNull
	private String taxpayer;

	public Tax getTax() {

		return tax;
	}

	public void setTax(final Tax tax) {

		this.tax = tax;
	}

	public String getTaxpayer() {

		return taxpayer;
	}

	public void setTaxpayer(final String taxpayer) {

		this.taxpayer = taxpayer;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DigitalTaxCertificate)) {
			return false;
		}
		final DigitalTaxCertificate castOther = (DigitalTaxCertificate) other;
		return new EqualsBuilder().append(tax, castOther.tax).append(taxpayer, castOther.taxpayer).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(tax).append(taxpayer).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("tax", tax).append("taxpayer", taxpayer).toString();
	}

}
