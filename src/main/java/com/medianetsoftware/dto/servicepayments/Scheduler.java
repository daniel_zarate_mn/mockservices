package com.medianetsoftware.dto.servicepayments;


import com.medianetsoftware.dto.enums.EnumIterationDay;
import com.medianetsoftware.dto.enums.EnumLastModifyStatus;
import com.medianetsoftware.dto.enums.EnumSchedulerType;
import com.medianetsoftware.dto.enums.EnumTimeUnit;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlRootElement(name = "scheduler", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "scheduler", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Scheduler {

	@NotNull
	private Date initDate;

	@NotNull
	private Date endDate;

	@NotNull
	private EnumTimeUnit timeUnit;

	@NotNull
	private Integer iterationNumber;

	@NotNull
	private EnumIterationDay iterationDay;

	@NotNull
	private Date executionDate;

	@NotNull
	private Date expirationDate;

	@NotNull
	private Date lastOperationDate;

	@NotNull
	private Date applicationDate;

	@NotNull
	private Date liquidationDate;

	@NotNull
	private Date returnDate;

	@NotNull
	private Date sendTime;

	@NotNull
	private Date approvalTime;

	@NotNull
	private Date registerTime;

	@NotNull
	private Date notificationTime;

	@NotNull
	private EnumSchedulerType type;

	@NotNull
	private Date lastModifyDate;

	@NotNull
	private EnumLastModifyStatus lastModifyStatus;

	public Date getInitDate() {

		return initDate;
	}

	public void setInitDate(final Date initDate) {

		this.initDate = initDate;
	}

	public Date getEndDate() {

		return endDate;
	}

	public void setEndDate(final Date endDate) {

		this.endDate = endDate;
	}

	public EnumTimeUnit getTimeUnit() {

		return timeUnit;
	}

	public void setTimeUnit(final EnumTimeUnit timeUnit) {

		this.timeUnit = timeUnit;
	}

	public Integer getIterationNumber() {

		return iterationNumber;
	}

	public void setIterationNumber(final Integer iterationNumber) {

		this.iterationNumber = iterationNumber;
	}

	public EnumIterationDay getIterationDay() {

		return iterationDay;
	}

	public void setIterationDay(final EnumIterationDay iterationDay) {

		this.iterationDay = iterationDay;
	}

	public Date getExecutionDate() {

		return executionDate;
	}

	public void setExecutionDate(final Date executionDate) {

		this.executionDate = executionDate;
	}

	public Date getExpirationDate() {

		return expirationDate;
	}

	public void setExpirationDate(final Date expirationDate) {

		this.expirationDate = expirationDate;
	}

	public Date getLastOperationDate() {

		return lastOperationDate;
	}

	public void setLastOperationDate(final Date lastOperationDate) {

		this.lastOperationDate = lastOperationDate;
	}

	public Date getApplicationDate() {

		return applicationDate;
	}

	public void setApplicationDate(final Date applicationDate) {

		this.applicationDate = applicationDate;
	}

	public Date getLiquidationDate() {

		return liquidationDate;
	}

	public void setLiquidationDate(final Date liquidationDate) {

		this.liquidationDate = liquidationDate;
	}

	public Date getReturnDate() {

		return returnDate;
	}

	public void setReturnDate(final Date returnDate) {

		this.returnDate = returnDate;
	}

	public Date getSendTime() {

		return sendTime;
	}

	public void setSendTime(final Date sendTime) {

		this.sendTime = sendTime;
	}

	public Date getApprovalTime() {

		return approvalTime;
	}

	public void setApprovalTime(final Date approvalTime) {

		this.approvalTime = approvalTime;
	}

	public Date getRegisterTime() {

		return registerTime;
	}

	public void setRegisterTime(final Date registerTime) {

		this.registerTime = registerTime;
	}

	public Date getNotificationTime() {

		return notificationTime;
	}

	public void setNotificationTime(final Date notificationTime) {

		this.notificationTime = notificationTime;
	}

	public EnumSchedulerType getType() {

		return type;
	}

	public void setType(final EnumSchedulerType type) {

		this.type = type;
	}

	public Date getLastModifyDate() {

		return lastModifyDate;
	}

	public void setLastModifyDate(final Date lastModifyDate) {

		this.lastModifyDate = lastModifyDate;
	}

	public EnumLastModifyStatus getLastModifyStatus() {

		return lastModifyStatus;
	}

	public void setLastModifyStatus(final EnumLastModifyStatus lastModifyStatus) {

		this.lastModifyStatus = lastModifyStatus;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Scheduler)) {
			return false;
		}
		final Scheduler castOther = (Scheduler) other;
		return new EqualsBuilder().append(initDate, castOther.initDate).append(endDate, castOther.endDate)
				.append(timeUnit, castOther.timeUnit).append(iterationNumber, castOther.iterationNumber)
				.append(iterationDay, castOther.iterationDay).append(executionDate, castOther.executionDate)
				.append(expirationDate, castOther.expirationDate).append(lastOperationDate, castOther.lastOperationDate)
				.append(applicationDate, castOther.applicationDate).append(liquidationDate, castOther.liquidationDate)
				.append(returnDate, castOther.returnDate).append(sendTime, castOther.sendTime).append(approvalTime, castOther.approvalTime)
				.append(registerTime, castOther.registerTime).append(notificationTime, castOther.notificationTime)
				.append(type, castOther.type).append(lastModifyDate, castOther.lastModifyDate)
				.append(lastModifyStatus, castOther.lastModifyStatus).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(initDate).append(endDate).append(timeUnit).append(iterationNumber).append(iterationDay)
				.append(executionDate).append(expirationDate).append(lastOperationDate).append(applicationDate).append(liquidationDate)
				.append(returnDate).append(sendTime).append(approvalTime).append(registerTime).append(notificationTime).append(type)
				.append(lastModifyDate).append(lastModifyStatus).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("initDate", initDate).append("endDate", endDate).append("timeUnit", timeUnit)
				.append("iterationNumber", iterationNumber).append("iterationDay", iterationDay).append("executionDate", executionDate)
				.append("expirationDate", expirationDate).append("lastOperationDate", lastOperationDate)
				.append("applicationDate", applicationDate).append("liquidationDate", liquidationDate).append("returnDate", returnDate)
				.append("sendTime", sendTime).append("approvalTime", approvalTime).append("registerTime", registerTime)
				.append("notificationTime", notificationTime).append("type", type).append("lastModifyDate", lastModifyDate)
				.append("lastModifyStatus", lastModifyStatus).toString();
	}

}
