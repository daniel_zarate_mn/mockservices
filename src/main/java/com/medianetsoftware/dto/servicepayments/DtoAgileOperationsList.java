package com.medianetsoftware.dto.servicepayments;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoAgileOperation;
import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumAgileOperationType;
import com.medianetsoftware.dto.Pagination;

public class DtoAgileOperationsList {
	
	@NotNull
	private Pagination pagination;

//	@NotNull
//	private EnumAgileOperationType agileOperationType;
	
	@NotNull
	private List<DtoAgileOperation> items;

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

//	public EnumAgileOperationType getAgileOperationType() {
//		return agileOperationType;
//	}
//
//	public void setAgileOperationType(EnumAgileOperationType agileOperationType) {
//		this.agileOperationType = agileOperationType;
//	}

	public List<DtoAgileOperation> getItems() {
		return items;
	}

	public void setItems(List<DtoAgileOperation> items) {
		this.items = items;
	}
	
}
