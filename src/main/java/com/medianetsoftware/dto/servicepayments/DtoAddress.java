package com.medianetsoftware.dto.servicepayments;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.medianetsoftware.dto.Country;

@XmlRootElement(name = "address", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "address", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAddress {


	@NotNull
	private String id;


	@NotNull
	private String locate;


	@NotNull
	private String owner;


	@NotNull
	private String type;


	@NotNull
	private String streetType;


	@NotNull
	private String streetName;


	@NotNull
	private String streetNumber;


	@NotNull
	private String county;


	@NotNull
	private String neightborthood;


	@NotNull
	private String block;


	@NotNull
	private String stair;


	@NotNull
	private String floor;


	@NotNull
	private String door;


	@NotNull
	private String name;


	@NotNull
	private String startingResidenceName;


	@NotNull
	private String alias;


	@NotNull
	private String additionalInformation;


	@NotNull
	private String geographicGroup;


	@NotNull
	private String hasContractAssociated;


	@NotNull
	private String ownerShiptype;


	@NotNull
	private Country country;


	@NotNull
	private String city;


	@NotNull
	private String streetAddress;


	@NotNull
	private String state;


	@NotNull
	private Integer zipCode;

	public String getId() {

		return id;
	}

	public void setId(final String id) {

		this.id = id;
	}

	public String getLocate() {

		return locate;
	}

	public void setLocate(final String locate) {

		this.locate = locate;
	}

	public String getOwner() {

		return owner;
	}

	public void setOwner(final String owner) {

		this.owner = owner;
	}

	public String getType() {

		return type;
	}

	public void setType(final String type) {

		this.type = type;
	}

	public String getStreetType() {

		return streetType;
	}

	public void setStreetType(final String streetType) {

		this.streetType = streetType;
	}

	public String getStreetName() {

		return streetName;
	}

	public void setStreetName(final String streetName) {

		this.streetName = streetName;
	}

	public String getStreetNumber() {

		return streetNumber;
	}

	public void setStreetNumber(final String streetNumber) {

		this.streetNumber = streetNumber;
	}

	public String getCounty() {

		return county;
	}

	public void setCounty(final String county) {

		this.county = county;
	}

	public String getNeightborthood() {

		return neightborthood;
	}

	public void setNeightborthood(final String neightborthood) {

		this.neightborthood = neightborthood;
	}

	public String getBlock() {

		return block;
	}

	public void setBlock(final String block) {

		this.block = block;
	}

	public String getStair() {

		return stair;
	}

	public void setStair(final String stair) {

		this.stair = stair;
	}

	public String getFloor() {

		return floor;
	}

	public void setFloor(final String floor) {

		this.floor = floor;
	}

	public String getDoor() {

		return door;
	}

	public void setDoor(final String door) {

		this.door = door;
	}

	public String getName() {

		return name;
	}

	public void setName(final String name) {

		this.name = name;
	}

	public String getStartingResidenceName() {

		return startingResidenceName;
	}

	public void setStartingResidenceName(final String startingResidenceName) {

		this.startingResidenceName = startingResidenceName;
	}

	public String getAlias() {

		return alias;
	}

	public void setAlias(final String alias) {

		this.alias = alias;
	}

	public String getAdditionalInformation() {

		return additionalInformation;
	}

	public void setAdditionalInformation(final String additionalInformation) {

		this.additionalInformation = additionalInformation;
	}

	public String getGeographicGroup() {

		return geographicGroup;
	}

	public void setGeographicGroup(final String geographicGroup) {

		this.geographicGroup = geographicGroup;
	}

	public String getHasContractAssociated() {

		return hasContractAssociated;
	}

	public void setHasContractAssociated(final String hasContractAssociated) {

		this.hasContractAssociated = hasContractAssociated;
	}

	public String getOwnerShiptype() {

		return ownerShiptype;
	}

	public void setOwnerShiptype(final String ownerShiptype) {

		this.ownerShiptype = ownerShiptype;
	}

	public Country getCountry() {

		return country;
	}

	public void setCountry(final Country country) {

		this.country = country;
	}

	public String getCity() {

		return city;
	}

	public void setCity(final String city) {

		this.city = city;
	}

	public String getStreetAddress() {

		return streetAddress;
	}

	public void setStreetAddress(final String streetAddress) {

		this.streetAddress = streetAddress;
	}

	public String getState() {

		return state;
	}

	public void setState(final String state) {

		this.state = state;
	}

	public Integer getZipCode() {

		return zipCode;
	}

	public void setZipCode(final Integer zipCode) {

		this.zipCode = zipCode;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoAddress)) {
			return false;
		}
		final DtoAddress castOther = (DtoAddress) other;
		return new EqualsBuilder().append(id, castOther.id).append(locate, castOther.locate).append(owner, castOther.owner)
				.append(type, castOther.type).append(streetType, castOther.streetType).append(streetName, castOther.streetName)
				.append(streetNumber, castOther.streetNumber).append(county, castOther.county)
				.append(neightborthood, castOther.neightborthood).append(block, castOther.block).append(stair, castOther.stair)
				.append(floor, castOther.floor).append(door, castOther.door).append(name, castOther.name)
				.append(startingResidenceName, castOther.startingResidenceName).append(alias, castOther.alias)
				.append(additionalInformation, castOther.additionalInformation).append(geographicGroup, castOther.geographicGroup)
				.append(hasContractAssociated, castOther.hasContractAssociated).append(ownerShiptype, castOther.ownerShiptype)
				.append(country, castOther.country).append(city, castOther.city).append(streetAddress, castOther.streetAddress)
				.append(state, castOther.state).append(zipCode, castOther.zipCode).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(locate).append(owner).append(type).append(streetType).append(streetName)
				.append(streetNumber).append(county).append(neightborthood).append(block).append(stair).append(floor).append(door)
				.append(name).append(startingResidenceName).append(alias).append(additionalInformation).append(geographicGroup)
				.append(hasContractAssociated).append(ownerShiptype).append(country).append(city).append(streetAddress).append(state)
				.append(zipCode).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("locate", locate).append("owner", owner).append("type", type)
				.append("streetType", streetType).append("streetName", streetName).append("streetNumber", streetNumber)
				.append("county", county).append("neightborthood", neightborthood).append("block", block).append("stair", stair)
				.append("floor", floor).append("door", door).append("name", name).append("startingResidenceName", startingResidenceName)
				.append("alias", alias).append("additionalInformation", additionalInformation).append("geographicGroup", geographicGroup)
				.append("hasContractAssociated", hasContractAssociated).append("ownerShiptype", ownerShiptype).append("country", country)
				.append("city", city).append("streetAddress", streetAddress).append("state", state).append("zipCode", zipCode).toString();
	}

}
