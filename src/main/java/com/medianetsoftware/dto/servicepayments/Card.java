package com.medianetsoftware.dto.servicepayments;

import java.io.Serializable;

import com.medianetsoftware.dto.DebitCard;
import com.medianetsoftware.dto.ProductBase;

public class Card implements Serializable {
	
	private static final long serialVersionUID = -8004141591404730556L;

	private ProductBase productBase;

	private DebitCard debitCard;

	public ProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(ProductBase productBase) {
		this.productBase = productBase;
	}

	public DebitCard getDebitCard() {
		return debitCard;
	}

	public void setDebitCard(DebitCard debitCard) {
		this.debitCard = debitCard;
	}
	
	
}
