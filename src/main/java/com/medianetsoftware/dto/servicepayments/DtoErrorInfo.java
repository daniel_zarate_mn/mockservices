package com.medianetsoftware.dto.servicepayments;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "errorInfo", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "errorInfo", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoErrorInfo {

	@NotNull
	private String errorCode;

	@NotNull
	private String errorMessage;

	public String getErrorCode() {

		return errorCode;
	}

	public void setErrorCode(final String errorCode) {

		this.errorCode = errorCode;
	}

	public String getErrorMessage() {

		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {

		this.errorMessage = errorMessage;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoErrorInfo)) {
			return false;
		}
		final DtoErrorInfo castOther = (DtoErrorInfo) other;
		return new EqualsBuilder().append(errorCode, castOther.errorCode).append(errorMessage, castOther.errorMessage).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(errorCode).append(errorMessage).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("errorCode", errorCode).append("errorMessage", errorMessage).toString();
	}

}
