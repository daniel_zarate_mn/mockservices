package com.medianetsoftware.dto.servicepayments;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.medianetsoftware.dto.FinancialValues;

@XmlRootElement(name = "sender", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "sender", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoSender {

	@NotNull
	private Contract contract;

	@NotNull
	private FinancialValues charge;

	@NotNull
	private String sender;

	public Contract getContract() {

		return contract;
	}

	public void setContract(final Contract contract) {

		this.contract = contract;
	}

	public FinancialValues getCharge() {

		return charge;
	}

	public void setCharge(final FinancialValues charge) {

		this.charge = charge;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(final String sender) {
		this.sender = sender;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoSender)) {
			return false;
		}
		final DtoSender castOther = (DtoSender) other;
		return new EqualsBuilder().append(contract, castOther.contract).append(charge, castOther.charge).append(sender, castOther.sender)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(contract).append(charge).append(sender).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("contract", contract).append("charge", charge).append("sender", sender).toString();
	}

}
