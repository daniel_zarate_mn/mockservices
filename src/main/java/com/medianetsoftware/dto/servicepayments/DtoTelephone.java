package com.medianetsoftware.dto.servicepayments;



import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.medianetsoftware.dto.enums.EnumPhoneType;

@XmlRootElement(name = "telephone", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "telephone", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoTelephone {

	@NotNull
	private Integer telephoneNumber;

	@NotNull
	private Integer areaCode;

	@NotNull
	private Integer phoneExtension;

	@NotNull
	private EnumPhoneType type;

	@NotNull
	private DtoCellphoneCompany cellphoneCompany;

	public Integer getTelephoneNumber() {

		return telephoneNumber;
	}

	public void setTelephoneNumber(final Integer telephoneNumber) {

		this.telephoneNumber = telephoneNumber;
	}

	public Integer getAreaCode() {

		return areaCode;
	}

	public void setAreaCode(final Integer areaCode) {

		this.areaCode = areaCode;
	}

	public Integer getPhoneExtension() {

		return phoneExtension;
	}

	public void setPhoneExtension(final Integer phoneExtension) {

		this.phoneExtension = phoneExtension;
	}

	public EnumPhoneType getType() {

		return type;
	}

	public void setType(final EnumPhoneType type) {

		this.type = type;
	}

	public DtoCellphoneCompany getCellphoneCompany() {

		return cellphoneCompany;
	}

	public void setCellphoneCompany(final DtoCellphoneCompany cellphoneCompany) {

		this.cellphoneCompany = cellphoneCompany;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoTelephone)) {
			return false;
		}
		final DtoTelephone castOther = (DtoTelephone) other;
		return new EqualsBuilder().append(telephoneNumber, castOther.telephoneNumber).append(areaCode, castOther.areaCode)
				.append(phoneExtension, castOther.phoneExtension).append(type, castOther.type)
				.append(cellphoneCompany, castOther.cellphoneCompany).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(telephoneNumber).append(areaCode).append(phoneExtension).append(type).append(cellphoneCompany)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("telephoneNumber", telephoneNumber).append("areaCode", areaCode)
				.append("phoneExtension", phoneExtension).append("type", type).append("cellphoneCompany", cellphoneCompany).toString();
	}

}
