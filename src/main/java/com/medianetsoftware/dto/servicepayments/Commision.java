package com.medianetsoftware.dto.servicepayments;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "commision", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "commision", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Commision {

	@NotNull
	private String id;

	@NotNull
	private Money amount;

	@NotNull
	private String creditLineCommWithdrawalCash;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(final Money amount) {
		this.amount = amount;
	}

	public String getCreditLineCommWithdrawalCash() {
		return creditLineCommWithdrawalCash;
	}

	public void setCreditLineCommWithdrawalCash(final String creditLineCommWithdrawalCash) {
		this.creditLineCommWithdrawalCash = creditLineCommWithdrawalCash;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Commision)) {
			return false;
		}
		final Commision castOther = (Commision) other;
		return new EqualsBuilder().append(id, castOther.id).append(amount, castOther.amount)
				.append(creditLineCommWithdrawalCash, castOther.creditLineCommWithdrawalCash).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(amount).append(creditLineCommWithdrawalCash).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("amount", amount)
				.append("creditLineCommWithdrawalCash", creditLineCommWithdrawalCash).toString();
	}

}
