package com.medianetsoftware.dto.servicepayments;

import com.medianetsoftware.dto.IAccount;
import com.medianetsoftware.dto.IProduct;

public class Product implements IProduct{

	private IAccount account2;
	
	private Account account;
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public IAccount getAccount2() {
		return account2;
	}

	public void setAccount2(IAccount account2) {
		this.account2 = account2;
	}
	
}
