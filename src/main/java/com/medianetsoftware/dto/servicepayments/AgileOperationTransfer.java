package com.medianetsoftware.dto.servicepayments;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.medianetsoftware.dto.FinancialValues;
import com.medianetsoftware.dto.enums.EnumSchedulerType;


@XmlRootElement(name = "agileOperationTransfer", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "agileOperationTransfer", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgileOperationTransfer {

	@NotNull
	private Sender sender;

	@NotNull
	private Receiver receiver;

	@NotNull
	private String reasonPayment;

	@NotNull
	private String client;

	@NotNull
	private Boolean token;

	@NotNull
	private EnumSchedulerType typeScheduler;

	@NotNull
	private FinancialValues amount;

	@NotNull
	private String concept;

	public Sender getSender() {
		return sender;
	}

	public void setSender(final Sender sender) {
		this.sender = sender;
	}

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(final Receiver receiver) {
		this.receiver = receiver;
	}

	public String getReasonPayment() {
		return reasonPayment;
	}

	public void setReasonPayment(final String reasonPayment) {
		this.reasonPayment = reasonPayment;
	}

	public String getClient() {
		return client;
	}

	public void setClient(final String client) {
		this.client = client;
	}

	public Boolean getToken() {
		return token;
	}

	public void setToken(final Boolean token) {
		this.token = token;
	}

	public EnumSchedulerType getTypeScheduler() {
		return typeScheduler;
	}

	public void setTypeScheduler(final EnumSchedulerType typeScheduler) {
		this.typeScheduler = typeScheduler;
	}

	public FinancialValues getAmount() {
		return amount;
	}

	public void setAmount(final FinancialValues amount) {
		this.amount = amount;
	}

	public String getConcept() {
		return concept;
	}

	public void setConcept(final String concept) {
		this.concept = concept;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AgileOperationTransfer)) {
			return false;
		}
		final AgileOperationTransfer castOther = (AgileOperationTransfer) other;
		return new EqualsBuilder().append(sender, castOther.sender).append(receiver, castOther.receiver)
				.append(reasonPayment, castOther.reasonPayment).append(client, castOther.client).append(token, castOther.token)
				.append(typeScheduler, castOther.typeScheduler).append(amount, castOther.amount).append(concept, castOther.concept)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(sender).append(receiver).append(reasonPayment).append(client).append(token)
				.append(typeScheduler).append(amount).append(concept).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("sender", sender).append("receiver", receiver).append("reasonPayment", reasonPayment)
				.append("client", client).append("token", token).append("typeScheduler", typeScheduler).append("amount", amount)
				.append("concept", concept).toString();
	}

}
