package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(name = "transfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "transfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transfers {

	@NotNull
	private List<Transfer> listTransfer;

	@NotNull
	private Pagination pagination;

	public List<Transfer> getListTransfer() {
		return listTransfer;
	}

	public void setListTransfer(final List<Transfer> listTransfer) {
		this.listTransfer = listTransfer;
	}

	public void setPagination(final Pagination pagination) {
		this.pagination = pagination;
	}

	public Pagination getPagination() {
		return pagination;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Transfers)) {
			return false;
		}
		final Transfers castOther = (Transfers) other;
		return new EqualsBuilder().append(listTransfer, castOther.listTransfer).append(pagination, castOther.pagination).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(listTransfer).append(pagination).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("listTransfer", listTransfer).append("pagination", pagination).toString();
	}

}
