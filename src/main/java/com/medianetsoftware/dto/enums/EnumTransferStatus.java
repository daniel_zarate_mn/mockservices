package com.medianetsoftware.dto.enums;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@JsonSerialize
public enum EnumTransferStatus {

	ALL("TD"), EXPIRE("CD"), CANCELED("CN"), PERFORMED("CO"), PENDING("VG"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumTransferStatus.class);

	private String key;

	EnumTransferStatus(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumTransferStatus getEnumValue(final String key) {
		for (final EnumTransferStatus transferStatus : values()) {
			if (transferStatus.key.equals(key)) {
				return transferStatus;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumTransferStatus enumIntTransferStatus : values()) {
			if (enumIntTransferStatus.name().equalsIgnoreCase(key)) {
				return enumIntTransferStatus.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}
}
