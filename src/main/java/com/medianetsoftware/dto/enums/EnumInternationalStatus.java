package com.medianetsoftware.dto.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumInternationalStatus {

	UNSUCCESSFUL("X"), PENDING_LIQUIDATION("X"), LIQUIDATED_PENDING_SWIFT("X"), LIQUIDATED_DELIVERED_SWIFT("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumInternationalStatus.class);

	private String key;

	EnumInternationalStatus(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumInternationalStatus getEnumValue(final String key) {
		for (final EnumInternationalStatus internationalStatus : values()) {
			if (internationalStatus.key.equals(key)) {
				return internationalStatus;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumInternationalStatus internationalStatus : values()) {
			if (internationalStatus.name().equalsIgnoreCase(key)) {
				return internationalStatus.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
