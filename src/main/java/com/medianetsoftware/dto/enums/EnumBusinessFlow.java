package com.medianetsoftware.dto.enums;

public enum EnumBusinessFlow {

	INTERNATIONAL, INTERBANK, MOBILEMONEY, THIRD_PARTY, MY_ACCOUNTS, SERVICEPAYMENT, EXPRESS, PREPAID;

}
