package com.medianetsoftware.dto.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumSchedulerType {

	SCHEDULED("X"), PERIODIC("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumSchedulerType.class);

	private String key;

	EnumSchedulerType(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumSchedulerType getEnumValue(final String key) {
		for (final EnumSchedulerType typeScheduler : values()) {
			if (typeScheduler.key.equals(key)) {
				return typeScheduler;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumSchedulerType typeScheduler : values()) {
			if (typeScheduler.name().equalsIgnoreCase(key)) {
				return typeScheduler.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}
}
