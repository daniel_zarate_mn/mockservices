package com.medianetsoftware.dto.enums;

public enum EnumCurrencyCode {

	MXP, USD, EUR

}
