package com.medianetsoftware.dto.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumIterationDay {

	MONDAY("X"), TUESDAY("X"), WEDNESDAY("X"), THURSDAY("X"), FRIDAY("X"), SATURDAY("X"), SUNDAY("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumIterationDay.class);

	private String key;

	EnumIterationDay(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumIterationDay getEnumValue(final String key) {
		for (final EnumIterationDay iterationDay : values()) {
			if (iterationDay.key.equals(key)) {
				return iterationDay;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumIterationDay iterationDay : values()) {
			if (iterationDay.name().equalsIgnoreCase(key)) {
				return iterationDay.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
