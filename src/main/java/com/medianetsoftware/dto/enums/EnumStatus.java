package com.medianetsoftware.dto.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumStatus {

	ACTIVE("X"), CANCEL("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumStatus.class);

	private String key;

	EnumStatus(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumStatus getEnumValue(final String key) {
		for (final EnumStatus status : values()) {
			if (status.key.equals(key)) {
				return status;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumStatus status : values()) {
			if (status.name().equalsIgnoreCase(key)) {
				return status.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}
}
