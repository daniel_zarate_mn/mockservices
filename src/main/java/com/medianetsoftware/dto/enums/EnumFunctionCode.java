package com.medianetsoftware.dto.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumFunctionCode {

	/**
	 * PREREGISTER
	 */

	ORDENES_DE_PAGO_INTERNACIONALES("I6F1"), 
	TRASPASO_DE_CHEQUE_A_CHEQUE_TERCEROS("I340"), 
	TRASPASO_DE_CHEQUE_A_CUENTA_EXPRESS_TERCEROS("I345"), 
	TRASPASO_DE_TERCEROS_BANCOMER_TARJETA_CREDITO("I350"), 
	TRASPASO_DE_CHEQUE_A_TARJETA_PREPAGO_EUR_USD_MXP_TERCEROS_PREREGISTRO("I314"), 
	TRASPASO_DE_CHEQUE_A_TARJETA_PREPAGO_EUR_USD_MXP_TERCEROS_FRECUENTES("I315"), 
	TRASPASO_DE_CHEQUE_A_TARJETA_CREDITO_OTROS_BANCOS("I325"), 
	PAGO_CIE("I380"),
	PAGO_CONVENIOS_CIE_MONEDA_EXTRANJERA("I301"),
	TRANSFERENCIAS_INTERBANCARIAS("I390"), TRASFERENCIA_HIPOTECARIO_INTERBANCARIA("I394"), 
	TRASFERENCIA_AUTOMOTRIZ_INTERBANCARIA("I395"), 
	TRASFERENCIA_PERSONAL_INTERBANCARIA("I396"), 
	ALTA_DE_DINERO_MOVIL("I6E0"),
	CONSULTA_DE_CUENTAS_PREREGISTRADAS("I8R1"),
	CONSULTA_DE_CUENTAS_PREREGISTRADAS_PAGO_DE_SERVICIOS("I8R7"),

	


	/**
	 * QUICK
	 */
	ALTA_DE_PAGOS_RAPIDOS("I8T1"),
	CONSULTA_DE_PAGOS_RAPIDOS("I8R5"),

	NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumFunctionCode.class);

	private String key;

	EnumFunctionCode(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumFunctionCode getEnumValue(final String key) {
		for (final EnumFunctionCode functionCodeAgileOperations : values()) {
			if (functionCodeAgileOperations.key.equals(key)) {
				return functionCodeAgileOperations;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumFunctionCode functionCodeAgileOperations : values()) {
			if (functionCodeAgileOperations.name().equalsIgnoreCase(key)) {
				return functionCodeAgileOperations.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
