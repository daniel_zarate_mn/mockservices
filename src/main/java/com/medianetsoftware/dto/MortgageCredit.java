package com.medianetsoftware.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(name = "mortgageCredit", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "mortgageCredit", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class MortgageCredit implements ILoan {

	@NotNull
	private ProductBase productBase;

	@NotNull
	private List<Contract> associatedContracts;

	@NotNull
	private String idContract;

	@NotNull
	private Float nationalMoneyBalance;

	public ProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final ProductBase productBase) {
		this.productBase = productBase;
	}

	public List<Contract> getAssociatedContracts() {
		return associatedContracts;
	}

	public void setAssociatedContracts(final List<Contract> associatedContracts) {
		this.associatedContracts = associatedContracts;
	}

	public String getIdContract() {
		return idContract;
	}

	public void setIdContract(final String idContract) {
		this.idContract = idContract;
	}

	public Float getNationalMoneypBalance() {
		return nationalMoneyBalance;
	}

	public void setNationalMoneypBalance(final Float nationalMoneypBalance) {
		this.nationalMoneyBalance = nationalMoneypBalance;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MortgageCredit)) {
			return false;
		}
		final MortgageCredit castOther = (MortgageCredit) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(associatedContracts, castOther.associatedContracts)
				.append(idContract, castOther.idContract).append(nationalMoneyBalance, castOther.nationalMoneyBalance).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(associatedContracts).append(idContract).append(nationalMoneyBalance)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("associatedContracts", associatedContracts)
				.append("idContract", idContract).append("nationalMoneypBalance", nationalMoneyBalance).toString();
	}

}
