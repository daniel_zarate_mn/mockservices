package com.medianetsoftware.builders;

import java.util.Date;

import com.bbvabancomer.andrea.beans.transfer.dto.DtoAdditionalInfoTransfer;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoAuthorizationInfo;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoBalance;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoCommision;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoEntityId;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoErrorInfo;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoMoney;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoOperationResult;
import com.medianetsoftware.dto.AdditionalInfoTransfer;
import com.medianetsoftware.dto.AuthorizationInfo;
import com.medianetsoftware.dto.Balance;
import com.medianetsoftware.dto.Commision;
import com.medianetsoftware.dto.Currency;
import com.medianetsoftware.dto.EntityId;
import com.medianetsoftware.dto.ErrorInfo;
import com.medianetsoftware.dto.Money;
import com.medianetsoftware.dto.OperationResult;


/**
 * @author MedianetMx
 *
 */
public class OperationResultBuilder {
	
	public static OperationResult build() {
		OperationResult opResult = new OperationResult();

        //operation date
        opResult.setOperationDate(new Date());

        //setting balance sender
        Balance balanceSender = new Balance();

        Money balanceSenderAMT = new Money();
        balanceSenderAMT.setAmount(9.9);
        Currency pendingBalanceCurrency = new Currency();
        pendingBalanceCurrency.setCode("MXP");
        pendingBalanceCurrency.setId("S");
        pendingBalanceCurrency.setName("S");
        balanceSenderAMT.setCurrency(pendingBalanceCurrency);
        balanceSender.setPendingBalance(balanceSenderAMT);
        balanceSender.setBalance(balanceSenderAMT);
        balanceSender.setCuttingBalance(balanceSenderAMT);
        balanceSender.setTotalBalance(balanceSenderAMT);
        balanceSender.setGrantedBalance(balanceSenderAMT);
        balanceSender.setCurrentBalance(balanceSenderAMT);
        balanceSender.setBalanceAtDate(balanceSenderAMT);
        balanceSender.setAvailableBalance(balanceSenderAMT);

        opResult.setFinalBalanceSender(balanceSender);

        // setting authorizationInfo
        AuthorizationInfo authorizationInfo = new AuthorizationInfo();
        authorizationInfo.setReceiverAuthNumber("S");
        authorizationInfo.setSenderAuthNumber("S");
        opResult.setAuthorizationInfo(authorizationInfo);

        // setting final balance receiver
        Balance balanceReceiver = new Balance();

        Money balanceReceiverAMT = new Money();
        balanceReceiverAMT.setAmount(9.9);
        Currency pendingBalanceReceiverCurrency = new Currency();
        pendingBalanceReceiverCurrency.setCode("S");
        pendingBalanceReceiverCurrency.setId("S");
        pendingBalanceReceiverCurrency.setName("S");
        balanceReceiverAMT.setCurrency(pendingBalanceReceiverCurrency);
        balanceReceiver.setPendingBalance(balanceReceiverAMT);
        balanceReceiver.setBalance(balanceReceiverAMT);
        balanceReceiver.setCuttingBalance(balanceReceiverAMT);
        balanceReceiver.setTotalBalance(balanceReceiverAMT);
        balanceReceiver.setGrantedBalance(balanceReceiverAMT);
        balanceReceiver.setCurrentBalance(balanceReceiverAMT);
        balanceReceiver.setBalanceAtDate(balanceReceiverAMT);
        balanceReceiver.setAvailableBalance(balanceReceiverAMT);

        opResult.setFinalBalanceReceiver(balanceReceiver);

        //setting execution date
        opResult.setExecutionDate(new Date());

        //setting addtional info
        AdditionalInfoTransfer infoTransfer = new AdditionalInfoTransfer();
        Money moneyTrans = new Money();
        moneyTrans.setAmount(9.9);
        Currency currencyTrans = new Currency();
        currencyTrans.setCode("S");
        currencyTrans.setId("S");
        currencyTrans.setName("S");
        moneyTrans.setCurrency(currencyTrans);
        infoTransfer.setDepositAmount(moneyTrans);
        infoTransfer.setPercentCommissionWithdrawal(9);
        Commision commisionWithdrawalCash = new Commision();
        commisionWithdrawalCash.setId("S");
        commisionWithdrawalCash.setCreditLineCommWithdrawalCash("S");
        commisionWithdrawalCash.setAmount(moneyTrans);
        infoTransfer.setCommisionWithdrawalCash(commisionWithdrawalCash);
        infoTransfer.setExtractNumber("S");
        infoTransfer.setChargeAmount(moneyTrans);
        infoTransfer.setMovementNumber("S");
        infoTransfer.setCancellationReference("S");
        opResult.setAdditionalInfo(infoTransfer);

        //setting errorInfo
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("Error Code");
        errorInfo.setErrorMessage("Error Message");
//        opResult.setErrorInfo(errorInfo);

        //entityId
        EntityId entityId = new EntityId();
        entityId.setSecurityCode("S");
        entityId.setMainTransactionId("S");
        entityId.setApplicationId("S");
        entityId.setApplicationId("S");
        entityId.setServicePaymentId("S");
        entityId.setTrackingId("S");
        opResult.setEntityId(entityId);

        opResult.setId("S");
        opResult.setExpirationDate(new Date());
		return opResult;
	}
	
	/**
	 * Construye salidas para los servicios de createInternalTransfers
	 * 
	 * @param tipo Tipo de traspaso que se esta realizando
	 * @return
	 */
	public static DtoOperationResult internalTransferBuild(String tipo) {
		return buildOutputWYJC();
	}
	
	
	/**
	 * Construye respuesta para una transacción entreMisCuentas -> terceros, express
	 * 
	 * @return
	 */
	private static DtoOperationResult buildOutputWYJC() {
		DtoOperationResult opResult = new DtoOperationResult();
		
		//Autorizathion info - Opcional
		DtoAuthorizationInfo authorizationInfo = new DtoAuthorizationInfo();
		authorizationInfo.setReceiverAuthNumber("receiverAuthNumber");
		authorizationInfo.setSenderAuthNumber("senderAuthNumber");
		
		opResult.setAuthorizationInfo(authorizationInfo);
		
		//OperationDate
		opResult.setOperationDate(new Date());
		
		//Execution Date - Opcional
		opResult.setExecutionDate(new Date());
		
		//ID
		opResult.setId("ID: sesion + numOperacion");
		
		//Error Info - Opcional
		DtoErrorInfo errorInfo = new DtoErrorInfo();
		errorInfo.setErrorCode("Error code");
		
//		opResult.setErrorInfo(errorInfo);  //Solo setearlo cuando se quiera probar error.
		
		//Final Balance Sender
		DtoBalance finalBalanceSender = new DtoBalance();
		DtoMoney moneySender = new DtoMoney();
		moneySender.setAmount(123.00);
		finalBalanceSender.setAvailableBalance(moneySender);
		
		opResult.setFinalBalanceSender(finalBalanceSender);
		
		//Final Balance Receiver - Opcional
		DtoBalance finalBalanceReceiver = new DtoBalance();
		DtoMoney moneyeReceiver = new DtoMoney();
		moneyeReceiver.setAmount(123.00);
		finalBalanceReceiver.setAvailableBalance(moneyeReceiver);
		
		opResult.setFinalBalanceReceiver(finalBalanceReceiver);
		
		return opResult;
	}
	
	/**
	 * Construye respuesta para una transacción entreMisCuentas -> monedaExtranjera
	 * 
	 * @return
	 */
	private static DtoOperationResult buildOutputWYKB() {
		DtoOperationResult opResult = new DtoOperationResult();
		
		//Authorization Info - Opcional
		DtoAuthorizationInfo authorizationInfo = new DtoAuthorizationInfo();
		authorizationInfo.setReceiverAuthNumber("receiverAuthNumber");
		authorizationInfo.setSenderAuthNumber("senderAuthNumber");
		
		opResult.setAuthorizationInfo(authorizationInfo);
		
		//Operation Date - Opcional
		opResult.setOperationDate(new Date());
		
		//Execution Date - Opcional
		opResult.setExecutionDate(new Date());
		
		//ID - Opcional
		opResult.setId("ID: sesion + numOperacion");
		
		//Final Balance Sender - Opcional
		DtoBalance finalBalanceSender = new DtoBalance();
		DtoMoney moneySender = new DtoMoney();
		moneySender.setAmount(123.00);
		finalBalanceSender.setAvailableBalance(moneySender);
		
		opResult.setFinalBalanceSender(finalBalanceSender);
		
		//Final Balance Receiver - Opcional
		DtoBalance finalBalanceReceiver = new DtoBalance();
		DtoMoney moneyeReceiver = new DtoMoney();
		moneyeReceiver.setAmount(123.00);
		finalBalanceReceiver.setAvailableBalance(moneyeReceiver);
		
		opResult.setFinalBalanceReceiver(finalBalanceReceiver);
		
		return opResult;
	}
	
	/**
	 * Construye respuesta para una transacción entreMisCuentas, terceros -> TDC
	 * 
	 * @return
	 */
	private static DtoOperationResult buildOutputWYLF() {
		DtoOperationResult opResult = new DtoOperationResult();
		
		//Operation Date
		opResult.setOperationDate(new Date());
		
		//ID
		opResult.setId("ID: sesion + numOperacion");
		
		//Final Balance Sender
		DtoBalance finalBalanceSender = new DtoBalance();
		DtoMoney moneySender = new DtoMoney();
		moneySender.setAmount(123.00);
		finalBalanceSender.setAvailableBalance(moneySender);
		DtoMoney totalBalanceSender = new DtoMoney();
		totalBalanceSender.setAmount(321.00);
		finalBalanceSender.setTotalBalance(totalBalanceSender);
		
		opResult.setFinalBalanceSender(finalBalanceSender);
		
		//Final Balance Receiver
		DtoBalance finalBalanceReceiver = new DtoBalance();
		DtoMoney moneyeReceiver = new DtoMoney();
		moneyeReceiver.setAmount(123.00);
		finalBalanceReceiver.setAvailableBalance(moneyeReceiver);
		
		opResult.setFinalBalanceReceiver(finalBalanceReceiver);
		
		//Authorizathion Info
		DtoAuthorizationInfo authorizationInfo = new DtoAuthorizationInfo();
		authorizationInfo.setSenderAuthNumber("senderAuthNumber");
		
		opResult.setAuthorizationInfo(authorizationInfo);
		
		//Entity ID
		DtoEntityId entityId = new DtoEntityId();
		entityId.setMainTransactionId("mainTransactionId");
		
		opResult.setEntityId(entityId);
		
		//Execution Date
		opResult.setExecutionDate(new Date());
		
		//Additional Info Transfer  - Opcional
		DtoAdditionalInfoTransfer additionalInfo = new DtoAdditionalInfoTransfer();
		DtoCommision commisionWithdrawalCash = new DtoCommision();
		DtoMoney amoutCommisionWithdrawalCash = new DtoMoney();
		amoutCommisionWithdrawalCash.setAmount(345.00);
		commisionWithdrawalCash.setAmount(amoutCommisionWithdrawalCash);
		additionalInfo.setCommisionWithdrawalCash(commisionWithdrawalCash);
		additionalInfo.setPercentCommissionWithdrawal(16);
		additionalInfo.setExtractNumber("extractNumber");
		additionalInfo.setMovementNumber("movementNumber");
		additionalInfo.setCancellationReference("cancellationReference");
		
		opResult.setAdditionalInfo(additionalInfo);
		
		//commision  - Opcional
		/****** No hay campo en el DTO para comision *********/
		
		return opResult;
	}
	
	/**
	 * Construye respuesta para una transacción MonedaExtranjera -> TPP
	 * 
	 * @return
	 */
	private static DtoOperationResult buildOutputWYLT() {
		DtoOperationResult opResult = new DtoOperationResult();
		
		//Additional Info - Opcional
		DtoAdditionalInfoTransfer additionalInfo = new DtoAdditionalInfoTransfer();
		DtoMoney chargeAmount = new DtoMoney();
		chargeAmount.setAmount(456.00);
		additionalInfo.setChargeAmount(chargeAmount);
		DtoMoney depositAmount = new DtoMoney();
		depositAmount.setAmount(456.00);
		additionalInfo.setDepositAmount(depositAmount);
		
		opResult.setAdditionalInfo(additionalInfo);
		
		//Id - Opcional
		opResult.setId("ID: sesion + numOperacion");
		
		//Operation Date - Opcional
		opResult.setOperationDate(new Date());
		
		//Execution Date - Opcional
		opResult.setExecutionDate(new Date());
		
		//Entity Id - Opcional
		DtoEntityId entityId = new DtoEntityId();
		entityId.setMainTransactionId("mainTransactionId");

		opResult.setEntityId(entityId);
		
		return opResult;
	}

}
