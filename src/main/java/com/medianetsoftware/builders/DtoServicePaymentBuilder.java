package com.medianetsoftware.builders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.medianetsoftware.dto.ExpressAccount;
import com.medianetsoftware.dto.servicepayments.Account;
import com.medianetsoftware.dto.servicepayments.Bank;
import com.medianetsoftware.dto.servicepayments.Check;
import com.medianetsoftware.dto.servicepayments.Contract;
import com.medianetsoftware.dto.servicepayments.Currency;
import com.medianetsoftware.dto.servicepayments.DigitalTaxCertificate;
import com.medianetsoftware.dto.servicepayments.DtoCommission;
import com.medianetsoftware.dto.servicepayments.DtoServicePayment;
import com.medianetsoftware.dto.servicepayments.Money;
import com.medianetsoftware.dto.servicepayments.Product;
import com.medianetsoftware.dto.servicepayments.Receiver;
import com.medianetsoftware.dto.servicepayments.Sender;
import com.medianetsoftware.dto.servicepayments.Tax;

public class DtoServicePaymentBuilder {

	public static DtoServicePayment build() {
		DtoServicePayment servicePayment = new DtoServicePayment();
		Money money = new Money();
		Currency currency = new Currency();
		currency.setId("1");
		currency.setName("Dolar Mex");
		currency.setCode("$");
		money.setCurrency(currency);		
		
		List<Check> checksNumber = new ArrayList<Check>();
		Check check = new Check();
		check.setAmount(money);
		check.setAvailability("Y");
		check.setBR6digits("123456");
		check.setCheckNumber("12");
		check.setDocumentData("document dataaaa");
		check.setSecurityCode("4321");
		checksNumber.add(check);
		servicePayment.setChecksNumber(checksNumber);
		servicePayment.setCompanyID("466834333");
		servicePayment.setAmount(money);
		DtoCommission commission = new DtoCommission();
		commission.setCreditLineCommissionWithdrawingCash("Y");
		commission.setAmount(money);
		
		servicePayment.setCommission(commission);
		servicePayment.setConcept("Concept");
		DigitalTaxCertificate digitalTaxCertificate = new DigitalTaxCertificate();
		Tax tax = new Tax();
		tax.setDescription("description");
		tax.setValue(money);
		commission.setTax(tax);
		digitalTaxCertificate.setTax(tax);
		digitalTaxCertificate.setTaxpayer("taxpayer");
		servicePayment.setDigitalTaxCertificate(digitalTaxCertificate);
		//18-Marzo-2016
		servicePayment.setExpirationDate(new Date(1458269184992L));
		servicePayment.setId("2354234234234");
		servicePayment.setNotificationDate(new Date(1458269184992L));
		servicePayment.setOperationDate(new Date(1458269184992L));
		servicePayment.setPaymentStatus("paymentStatus");
		servicePayment.setPaymentType("servicePayment");
		Receiver receiver = new Receiver();
		
		receiver.setName("receiver");
		Bank bank = new Bank();
		bank.setId("23423423");
		receiver.setBank(bank);
		receiver.setEmail("mail@mail.com");		
		
		ExpressAccount expressAccount = new ExpressAccount();
		expressAccount.setCellphoneNumber("666777888");
	
		Account account = new Account();
		account.setExpressAccount(expressAccount);
		Product product = new Product();		
		product.setAccount(account);
		Contract contract = new Contract();
		contract.setProduct(product);
		receiver.setContract(contract);
		servicePayment.setReceiver(receiver);
		
		servicePayment.setReference("468486468468541");
		Sender sender = new Sender();
		sender.setContract(contract);
		servicePayment.setSender(sender);
		return servicePayment;
	}
}
