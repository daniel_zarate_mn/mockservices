package com.medianetsoftware.filters;


import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Daniel Zarate on 15/10/2015.
 *
 * This class add a http header named tsec in each json response
 */
@Component
public class TSECFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("tsec", "NDE1MjMxMDAzNTg2MTQzNUFETUlORjowMDAwMDAwMA==");

        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
