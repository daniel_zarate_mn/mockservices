package com.medianetsoftware.endpoints;


import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bbvabancomer.andrea.beans.transfer.dto.DtoOperationResult;
import com.medianetsoftware.builders.OperationResultBuilder;

/**
 * Created by Daniel Zarate on 13/10/2015.
 *
 * This class emulate the internalTransfer web service and only returns a fake json response
 */
@RestController
public class InternalTransfersController {

    @RequestMapping(value = "/internalTransfers/V01", method = RequestMethod.POST)
    public DtoOperationResult internalTransfersV01(@RequestBody String transfer){
    	
        Logger log = Logger.getLogger(InternalTransfersController.class);
        log.debug(transfer);

//        return OperationResultBuilder.build();
        return OperationResultBuilder.internalTransferBuild("WYJC");
    }	
}