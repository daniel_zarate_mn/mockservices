package com.medianetsoftware.endpoints;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.medianetsoftware.dto.grantingticket.AuthenticationState;

@RestController
public class GrantingTicketController {
	
	Logger log = Logger.getLogger(GrantingTicketController.class);
	
	@RequestMapping(value = "/grantingTicket/V01", method = RequestMethod.POST)
	public AuthenticationState executeGrantingTicket (@RequestBody String request, HttpServletResponse response){
		log.debug("GrantingTicketController - executeGrantingTicket: " + request);
		
		response.setHeader("tsec", "NDE1MjMxMDAzNTg2MTQzNUFETUlORjowMDAwMDAwMA==");
		return new AuthenticationState();
	}

}
