package com.medianetsoftware.endpoints;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoAgileOperation;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoAgileOperationTransfer;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoBank;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoCheckAccount;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoContract;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoCurrency;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoEntityId;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoExpressAccount;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoReceiver;
import com.bbva.zic.mobiletransfers.facade.v01.dto.DtoSender;
import com.bbva.zic.mobiletransfers.facade.v01.enums.EnumSchedulerType;
import com.medianetsoftware.dto.CheckAccount;
import com.medianetsoftware.dto.OperationResult;
import com.medianetsoftware.dto.Pagination;
import com.medianetsoftware.dto.servicepayments.Account;
import com.medianetsoftware.dto.servicepayments.DtoAgileOperationsList;
import com.medianetsoftware.dto.servicepayments.Product;

@RestController
public class AgileOperationsController {

	Logger log = Logger.getLogger(AgileOperationsController.class);
	
	@RequestMapping(value = "/agileOperations/V01", method = RequestMethod.POST)
	public DtoAgileOperation createAgileOperations (@RequestBody String agileOperation){
		log.debug("AgileOperationsController - createAgileOperations - agileOperation: " + agileOperation);
		DtoAgileOperation result = new DtoAgileOperation();
		result.setId("5562625143");
		return result;
	}
	
	@RequestMapping(value = "/agileOperations/V01" , method = RequestMethod.GET)
	public DtoAgileOperationsList listAgileOperations(@PathParam("agileOperationType") String agileOperationType) {
		log.debug("AgileOperationsController - listAgileOperations - agileOperationType: " + agileOperationType);
		return generateListAgileOperationsMockedResponse(agileOperationType);
	}

	@RequestMapping(value = "/agileOperations/V01" , method = RequestMethod.PUT)
	public DtoAgileOperation modifyAgileOperation(@RequestParam DtoAgileOperation agileOperation) {
		log.debug("AgileOperationsController - modifyAgileOperation - agileOperationId: " + agileOperation);
		DtoAgileOperation result = new DtoAgileOperation();
		result.setId("5562625143");
		return result;
	}

	@RequestMapping(value = "/agileOperations/V01" , method = RequestMethod.DELETE)
	public OperationResult deleteAgileOperation(@PathParam("agileOperationType") String agileOperationId) {
		log.debug("AgileOperationsController - deleteAgileOperation - agileOperationId: " + agileOperationId);
		OperationResult operationResult = new OperationResult();
		return operationResult;
	}
	
	private Pagination setPaginationData(String next, String previous) {
		Pagination pagination = new Pagination();
		pagination.setNextPaginationKey(next);
		pagination.setPreviousPaginationKey(previous);
		return pagination;
	}
	
	private DtoAgileOperationsList generateListAgileOperationsMockedResponse(
			String agileOperationType) {
		DtoAgileOperationsList result = new DtoAgileOperationsList();
		List<DtoAgileOperation> listOperations = new ArrayList<DtoAgileOperation>();
		
		listOperations.add(setAgileOperationItem("876542344234","Boletos de Avion", "Javier Guzman","2354673454", "Pago",
				false, EnumSchedulerType.PERIODIC, "Juan Marquez", "45674", "jmarqiuez@correo.com", "5521345698" , "1254698752364586" ,
				"hjg89*?gs#$" , "13f78HFGJB", "jguzman" , "Pago Avion"));
		
		listOperations.add(setAgileOperationItem("65432423453", "Mensualidad" , "Rodrigo Ramírez" , "0213546978" 
				, "Pago de mensualidad" , true , EnumSchedulerType.PERIODIC , "Carlos García","563423" ,"example@agiloperations.com" ,
				"5512369487",null,"1#$&hj526+*lk" , "34AHGVDHBD", "m.estrada" , "MANUTENCION"));
		
		result.setItems(listOperations);
//		result.setAgileOperationType(EnumAgileOperationType.getEnumValue(agileOperationType));
		result.setPagination(setPaginationData("",""));
		
		return result;
	}

	
	private DtoAgileOperation setAgileOperationItem(String mainTransactionId, String reasonPayment, String client, String accountNumber,
			String concept, boolean token, EnumSchedulerType typeScheduler, String receiverName, String bankId, String receiverEmail,
			String cellphoneNumber, String receiverAccountNumber, String operationId, String reference, String user, String shortName) {

		DtoAgileOperation operation = new DtoAgileOperation();
		
		//EntityId
		DtoEntityId entityId = new DtoEntityId();
		entityId.setMainTransactionId(mainTransactionId);
		operation.setEntityId(entityId);
		
		//Operation	
		DtoAgileOperationTransfer agileOperationTransfer = new DtoAgileOperationTransfer();
		agileOperationTransfer.setReasonPayment(reasonPayment);
		agileOperationTransfer.setClient(client);
		
		//Sender
		DtoSender sender = new DtoSender();
		DtoContract contract = new DtoContract();		
		DtoCheckAccount checkAccount = new DtoCheckAccount();
		checkAccount.setAccountNumber(accountNumber);

		
		
		Product product = new Product();
		Account account = new Account();
		CheckAccount checkAccount2 = new CheckAccount();
		checkAccount2.setAccountNumber("342342342");
		
		account.setCheckAccount(checkAccount2 );
		product.setAccount(account );
		contract.setProduct(product );
		
		DtoCurrency currency = new DtoCurrency();
		currency.setCode("MXP");
		currency.setId("MXP");
		currency.setName("MXP");
		contract.setCurrency(currency);
		sender.setContract(contract);
		agileOperationTransfer.setSender(sender);
				
		agileOperationTransfer.setConcept(concept);
		agileOperationTransfer.setToken(token);
		agileOperationTransfer.setTypeScheduler(typeScheduler);

		//Receiver
		DtoReceiver receiver = new DtoReceiver();
		receiver.setName(receiverName);
		DtoBank bank = new DtoBank();
		bank.setId(bankId);
		receiver.setBank(bank);
		receiver.setEmail(receiverEmail);		
		
		contract = new DtoContract();		
		DtoExpressAccount expressAccount = new DtoExpressAccount();
		expressAccount.setCellphoneNumber(cellphoneNumber);
		
		contract.setProduct(product);
	
		receiver.setContract(contract);		
		agileOperationTransfer.setReceiver(receiver);
		
		operation.setOperation(agileOperationTransfer);
		operation.setId(operationId);
		operation.setReference(reference);
		operation.setUser(user);
		operation.setShortName(shortName);	
		
		return operation;
	}
}
