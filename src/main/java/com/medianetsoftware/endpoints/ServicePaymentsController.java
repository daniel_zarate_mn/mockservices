package com.medianetsoftware.endpoints;

import javax.websocket.server.PathParam;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.medianetsoftware.builders.DtoServicePaymentBuilder;
import com.medianetsoftware.builders.OperationResultBuilder;
import com.medianetsoftware.dto.OperationResult;
import com.medianetsoftware.dto.servicepayments.DtoServicePayment;
@RestController
public class ServicePaymentsController {

	Logger log = Logger.getLogger(ServicePaymentsController.class);
    
	@RequestMapping(value = "/servicePayments/rup", method = RequestMethod.GET)
	public DtoServicePayment getServicePaymentByRUP(@PathParam("rupCode") String rupCode){
		log.debug("ServicePaymentsController - getServicePaymentByRUP - rupCode: " + rupCode);
		return DtoServicePaymentBuilder.build();
	}
	
	@RequestMapping(value = "/servicePayments/V01/{paymentCode}", method = RequestMethod.GET)
	public DtoServicePayment getServicePayment(@PathParam("paymentCode") String paymentCode){
		log.debug("ServicePaymentsController - getServicePayment - paymentCode: " + paymentCode);
		return DtoServicePaymentBuilder.build();
	}
	
	@RequestMapping(value = "/servicePayments/V01", method = RequestMethod.POST)
	public OperationResult createServicePayment(@RequestBody String servicePayment){
		log.debug("ServicePaymentsController - createServicePayment - request: " + servicePayment);
        return OperationResultBuilder.build();
	}
	
	@RequestMapping(value = "/servicePayments/simulate/V01", method = RequestMethod.POST)
	public OperationResult simulateServicePayment(@RequestBody String servicePayment){
		log.debug("ServicePaymentsController - simulateServicePayment - request: " + servicePayment);
        return OperationResultBuilder.build();
	}
}
