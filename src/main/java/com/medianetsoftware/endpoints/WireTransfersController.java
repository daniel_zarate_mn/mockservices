package com.medianetsoftware.endpoints;


import com.medianetsoftware.dto.*;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by Jorge Roldan on 30/10/2015.
 *
 * This class emulate the wireTransfers web service and only returns a fake json response
 */
@RestController
public class WireTransfersController {

	private static Logger LOGGER = Logger.getLogger(WireTransfersController.class);
	
    @RequestMapping(value = "/wireTransfers/V01", method = RequestMethod.POST)
    public OperationResult internalTransfersV01(@RequestBody String transfer){        
        LOGGER.debug(transfer);
        return createOperationResult();
    }

	private OperationResult createOperationResult() {
		OperationResult opResult = new OperationResult();
        opResult.setOperationDate(new Date());
        opResult.setFinalBalanceSender(createBalance());
        opResult.setAuthorizationInfo(createAuthorizationInfo());
        opResult.setFinalBalanceReceiver(createBalance());
        opResult.setExecutionDate(new Date());
        opResult.setAdditionalInfo(createAditionalInfo());
        //opResult.setErrorInfo(createErrorInfo());
        opResult.setEntityId(createEntityId());
        opResult.setId("S");
        opResult.setExpirationDate(new Date());
		return opResult;
	}

	private AuthorizationInfo createAuthorizationInfo() {
		AuthorizationInfo authorizationInfo = new AuthorizationInfo();
        authorizationInfo.setReceiverAuthNumber("S");
        authorizationInfo.setSenderAuthNumber("S");
		return authorizationInfo;
	}

	private Balance createBalance() {
		Balance balanceReceiver = new Balance();

        Money balanceReceiverAMT = new Money();
        balanceReceiverAMT.setAmount(9.9);
        Currency pendingBalanceReceiverCurrency = new Currency();
        pendingBalanceReceiverCurrency.setCode("S");
        pendingBalanceReceiverCurrency.setId("S");
        pendingBalanceReceiverCurrency.setName("S");
        balanceReceiverAMT.setCurrency(pendingBalanceReceiverCurrency);
        balanceReceiver.setPendingBalance(balanceReceiverAMT);
        balanceReceiver.setBalance(balanceReceiverAMT);
        balanceReceiver.setCuttingBalance(balanceReceiverAMT);
        balanceReceiver.setTotalBalance(balanceReceiverAMT);
        balanceReceiver.setGrantedBalance(balanceReceiverAMT);
        balanceReceiver.setCurrentBalance(balanceReceiverAMT);
        balanceReceiver.setBalanceAtDate(balanceReceiverAMT);
        balanceReceiver.setAvailableBalance(balanceReceiverAMT);
		return balanceReceiver;
	}

	private AdditionalInfoTransfer createAditionalInfo() {
		AdditionalInfoTransfer infoTransfer = new AdditionalInfoTransfer();
        Money moneyTrans = new Money();
        moneyTrans.setAmount(9.9);
        Currency currencyTrans = new Currency();
        currencyTrans.setCode("S");
        currencyTrans.setId("S");
        currencyTrans.setName("S");
        moneyTrans.setCurrency(currencyTrans);
        infoTransfer.setDepositAmount(moneyTrans);
        infoTransfer.setPercentCommissionWithdrawal(9);
        Commision commisionWithdrawalCash = new Commision();
        commisionWithdrawalCash.setId("S");
        commisionWithdrawalCash.setCreditLineCommWithdrawalCash("S");
        commisionWithdrawalCash.setAmount(moneyTrans);
        infoTransfer.setCommisionWithdrawalCash(commisionWithdrawalCash);
        infoTransfer.setExtractNumber("S");
        infoTransfer.setChargeAmount(moneyTrans);
        infoTransfer.setMovementNumber("S");
        infoTransfer.setCancellationReference("S");
		return infoTransfer;
	}

	private ErrorInfo createErrorInfo() {
		ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("S");
        errorInfo.setErrorMessage("S");
		return errorInfo;
	}

	private EntityId createEntityId() {
		EntityId entityId = new EntityId();
        entityId.setSecurityCode("S");
        entityId.setMainTransactionId("S");
        entityId.setApplicationId("S");
        entityId.setApplicationId("S");
        entityId.setServicePaymentId("S");
        entityId.setTrackingId("S");
		return entityId;
	}
}
