package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.IAccount;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "checkAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "checkAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoCheckAccount implements IAccount, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiParam(name = "productBase", value = "Product Base", required = true)
	@NotNull
	private DtoProductBase productBase;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "accountNumber", value = "Account Number", required = true)
	@NotNull
	private String accountNumber;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	@ApiParam(name = "indicatorJointAccount", value = "Indicator Joint Account", required = true)
	@NotNull
	private String indicatorJointAccount;

	@ApiParam(name = "isAditionalAccount", value = "Is Aditional Account", required = true)
	@NotNull
	private Boolean isAditionalAccount;

	@ApiParam(name = "region", value = "Region", required = true)
	@NotNull
	private DtoRegion region;

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getIndicatorJointAccount() {
		return indicatorJointAccount;
	}

	public void setIndicatorJointAccount(final String indicatorJointAccount) {
		this.indicatorJointAccount = indicatorJointAccount;
	}

	public Boolean getIsAditionalAccount() {
		return isAditionalAccount;
	}

	public void setIsAditionalAccount(final Boolean isAditionalAccount) {
		this.isAditionalAccount = isAditionalAccount;
	}

	public DtoRegion getRegion() {
		return region;
	}

	public void setRegion(final DtoRegion region) {
		this.region = region;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoCheckAccount)) {
			return false;
		}
		final DtoCheckAccount castOther = (DtoCheckAccount) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(accountNumber, castOther.accountNumber).append(shortName, castOther.shortName)
				.append(indicatorJointAccount, castOther.indicatorJointAccount).append(isAditionalAccount, castOther.isAditionalAccount)
				.append(region, castOther.region).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(accountNumber).append(shortName).append(indicatorJointAccount)
				.append(isAditionalAccount).append(region).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("accountNumber", accountNumber)
				.append("shortName", shortName).append("indicatorJointAccount", indicatorJointAccount)
				.append("isAditionalAccount", isAditionalAccount).append("region", region).toString();
	}

}
