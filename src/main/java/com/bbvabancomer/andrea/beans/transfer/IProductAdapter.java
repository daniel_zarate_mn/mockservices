package com.bbvabancomer.andrea.beans.transfer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.bbvabancomer.andrea.beans.transfer.dto.DtoAutoLoan;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoCheckAccount;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoCreditCard;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoDebitCard;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoEnterpriseAgreement;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoExpressAccount;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoMobileAccount;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoMortgageCredit;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoPersonalLoan;
import com.bbvabancomer.andrea.beans.transfer.dto.DtoPrepaidCard;
import com.bbvabancomer.andrea.beans.transfer.IProductAdapter.AdaptedIProduct;


public class IProductAdapter extends XmlAdapter<AdaptedIProduct, IProduct> {

	@Override
	public AdaptedIProduct marshal(final IProduct iProduct) throws Exception {
		final AdaptedIProduct adaptedIProduct = new AdaptedIProduct();
		if (iProduct instanceof DtoCheckAccount) {
			adaptedIProduct.setCheckAccount((DtoCheckAccount) iProduct);
		} else if (iProduct instanceof DtoExpressAccount) {
			adaptedIProduct.setExpressAccount((DtoExpressAccount) iProduct);
		} else if (iProduct instanceof DtoMobileAccount) {
			adaptedIProduct.setMobileAccount((DtoMobileAccount) iProduct);
		} else if (iProduct instanceof DtoEnterpriseAgreement) {
			adaptedIProduct.setEnterpriseAgreement((DtoEnterpriseAgreement) iProduct);
		} else if (iProduct instanceof DtoCreditCard) {
			adaptedIProduct.setCreditCard((DtoCreditCard) iProduct);
		} else if (iProduct instanceof DtoDebitCard) {
			adaptedIProduct.setDebitCard((DtoDebitCard) iProduct);
		} else if (iProduct instanceof DtoPrepaidCard) {
			adaptedIProduct.setPrepaidCard((DtoPrepaidCard) iProduct);
		} else if (iProduct instanceof DtoAutoLoan) {
			adaptedIProduct.setAutoLoan((DtoAutoLoan) iProduct);
		} else if (iProduct instanceof DtoMortgageCredit) {
			adaptedIProduct.setMortgageCredit((DtoMortgageCredit) iProduct);
		} else if (iProduct instanceof DtoPersonalLoan) {
			adaptedIProduct.setPersonalLoan((DtoPersonalLoan) iProduct);
//		} else if (iProduct instanceof DtoCheckAccount) {
//			adaptedIProduct.setSavingAccount((DtoSavingAccount) iProduct);
		} else {
			throw new RuntimeException("Tipo no soportado por la interfaz");
		}
		return adaptedIProduct;
	}

	@Override
	public IProduct unmarshal(final AdaptedIProduct adaptedIProduct) throws Exception {
		if (adaptedIProduct.getCheckAccount() != null) {
			return adaptedIProduct.getCheckAccount();
		} else if (adaptedIProduct.getExpressAccount() != null) {
			return adaptedIProduct.getExpressAccount();
		} else if (adaptedIProduct.getMobileAccount() != null) {
			return adaptedIProduct.getMobileAccount();
		} else if (adaptedIProduct.getEnterpriseAgreement() != null) {
			return adaptedIProduct.getEnterpriseAgreement();
		} else if (adaptedIProduct.getCreditCard() != null) {
			return adaptedIProduct.getCreditCard();
		} else if (adaptedIProduct.getDebitCard() != null) {
			return adaptedIProduct.getDebitCard();
		} else if (adaptedIProduct.getPrepaidCard() != null) {
			return adaptedIProduct.getPrepaidCard();
		} else if (adaptedIProduct.getAutoLoan() != null) {
			return adaptedIProduct.getAutoLoan();
		} else if (adaptedIProduct.getMortgageCredit() != null) {
			return adaptedIProduct.getMortgageCredit();
		} else if (adaptedIProduct.getPersonalLoan() != null) {
			return adaptedIProduct.getPersonalLoan();
//		} else if (adaptedIProduct.getSavingAccount() != null) {
//			return adaptedIProduct.getSavingAccount();
		} else {
			throw new RuntimeException("Todos los valores son null");
		}
	}

	public static class AdaptedIProduct {

		@XmlElement(name = "checkAccount")
		private DtoCheckAccount checkAccount;

//		@XmlElement(name = "savingAccount")
//		private DtoSavingAccount savingAccount;

		@XmlElement(name = "expressAccount")
		private DtoExpressAccount expressAccount;

		@XmlElement(name = "mobileAccount")
		private DtoMobileAccount mobileAccount;

		@XmlElement(name = "enterpriseAgreement")
		private DtoEnterpriseAgreement enterpriseAgreement;

		@XmlElement(name = "creditCard")
		private DtoCreditCard creditCard;

		@XmlElement(name = "debitCard")
		private DtoDebitCard debitCard;

		@XmlElement(name = "prepaidCard")
		private DtoPrepaidCard prepaidCard;

		@XmlElement(name = "autoLoan")
		private DtoAutoLoan autoLoan;

		@XmlElement(name = "mortgageCredit")
		private DtoMortgageCredit mortgageCredit;

		@XmlElement(name = "personalLoan")
		private DtoPersonalLoan personalLoan;

//		/**
//		 * @return the savingAccount
//		 */
//		public DtoSavingAccount getSavingAccount() {
//			return savingAccount;
//		}
//
//		/**
//		 * @param savingAccount
//		 *            the savingAccount to set
//		 */
//		public void setSavingAccount(final DtoSavingAccount savingAccount) {
//			this.savingAccount = savingAccount;
//		}

		/**
		 * @return the checkAccount
		 */
		public DtoCheckAccount getCheckAccount() {
			return checkAccount;
		}

		/**
		 * @param checkAccount
		 *            the checkAccount to set
		 */
		public void setCheckAccount(final DtoCheckAccount checkAccount) {
			this.checkAccount = checkAccount;
		}

		/**
		 * @return the expressAccount
		 */
		public DtoExpressAccount getExpressAccount() {
			return expressAccount;
		}

		/**
		 * @param expressAccount
		 *            the expressAccount to set
		 */
		public void setExpressAccount(final DtoExpressAccount expressAccount) {
			this.expressAccount = expressAccount;
		}

		/**
		 * @return the mobileAccount
		 */
		public DtoMobileAccount getMobileAccount() {
			return mobileAccount;
		}

		/**
		 * @param mobileAccount
		 *            the mobileAccount to set
		 */
		public void setMobileAccount(final DtoMobileAccount mobileAccount) {
			this.mobileAccount = mobileAccount;
		}

		/**
		 * @return the enterpriseAgreement
		 */
		public DtoEnterpriseAgreement getEnterpriseAgreement() {
			return enterpriseAgreement;
		}

		/**
		 * @param enterpriseAgreement
		 *            the enterpriseAgreement to set
		 */
		public void setEnterpriseAgreement(final DtoEnterpriseAgreement enterpriseAgreement) {
			this.enterpriseAgreement = enterpriseAgreement;
		}

		/**
		 * @return the creditCard
		 */
		public DtoCreditCard getCreditCard() {
			return creditCard;
		}

		/**
		 * @param creditCard
		 *            the creditCard to set
		 */
		public void setCreditCard(final DtoCreditCard creditCard) {
			this.creditCard = creditCard;
		}

		/**
		 * @return the debitCard
		 */
		public DtoDebitCard getDebitCard() {
			return debitCard;
		}

		/**
		 * @param debitCard
		 *            the debitCard to set
		 */
		public void setDebitCard(final DtoDebitCard debitCard) {
			this.debitCard = debitCard;
		}

		/**
		 * @return the prepaidCard
		 */
		public DtoPrepaidCard getPrepaidCard() {
			return prepaidCard;
		}

		/**
		 * @param prepaidCard
		 *            the prepaidCard to set
		 */
		public void setPrepaidCard(final DtoPrepaidCard prepaidCard) {
			this.prepaidCard = prepaidCard;
		}

		/**
		 * @return the autoLoan
		 */
		public DtoAutoLoan getAutoLoan() {
			return autoLoan;
		}

		/**
		 * @param autoLoan
		 *            the autoLoan to set
		 */
		public void setAutoLoan(final DtoAutoLoan autoLoan) {
			this.autoLoan = autoLoan;
		}

		/**
		 * @return the mortgageCredit
		 */
		public DtoMortgageCredit getMortgageCredit() {
			return mortgageCredit;
		}

		/**
		 * @param mortgageCredit
		 *            the mortgageCredit to set
		 */
		public void setMortgageCredit(final DtoMortgageCredit mortgageCredit) {
			this.mortgageCredit = mortgageCredit;
		}

		/**
		 * @return the personalLoan
		 */
		public DtoPersonalLoan getPersonalLoan() {
			return personalLoan;
		}

		/**
		 * @param personalLoan
		 *            the personalLoan to set
		 */
		public void setPersonalLoan(final DtoPersonalLoan personalLoan) {
			this.personalLoan = personalLoan;
		}

	}

}


