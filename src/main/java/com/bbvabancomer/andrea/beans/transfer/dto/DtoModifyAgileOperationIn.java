package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "modifyAgileOperationIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "modifyAgileOperationIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoModifyAgileOperationIn implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "agileOperationId", value = "Agile Operation Id", required = true)
	@NotNull
	private String agileOperationId;

	@ApiParam(name = "email", value = "Email", required = true)
	@NotNull
	private String email;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	public String getAgileOperationId() {
		return agileOperationId;
	}

	public void setAgileOperationId(final String agileOperationId) {
		this.agileOperationId = agileOperationId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoModifyAgileOperationIn)) {
			return false;
		}
		final DtoModifyAgileOperationIn castOther = (DtoModifyAgileOperationIn) other;
		return new EqualsBuilder().append(agileOperationId, castOther.agileOperationId).append(email, castOther.email)
				.append(shortName, castOther.shortName).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(agileOperationId).append(email).append(shortName).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("agileOperationId", agileOperationId).append("email", email).append("shortName", shortName)
				.toString();
	}

}
