package com.bbvabancomer.andrea.beans.transfer.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumLastModifyStatus {

	CANCELED("X"), PENDING("X"), PROCESSED("X"), REJECTED("X"), EXPIRED("X"), CASHED("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumLastModifyStatus.class);

	private String key;

	EnumLastModifyStatus(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumLastModifyStatus getEnumValue(final String key) {
		for (final EnumLastModifyStatus lastModifyStatus : values()) {
			if (lastModifyStatus.key.equals(key)) {
				return lastModifyStatus;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumLastModifyStatus lastModifyStatus : values()) {
			if (lastModifyStatus.name().equalsIgnoreCase(key)) {
				return lastModifyStatus.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
