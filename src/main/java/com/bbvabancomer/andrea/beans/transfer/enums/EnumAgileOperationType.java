package com.bbvabancomer.andrea.beans.transfer.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumAgileOperationType {

	FRECUENT("FR"), QUICK("RA"), PREREGISTER("PR"), CONTRACTED_SERVICES("SC"), FAST_PERIODIC_OPERATIONS("3"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumAgileOperationType.class);

	private String key;

	EnumAgileOperationType(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumAgileOperationType getEnumValue(final String key) {
		for (final EnumAgileOperationType agileOperationType : values()) {
			if (agileOperationType.key.equals(key)) {
				return agileOperationType;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumAgileOperationType agileOperationType : values()) {
			if (agileOperationType.name().equalsIgnoreCase(key)) {
				return agileOperationType.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
