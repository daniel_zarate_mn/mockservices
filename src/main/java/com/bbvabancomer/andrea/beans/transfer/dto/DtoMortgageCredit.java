package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.ILoan;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "mortgageCredit", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "mortgageCredit", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoMortgageCredit implements ILoan, Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "productBase", value = "Product Base", required = true)
	@NotNull
	private DtoProductBase productBase;

	@ApiParam(name = "associatedContracts", value = "Associated Contracts", required = true)
	@NotNull
	private List<DtoContract> associatedContracts;

	@ApiParam(name = "idContract", value = "Id Contract", required = true)
	@NotNull
	private String idContract;

	@ApiParam(name = "nationalMoneyBalance", value = "National Money Balance", required = true)
	@NotNull
	private Float nationalMoneyBalance;

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public List<DtoContract> getAssociatedContracts() {
		return associatedContracts;
	}

	public void setAssociatedContracts(final List<DtoContract> associatedContracts) {
		this.associatedContracts = associatedContracts;
	}

	public String getIdContract() {
		return idContract;
	}

	public void setIdContract(final String idContract) {
		this.idContract = idContract;
	}

	public Float getNationalMoneypBalance() {
		return nationalMoneyBalance;
	}

	public void setNationalMoneypBalance(final Float nationalMoneypBalance) {
		this.nationalMoneyBalance = nationalMoneypBalance;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoMortgageCredit)) {
			return false;
		}
		final DtoMortgageCredit castOther = (DtoMortgageCredit) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(associatedContracts, castOther.associatedContracts)
				.append(idContract, castOther.idContract).append(nationalMoneyBalance, castOther.nationalMoneyBalance).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(associatedContracts).append(idContract).append(nationalMoneyBalance)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("associatedContracts", associatedContracts)
				.append("idContract", idContract).append("nationalMoneypBalance", nationalMoneyBalance).toString();
	}

}
