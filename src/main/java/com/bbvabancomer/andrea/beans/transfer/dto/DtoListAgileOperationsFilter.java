package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "listAgileOperationsFilter", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "listAgileOperationsFilter", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoListAgileOperationsFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "agileOperationType", value = "Agile Operation Type", required = true)
	@NotNull
	private String agileOperationType;

	@ApiParam(name = "businessFlow", value = "Business Flow", required = true)
	@NotNull
	private String businessFlow;

	public String getAgileOperationType() {
		return agileOperationType;
	}

	public void setAgileOperationType(final String agileOperationType) {
		this.agileOperationType = agileOperationType;
	}

	public String getBusinessFlow() {
		return businessFlow;
	}

	public void setBusinessFlow(final String businessFlow) {
		this.businessFlow = businessFlow;
	}

}
