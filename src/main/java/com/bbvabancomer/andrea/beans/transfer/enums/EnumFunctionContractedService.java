package com.bbvabancomer.andrea.beans.transfer.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumFunctionContractedService {

	DELETE("B"), MODIFY("M"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumFunctionContractedService.class);

	private String key;

	EnumFunctionContractedService(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumFunctionContractedService getEnumValue(final String key) {
		for (final EnumFunctionContractedService functionContractedService : values()) {
			if (functionContractedService.key.equals(key)) {
				return functionContractedService;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumFunctionContractedService functionContractedService : values()) {
			if (functionContractedService.name().equalsIgnoreCase(key)) {
				return functionContractedService.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
