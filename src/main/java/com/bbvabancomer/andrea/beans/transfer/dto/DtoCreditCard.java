package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.ICard;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "creditCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "creditCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoCreditCard implements ICard, Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "productBase", value = "Product Base", required = true)
	@NotNull
	private DtoProductBase productBase;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "accountNumber", value = "Account Number", required = true)
	@NotNull
	private String accountNumber;

	@ApiParam(name = "cardNumber", value = "CardNumber", required = true)
	@NotNull
	private String cardNumber;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	@ApiParam(name = "commission", value = "Commission", required = true)
	@NotNull
	private DtoCommision commission;

	@ApiParam(name = "minimumPayment", value = "Minimum Payment", required = true)
	@NotNull
	private DtoMoney minimumPayment;

	@ApiParam(name = "interbankCode", value = "Interbank Code", required = true)
	@NotNull
	private String interbankCode;

	@ApiParam(name = "indicatorJointAccount", value = "Indicator Joint Account", required = true)
	@NotNull
	private String indicatorJointAccount;

	@ApiParam(name = "isAditionalAccount", value = "Is Aditional Account", required = true)
	@NotNull
	private Boolean isAditionalAccount;

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public DtoCommision getCommission() {
		return commission;
	}

	public void setCommission(final DtoCommision commission) {
		this.commission = commission;
	}

	public DtoMoney getMinimumPayment() {
		return minimumPayment;
	}

	public void setMinimumPayment(final DtoMoney minimumPayment) {
		this.minimumPayment = minimumPayment;
	}

	public String getInterbankCode() {
		return interbankCode;
	}

	public void setInterbankCode(final String interbankCode) {
		this.interbankCode = interbankCode;
	}

	public String getIndicatorJointAccount() {
		return indicatorJointAccount;
	}

	public void setIndicatorJointAccount(final String indicatorJointAccount) {
		this.indicatorJointAccount = indicatorJointAccount;
	}

	public Boolean getIsAditionalAccount() {
		return isAditionalAccount;
	}

	public void setIsAditionalAccount(final Boolean isAditionalAccount) {
		this.isAditionalAccount = isAditionalAccount;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoCreditCard)) {
			return false;
		}
		final DtoCreditCard castOther = (DtoCreditCard) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(accountNumber, castOther.accountNumber).append(cardNumber, castOther.cardNumber)
				.append(shortName, castOther.shortName).append(commission, castOther.commission)
				.append(minimumPayment, castOther.minimumPayment).append(interbankCode, castOther.interbankCode)
				.append(indicatorJointAccount, castOther.indicatorJointAccount).append(isAditionalAccount, castOther.isAditionalAccount)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(accountNumber).append(cardNumber).append(shortName)
				.append(commission).append(minimumPayment).append(interbankCode).append(indicatorJointAccount).append(isAditionalAccount)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("accountNumber", accountNumber)
				.append("cardNumber", cardNumber).append("shortName", shortName).append("commission", commission)
				.append("minimumPayment", minimumPayment).append("interbankCode", interbankCode)
				.append("indicatorJointAccount", indicatorJointAccount).append("isAditionalAccount", isAditionalAccount).toString();
	}

}
