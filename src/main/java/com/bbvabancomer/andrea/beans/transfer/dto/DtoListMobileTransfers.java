package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author IEUser
 *
 */
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "listMobileTransfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "listMobileTransfers", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoListMobileTransfers implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@ApiParam(name = "transferStatus", value = "Transfer Status", required = true)
	@NotNull
	private String transferStatus;

	/**
	 * @return the transferStatus
	 */
	public String getTransferStatus() {
		return transferStatus;
	}

	/**
	 * @param transferStatus
	 *            the transferStatus to set
	 */
	public void setTransferStatus(final String transferStatus) {
		this.transferStatus = transferStatus;
	}
}
