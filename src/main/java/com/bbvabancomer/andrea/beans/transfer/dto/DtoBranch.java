package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.enums.EnumRegionalType;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "branch", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "branch", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoBranch implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "name", value = "Name", required = true)
	@NotNull
	private String name;

	@ApiParam(name = "type", value = "Type", required = true)
	@NotNull
	private EnumRegionalType type;

	@ApiParam(name = "bank", value = "Bank", required = true)
	@NotNull
	private DtoBank bank;

	@ApiParam(name = "region", value = "Region", required = true)
	@NotNull
	private DtoRegion region;

	public String getId() {

		return id;
	}

	public void setId(final String id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(final String name) {

		this.name = name;
	}

	public EnumRegionalType getType() {

		return type;
	}

	public void setType(final EnumRegionalType type) {

		this.type = type;
	}

	public DtoBank getBank() {

		return bank;
	}

	public void setBank(final DtoBank bank) {

		this.bank = bank;
	}

	public DtoRegion getRegion() {

		return region;
	}

	public void setRegion(final DtoRegion region) {

		this.region = region;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoBranch)) {
			return false;
		}
		final DtoBranch castOther = (DtoBranch) other;
		return new EqualsBuilder().append(id, castOther.id).append(name, castOther.name).append(type, castOther.type)
				.append(bank, castOther.bank).append(region, castOther.region).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(name).append(type).append(bank).append(region).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("type", type).append("bank", bank)
				.append("region", region).toString();
	}

}
