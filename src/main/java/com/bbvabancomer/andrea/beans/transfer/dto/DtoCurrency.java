package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "currency", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "currency", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoCurrency implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "code", value = "Code", required = true)
	@NotNull
	private String code;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "name", value = "Name", required = true)
	@NotNull
	private String name;

	public String getId() {

		return id;
	}

	public void setId(final String id) {

		this.id = id;
	}

	public String getCode() {

		return code;
	}

	public void setCode(final String code) {

		this.code = code;
	}

	public String getName() {

		return name;
	}

	public void setName(final String name) {

		this.name = name;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoCurrency)) {
			return false;
		}
		final DtoCurrency castOther = (DtoCurrency) other;
		return new EqualsBuilder().append(code, castOther.code).append(id, castOther.id).append(name, castOther.name).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(code).append(id).append(name).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("code", code).append("id", id).append("name", name).toString();
	}

}
