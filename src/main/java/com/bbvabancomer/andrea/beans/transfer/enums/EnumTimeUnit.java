package com.bbvabancomer.andrea.beans.transfer.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumTimeUnit {

	WEEKLY("X"), FORTNIGHT("X"), MONTHLY("X"), BIMONTHLY("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumTimeUnit.class);

	private String key;

	EnumTimeUnit(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumTimeUnit getEnumValue(final String key) {
		for (final EnumTimeUnit timeUnit : values()) {
			if (timeUnit.key.equals(key)) {
				return timeUnit;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumTimeUnit enumIntTimeUnit : values()) {
			if (enumIntTimeUnit.name().equalsIgnoreCase(key)) {
				return enumIntTimeUnit.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
