package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.ICard;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "debitCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "debitCard", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoDebitCard implements ICard, Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "productBase", value = "Product Base", required = true)
	@NotNull
	private DtoProductBase productBase;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "accountNumber", value = "Account Number", required = true)
	@NotNull
	private String accountNumber;

	@ApiParam(name = "cardNumber", value = "Card Number", required = true)
	@NotNull
	private String cardNumber;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	@ApiParam(name = "commision", value = "Commision", required = true)
	@NotNull
	private DtoCommision commision;

	@ApiParam(name = "minimumPayment", value = "Minimum Payment", required = true)
	@NotNull
	private DtoMoney minimumPayment;

	@ApiParam(name = "interbankCode", value = "Interbank Code", required = true)
	@NotNull
	private String interbankCode;

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public DtoCommision getCommision() {
		return commision;
	}

	public void setCommision(final DtoCommision commision) {
		this.commision = commision;
	}

	public DtoMoney getMinimumPayment() {
		return minimumPayment;
	}

	public void setMinimumPayment(final DtoMoney minimumPayment) {
		this.minimumPayment = minimumPayment;
	}

	public String getInterbankCode() {
		return interbankCode;
	}

	public void setInterbankCode(final String interbankCode) {
		this.interbankCode = interbankCode;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoDebitCard)) {
			return false;
		}
		final DtoDebitCard castOther = (DtoDebitCard) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(accountNumber, castOther.accountNumber).append(cardNumber, castOther.cardNumber)
				.append(shortName, castOther.shortName).append(commision, castOther.commision)
				.append(minimumPayment, castOther.minimumPayment).append(interbankCode, castOther.interbankCode).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(accountNumber).append(cardNumber).append(shortName)
				.append(commision).append(minimumPayment).append(interbankCode).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("accountNumber", accountNumber)
				.append("cardNumber", cardNumber).append("shortName", shortName).append("commision", commision)
				.append("minimumPayment", minimumPayment).append("interbankCode", interbankCode).toString();
	}

}
