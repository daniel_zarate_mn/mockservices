package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "tax", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "tax", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoTax implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "value", value = "Value", required = true)
	@NotNull
	private DtoMoney value;

	@ApiParam(name = "description", value = "Description", required = true)
	@NotNull
	private String description;

	public DtoMoney getValue() {
		return value;
	}

	public void setValue(final DtoMoney value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoTax)) {
			return false;
		}
		final DtoTax castOther = (DtoTax) other;
		return new EqualsBuilder().append(value, castOther.value).append(description, castOther.description).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(value).append(description).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("value", value).append("description", description).toString();
	}

}
