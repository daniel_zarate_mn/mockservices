package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "productBase", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "productBase", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoProductBase implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "name", value = "Name", required = true)
	@NotNull
	private String name;

	@ApiParam(name = "branch", value = "Branch", required = true)
	@NotNull
	private DtoBranch branch;

	@ApiParam(name = "currency", value = "Currency", required = true)
	@NotNull
	private DtoCurrency currency;

	@ApiParam(name = "type", value = "Type", required = true)
	@NotNull
	private String type;

	@ApiParam(name = "balance", value = "Balance", required = true)
	@NotNull
	private DtoBalance balance;

	@ApiParam(name = "shaftAccountIndicator", value = "Shaft Account Indicator", required = true)
	@NotNull
	private String shaftAccountIndicator;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public DtoBranch getBranch() {
		return branch;
	}

	public void setBranch(final DtoBranch branch) {
		this.branch = branch;
	}

	public DtoCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(final DtoCurrency currency) {
		this.currency = currency;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public DtoBalance getBalance() {
		return balance;
	}

	public void setBalance(final DtoBalance balance) {
		this.balance = balance;
	}

	public String getShaftAccountIndicator() {
		return shaftAccountIndicator;
	}

	public void setShaftAccountIndicator(final String shaftAccountIndicator) {
		this.shaftAccountIndicator = shaftAccountIndicator;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoProductBase)) {
			return false;
		}
		final DtoProductBase castOther = (DtoProductBase) other;
		return new EqualsBuilder().append(name, castOther.name).append(branch, castOther.branch).append(currency, castOther.currency)
				.append(type, castOther.type).append(balance, castOther.balance)
				.append(shaftAccountIndicator, castOther.shaftAccountIndicator).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(name).append(branch).append(currency).append(type).append(balance)
				.append(shaftAccountIndicator).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("branch", branch).append("currency", currency).append("type", type)
				.append("balance", balance).append("shaftAccountIndicator", shaftAccountIndicator).toString();
	}

}
