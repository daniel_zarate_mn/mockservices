package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.IAgreement;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "enterpriseAgreement", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "enterpriseAgreement", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoEnterpriseAgreement implements IAgreement, Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "productBase", value = "Product Base", required = true)
	@NotNull
	private DtoProductBase productBase;

	@ApiParam(name = "cashIndicator", value = "Cash Indicator", required = true)
	@NotNull
	private String cashIndicator;

	@ApiParam(name = "city", value = "City", required = true)
	@NotNull
	private DtoCity city;

	@ApiParam(name = "clasification", value = "Clasification", required = true)
	@NotNull
	private String clasification;

	@ApiParam(name = "commercialName", value = "Commercial Name", required = true)
	@NotNull
	private String commercialName;

	@ApiParam(name = "conceptFormat", value = "Concept Format", required = true)
	@NotNull
	private String conceptFormat;

	@ApiParam(name = "conceptIndicator", value = "Concept Indicator", required = true)
	@NotNull
	private String conceptIndicator;

	@ApiParam(name = "conceptValidationRoutine", value = "conceptValidationRoutine", required = true)
	@NotNull
	private String conceptValidationRoutine;

	@ApiParam(name = "enterpriseAgreementOwner", value = "Enterprise Agreement Owner", required = true)
	@NotNull
	private String enterpriseAgreementOwner;

	@ApiParam(name = "creditCardIndicator", value = "Credit Card Indicator", required = true)
	@NotNull
	private String creditCardIndicator;

	@ApiParam(name = "currency", value = "Currency", required = true)
	@NotNull
	private DtoCurrency currency;

	@ApiParam(name = "formalName", value = "Formal Name", required = true)
	@NotNull
	private String formalName;

	@ApiParam(name = "helpImageUrl", value = "Help Image Url", required = true)
	@NotNull
	private String helpImageUrl;

	@ApiParam(name = "helpPaymentType", value = "Help Payment Type", required = true)
	@NotNull
	private String helpPaymentType;

	@ApiParam(name = "helpReference", value = "Help Reference", required = true)
	@NotNull
	private String helpReference;

	@ApiParam(name = "helpConcept", value = "Help Concept", required = true)
	@NotNull
	private String helpConcept;

	@ApiParam(name = "helpThirdMessage", value = "Help Third Message", required = true)
	@NotNull
	private String helpThirdMessage;

	@ApiParam(name = "authorization", value = "Authorization", required = true)
	@NotNull
	private String authorization;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "immediatePaymentIndicator", value = "Immediate Payment Indicator", required = true)
	@NotNull
	private String immediatePaymentIndicator;

	@ApiParam(name = "businessLine", value = "Business Line", required = true)
	@NotNull
	private DtoBusinessLine businessLine;

	@ApiParam(name = "logoUrl", value = "Logo Url", required = true)
	@NotNull
	private String logoUrl;

	@ApiParam(name = "maxChecksLimit", value = "Max Checks Limit", required = true)
	@NotNull
	private Number maxChecksLimit;

	@ApiParam(name = "maxConceptLength", value = "Max Concept Length", required = true)
	@NotNull
	private Number maxConceptLength;

	@ApiParam(name = "maxReferenceLength", value = "Max Reference Length", required = true)
	@NotNull
	private Number maxReferenceLength;

	@ApiParam(name = "minConceptLength", value = "Min Concept Length", required = true)
	@NotNull
	private Number minConceptLength;

	@ApiParam(name = "minReferenceLength", value = "Min Reference Length", required = true)
	@NotNull
	private Number minReferenceLength;

	@ApiParam(name = "onlinePaymentIndicator", value = "Online Payment Indicator", required = true)
	@NotNull
	private String onlinePaymentIndicator;

	@ApiParam(name = "paymentType", value = "PaymentType", required = true)
	@NotNull
	private String paymentType;

	@ApiParam(name = "referenceFormat", value = "Reference Format", required = true)
	@NotNull
	private String referenceFormat;

	@ApiParam(name = "referenceIndicator", value = "Reference Indicator", required = true)
	@NotNull
	private String referenceIndicator;

	@ApiParam(name = "referenceValidationRoutine", value = "Reference Validation Routine", required = true)
	@NotNull
	private String referenceValidationRoutine;

	@ApiParam(name = "region", value = "Region", required = true)
	@NotNull
	private DtoRegion region;

	@ApiParam(name = "remittanceIndicator", value = "Remittance Indicator", required = true)
	@NotNull
	private String remittanceIndicator;

	@ApiParam(name = "rupCode", value = "Rup Code", required = true)
	@NotNull
	private String rupCode;

	@ApiParam(name = "rupIndicator", value = "Rup Indicator", required = true)
	@NotNull
	private String rupIndicator;

	@ApiParam(name = "status", value = "Status", required = true)
	@NotNull
	private String status;

	@ApiParam(name = "serviceIndicator", value = "Service Indicator", required = true)
	@NotNull
	private String serviceIndicator;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	@ApiParam(name = "issuingCompany", value = "Issuing Company", required = true)
	@NotNull
	private String issuingCompany;

	@ApiParam(name = "theme", value = "Theme", required = true)
	@NotNull
	private String theme;

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getCashIndicator() {
		return cashIndicator;
	}

	public void setCashIndicator(final String cashIndicator) {
		this.cashIndicator = cashIndicator;
	}

	public DtoCity getCity() {
		return city;
	}

	public void setCity(final DtoCity city) {
		this.city = city;
	}

	public String getClasification() {
		return clasification;
	}

	public void setClasification(final String clasification) {
		this.clasification = clasification;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public void setCommercialName(final String commercialName) {
		this.commercialName = commercialName;
	}

	public String getConceptFormat() {
		return conceptFormat;
	}

	public void setConceptFormat(final String conceptFormat) {
		this.conceptFormat = conceptFormat;
	}

	public String getConceptIndicator() {
		return conceptIndicator;
	}

	public void setConceptIndicator(final String conceptIndicator) {
		this.conceptIndicator = conceptIndicator;
	}

	public String getConceptValidationRoutine() {
		return conceptValidationRoutine;
	}

	public void setConceptValidationRoutine(final String conceptValidationRoutine) {
		this.conceptValidationRoutine = conceptValidationRoutine;
	}

	public String getEnterpriseAgreementOwner() {
		return enterpriseAgreementOwner;
	}

	public void setEnterpriseAgreementOwner(final String enterpriseAgreementOwner) {
		this.enterpriseAgreementOwner = enterpriseAgreementOwner;
	}

	public String getCreditCardIndicator() {
		return creditCardIndicator;
	}

	public void setCreditCardIndicator(final String creditCardIndicator) {
		this.creditCardIndicator = creditCardIndicator;
	}

	public DtoCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(final DtoCurrency currency) {
		this.currency = currency;
	}

	public String getFormalName() {
		return formalName;
	}

	public void setFormalName(final String formalName) {
		this.formalName = formalName;
	}

	public String getHelpImageUrl() {
		return helpImageUrl;
	}

	public void setHelpImageUrl(final String helpImageUrl) {
		this.helpImageUrl = helpImageUrl;
	}

	public String getHelpPaymentType() {
		return helpPaymentType;
	}

	public void setHelpPaymentType(final String helpPaymentType) {
		this.helpPaymentType = helpPaymentType;
	}

	public String getHelpReference() {
		return helpReference;
	}

	public void setHelpReference(final String helpReference) {
		this.helpReference = helpReference;
	}

	public String getHelpConcept() {
		return helpConcept;
	}

	public void setHelpConcept(final String helpConcept) {
		this.helpConcept = helpConcept;
	}

	public String getHelpThirdMessage() {
		return helpThirdMessage;
	}

	public void setHelpThirdMessage(final String helpThirdMessage) {
		this.helpThirdMessage = helpThirdMessage;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(final String authorization) {
		this.authorization = authorization;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getImmediatePaymentIndicator() {
		return immediatePaymentIndicator;
	}

	public void setImmediatePaymentIndicator(final String immediatePaymentIndicator) {
		this.immediatePaymentIndicator = immediatePaymentIndicator;
	}

	public DtoBusinessLine getBusinessLine() {
		return businessLine;
	}

	public void setBusinessLine(final DtoBusinessLine businessLine) {
		this.businessLine = businessLine;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(final String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public Number getMaxChecksLimit() {
		return maxChecksLimit;
	}

	public void setMaxChecksLimit(final Number maxChecksLimit) {
		this.maxChecksLimit = maxChecksLimit;
	}

	public Number getMaxConceptLength() {
		return maxConceptLength;
	}

	public void setMaxConceptLength(final Number maxConceptLength) {
		this.maxConceptLength = maxConceptLength;
	}

	public Number getMaxReferenceLength() {
		return maxReferenceLength;
	}

	public void setMaxReferenceLength(final Number maxReferenceLength) {
		this.maxReferenceLength = maxReferenceLength;
	}

	public Number getMinConceptLength() {
		return minConceptLength;
	}

	public void setMinConceptLength(final Number minConceptLength) {
		this.minConceptLength = minConceptLength;
	}

	public Number getMinReferenceLength() {
		return minReferenceLength;
	}

	public void setMinReferenceLength(final Number minReferenceLength) {
		this.minReferenceLength = minReferenceLength;
	}

	public String getOnlinePaymentIndicator() {
		return onlinePaymentIndicator;
	}

	public void setOnlinePaymentIndicator(final String onlinePaymentIndicator) {
		this.onlinePaymentIndicator = onlinePaymentIndicator;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(final String paymentType) {
		this.paymentType = paymentType;
	}

	public String getReferenceFormat() {
		return referenceFormat;
	}

	public void setReferenceFormat(final String referenceFormat) {
		this.referenceFormat = referenceFormat;
	}

	public String getReferenceIndicator() {
		return referenceIndicator;
	}

	public void setReferenceIndicator(final String referenceIndicator) {
		this.referenceIndicator = referenceIndicator;
	}

	public String getReferenceValidationRoutine() {
		return referenceValidationRoutine;
	}

	public void setReferenceValidationRoutine(final String referenceValidationRoutine) {
		this.referenceValidationRoutine = referenceValidationRoutine;
	}

	public DtoRegion getRegion() {
		return region;
	}

	public void setRegion(final DtoRegion region) {
		this.region = region;
	}

	public String getRemittanceIndicator() {
		return remittanceIndicator;
	}

	public void setRemittanceIndicator(final String remittanceIndicator) {
		this.remittanceIndicator = remittanceIndicator;
	}

	public String getRupCode() {
		return rupCode;
	}

	public void setRupCode(final String rupCode) {
		this.rupCode = rupCode;
	}

	public String getRupIndicator() {
		return rupIndicator;
	}

	public void setRupIndicator(final String rupIndicator) {
		this.rupIndicator = rupIndicator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getServiceIndicator() {
		return serviceIndicator;
	}

	public void setServiceIndicator(final String serviceIndicator) {
		this.serviceIndicator = serviceIndicator;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getIssuingCompany() {
		return issuingCompany;
	}

	public void setIssuingCompany(final String issuingCompany) {
		this.issuingCompany = issuingCompany;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(final String theme) {
		this.theme = theme;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoEnterpriseAgreement)) {
			return false;
		}
		final DtoEnterpriseAgreement castOther = (DtoEnterpriseAgreement) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(cashIndicator, castOther.cashIndicator)
				.append(city, castOther.city).append(clasification, castOther.clasification)
				.append(commercialName, castOther.commercialName).append(conceptFormat, castOther.conceptFormat)
				.append(conceptIndicator, castOther.conceptIndicator).append(conceptValidationRoutine, castOther.conceptValidationRoutine)
				.append(enterpriseAgreementOwner, castOther.enterpriseAgreementOwner)
				.append(creditCardIndicator, castOther.creditCardIndicator).append(currency, castOther.currency)
				.append(formalName, castOther.formalName).append(helpImageUrl, castOther.helpImageUrl)
				.append(helpPaymentType, castOther.helpPaymentType).append(helpReference, castOther.helpReference)
				.append(helpConcept, castOther.helpConcept).append(helpThirdMessage, castOther.helpThirdMessage)
				.append(authorization, castOther.authorization).append(id, castOther.id)
				.append(immediatePaymentIndicator, castOther.immediatePaymentIndicator).append(businessLine, castOther.businessLine)
				.append(logoUrl, castOther.logoUrl).append(maxChecksLimit, castOther.maxChecksLimit)
				.append(maxConceptLength, castOther.maxConceptLength).append(maxReferenceLength, castOther.maxReferenceLength)
				.append(minConceptLength, castOther.minConceptLength).append(minReferenceLength, castOther.minReferenceLength)
				.append(onlinePaymentIndicator, castOther.onlinePaymentIndicator).append(paymentType, castOther.paymentType)
				.append(referenceFormat, castOther.referenceFormat).append(referenceIndicator, castOther.referenceIndicator)
				.append(referenceValidationRoutine, castOther.referenceValidationRoutine).append(region, castOther.region)
				.append(remittanceIndicator, castOther.remittanceIndicator).append(rupCode, castOther.rupCode)
				.append(rupIndicator, castOther.rupIndicator).append(status, castOther.status)
				.append(serviceIndicator, castOther.serviceIndicator).append(shortName, castOther.shortName)
				.append(issuingCompany, castOther.issuingCompany).append(theme, castOther.theme).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(cashIndicator).append(city).append(clasification).append(commercialName)
				.append(conceptFormat).append(conceptIndicator).append(conceptValidationRoutine).append(enterpriseAgreementOwner)
				.append(creditCardIndicator).append(currency).append(formalName).append(helpImageUrl).append(helpPaymentType)
				.append(helpReference).append(helpConcept).append(helpThirdMessage).append(authorization).append(id)
				.append(immediatePaymentIndicator).append(businessLine).append(logoUrl).append(maxChecksLimit).append(maxConceptLength)
				.append(maxReferenceLength).append(minConceptLength).append(minReferenceLength).append(onlinePaymentIndicator)
				.append(paymentType).append(referenceFormat).append(referenceIndicator).append(referenceValidationRoutine).append(region)
				.append(remittanceIndicator).append(rupCode).append(rupIndicator).append(status).append(serviceIndicator).append(shortName)
				.append(issuingCompany).append(theme).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("cashIndicator", cashIndicator).append("city", city)
				.append("clasification", clasification).append("commercialName", commercialName).append("conceptFormat", conceptFormat)
				.append("conceptIndicator", conceptIndicator).append("conceptValidationRoutine", conceptValidationRoutine)
				.append("enterpriseAgreementOwner", enterpriseAgreementOwner).append("creditCardIndicator", creditCardIndicator)
				.append("currency", currency).append("formalName", formalName).append("helpImageUrl", helpImageUrl)
				.append("helpPaymentType", helpPaymentType).append("helpReference", helpReference).append("helpConcept", helpConcept)
				.append("helpThirdMessage", helpThirdMessage).append("authorization", authorization).append("id", id)
				.append("immediatePaymentIndicator", immediatePaymentIndicator).append("businessLine", businessLine)
				.append("logoUrl", logoUrl).append("maxChecksLimit", maxChecksLimit).append("maxConceptLength", maxConceptLength)
				.append("maxReferenceLength", maxReferenceLength).append("minConceptLength", minConceptLength)
				.append("minReferenceLength", minReferenceLength).append("onlinePaymentIndicator", onlinePaymentIndicator)
				.append("paymentType", paymentType).append("referenceFormat", referenceFormat)
				.append("referenceIndicator", referenceIndicator).append("referenceValidationRoutine", referenceValidationRoutine)
				.append("region", region).append("remittanceIndicator", remittanceIndicator).append("rupCode", rupCode)
				.append("rupIndicator", rupIndicator).append("status", status).append("serviceIndicator", serviceIndicator)
				.append("shortName", shortName).append("issuingCompany", issuingCompany).append("theme", theme).toString();
	}

}
