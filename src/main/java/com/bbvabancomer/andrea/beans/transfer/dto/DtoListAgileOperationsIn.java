package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbvabancomer.andrea.beans.transfer.enums.EnumAgileOperationType;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "listAgileOperationsIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "listAgileOperationsIn", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoListAgileOperationsIn implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "pagination", value = "Pagination", required = true)
	@NotNull
	private DtoPagination pagination;

	@ApiParam(name = "agileOperationType", value = "Agile Operation Type", required = true)
	@NotNull
	private EnumAgileOperationType agileOperationType;

	@ApiParam(name = "codigoFuncion", value = "Codigo Funcion", required = true)
	@NotNull
	private String codigoFuncion;

	public DtoPagination getPagination() {
		return pagination;
	}

	public void setPagination(final DtoPagination pagination) {
		this.pagination = pagination;
	}

	public EnumAgileOperationType getAgileOperationType() {
		return agileOperationType;
	}

	public void setAgileOperationType(final EnumAgileOperationType agileOperationType) {
		this.agileOperationType = agileOperationType;
	}

	public String getCodigoFuncion() {
		return codigoFuncion;
	}

	public void setCodigoFuncion(final String codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}

}
