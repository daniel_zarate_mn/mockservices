package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "bank", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "bank", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoBank implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "address", value = "Address", required = true)
	@NotNull
	private DtoAddress address;

	@ApiParam(name = "bicCode", value = "BicCode", required = true)
	@NotNull
	private String bicCode;

	@ApiParam(name = "corporate", value = "Corporate", required = true)
	@NotNull
	private String corporate;

	@ApiParam(name = "country", value = "Country", required = true)
	@NotNull
	private DtoCountry country;

	@ApiParam(name = "group", value = "Group", required = true)
	@NotNull
	private String group;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "name", value = "Name", required = true)
	@NotNull
	private String name;

	@ApiParam(name = "financialEntityId", value = "Financial Entity Id", required = true)
	@NotNull
	private String financialEntityId;

	@ApiParam(name = "abaCode", value = "AbaCode", required = true)
	@NotNull
	private String abaCode;

	public DtoAddress getAddress() {
		return address;
	}

	public void setAddress(final DtoAddress address) {
		this.address = address;
	}

	public String getBicCode() {
		return bicCode;
	}

	public void setBicCode(final String bicCode) {
		this.bicCode = bicCode;
	}

	public String getCorporate() {
		return corporate;
	}

	public void setCorporate(final String corporate) {
		this.corporate = corporate;
	}

	public DtoCountry getCountry() {
		return country;
	}

	public void setCountry(final DtoCountry country) {
		this.country = country;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(final String group) {
		this.group = group;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getFinancialEntityId() {
		return financialEntityId;
	}

	public void setFinancialEntityId(final String financialEntityId) {
		this.financialEntityId = financialEntityId;
	}

	public String getAbaCode() {
		return abaCode;
	}

	public void setAbaCode(final String abaCode) {
		this.abaCode = abaCode;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoBank)) {
			return false;
		}
		final DtoBank castOther = (DtoBank) other;
		return new EqualsBuilder().append(address, castOther.address).append(bicCode, castOther.bicCode)
				.append(corporate, castOther.corporate).append(country, castOther.country).append(group, castOther.group)
				.append(id, castOther.id).append(name, castOther.name).append(financialEntityId, castOther.financialEntityId)
				.append(abaCode, castOther.abaCode).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(address).append(bicCode).append(corporate).append(country).append(group).append(id)
				.append(name).append(financialEntityId).append(abaCode).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("address", address).append("bicCode", bicCode).append("corporate", corporate)
				.append("country", country).append("group", group).append("id", id).append("name", name)
				.append("financialEntityId", financialEntityId).append("abaCode", abaCode).toString();
	}

}
