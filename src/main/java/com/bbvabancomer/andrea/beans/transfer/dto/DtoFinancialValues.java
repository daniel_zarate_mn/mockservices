package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "financialValues", namespace = "http://bbva.com/zic/agileoperations/V01")
@XmlType(name = "financialValues", namespace = "http://bbva.com/zic/agileoperations/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoFinancialValues implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "exchangeValue", value = "Exchange Value", required = true)
	@NotNull
	private DtoMoney exchangeValue;

	@ApiParam(name = "value", value = "Value", required = true)
	@NotNull
	private DtoMoney value;

	public DtoMoney getValue() {
		return value;
	}

	public void setValue(final DtoMoney value) {
		this.value = value;
	}

	public DtoMoney getExchangeValue() {
		return exchangeValue;
	}

	public void setExchangeValue(final DtoMoney exchangeValue) {
		this.exchangeValue = exchangeValue;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoFinancialValues)) {
			return false;
		}
		final DtoFinancialValues castOther = (DtoFinancialValues) other;
		return new EqualsBuilder().append(exchangeValue, castOther.exchangeValue).append(value, castOther.value).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(exchangeValue).append(value).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("exchangeValue", exchangeValue).append("value", value).toString();
	}

}
