package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.ILoan;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "personalLoan", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "personalLoan", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoPersonalLoan implements ILoan, Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "productBase", value = "Product Base", required = true)
	@NotNull
	private DtoProductBase productBase;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "accountNumber", value = "Account Number", required = true)
	@NotNull
	private String accountNumber;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	public String getId() {
		return id;
	}

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoPersonalLoan)) {
			return false;
		}
		final DtoPersonalLoan castOther = (DtoPersonalLoan) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(accountNumber, castOther.accountNumber).append(shortName, castOther.shortName).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(accountNumber).append(shortName).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("accountNumber", accountNumber)
				.append("shortName", shortName).toString();
	}

}
