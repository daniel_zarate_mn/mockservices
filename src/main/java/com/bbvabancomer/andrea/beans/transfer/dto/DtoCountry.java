package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "country", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "country", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoCountry implements Serializable{

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "acronym", value = "Acronym", required = true)
	@NotNull
	private String acronym;

	@ApiParam(name = "name", value = "Name", required = true)
	@NotNull
	private String name;

	public String getId() {

		return id;
	}

	public void setId(final String id) {

		this.id = id;
	}

	public String getAcronym() {

		return acronym;
	}

	public void setAcronym(final String acronym) {

		this.acronym = acronym;
	}

	public String getName() {

		return name;
	}

	public void setName(final String name) {

		this.name = name;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoCountry)) {
			return false;
		}
		final DtoCountry castOther = (DtoCountry) other;
		return new EqualsBuilder().append(id, castOther.id).append(acronym, castOther.acronym).append(name, castOther.name).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(acronym).append(name).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("acronym", acronym).append("name", name).toString();
	}

}
