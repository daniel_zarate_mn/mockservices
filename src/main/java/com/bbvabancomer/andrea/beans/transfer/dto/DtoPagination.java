package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * @author adesis
 *
 */
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "pagination", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "pagination", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoPagination implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "nextPaginationKey", value = "Next Pagination Key", required = true)
	@NotNull
	private String nextPaginationKey;

	@ApiParam(name = "previousPaginationKey", value = "Previous Pagination Key", required = true)
	@NotNull
	private String previousPaginationKey;

	public String getNextPaginationKey() {
		return nextPaginationKey;
	}

	public void setNextPaginationKey(final String nextPaginationKey) {
		this.nextPaginationKey = nextPaginationKey;
	}

	public String getPreviousPaginationKey() {
		return previousPaginationKey;
	}

	public void setPreviousPaginationKey(final String previousPaginationKey) {
		this.previousPaginationKey = previousPaginationKey;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoPagination)) {
			return false;
		}
		final DtoPagination castOther = (DtoPagination) other;
		return new EqualsBuilder().append(nextPaginationKey, castOther.nextPaginationKey)
				.append(previousPaginationKey, castOther.previousPaginationKey).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(nextPaginationKey).append(previousPaginationKey).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("nextPaginationKey", nextPaginationKey)
				.append("previousPaginationKey", previousPaginationKey).toString();
	}

}
