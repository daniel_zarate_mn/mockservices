package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.enums.EnumBusinessFlow;
import com.bbvabancomer.andrea.beans.transfer.enums.EnumOrigin;
import com.bbvabancomer.andrea.beans.transfer.enums.EnumTransferStatus;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "Transfer", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "transfer", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoTransfer implements Serializable{

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "operationDate", value = "Operation Date", required = true)
	@NotNull
	private Date operationDate;

	@ApiParam(name = "concept", value = "Concept", required = true)
	@NotNull
	private List<String> concept;

	@ApiParam(name = "commission", value = "Commission", required = true)
	@NotNull
	private DtoCommision commission;

	@ApiParam(name = "expirationDate", value = "Expiration Date", required = true)
	@NotNull
	private Date expirationDate;

	@ApiParam(name = "observations", value = "Observations", required = true)
	@NotNull
	private String observations;

	@ApiParam(name = "reference", value = "Reference", required = true)
	@NotNull
	private String reference;

	@ApiParam(name = "numericReference", value = "Numeric Reference", required = true)
	@NotNull
	private String numericReference;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	@ApiParam(name = "reasonPayment", value = "Reason Payment", required = true)
	@NotNull
	private String reasonPayment;

	@ApiParam(name = "sender", value = "Sender", required = true)
	@NotNull
	private DtoSender sender;

	@ApiParam(name = "receiver", value = "Receiver", required = true)
	@NotNull
	private DtoReceiver receiver;

	@ApiParam(name = "amount", value = "Amount", required = true)
	@NotNull
	private DtoFinancialValues amount;

	@ApiParam(name = "scheduler", value = "Scheduler", required = true)
	@NotNull
	private DtoScheduler scheduler;

	@ApiParam(name = "nextDay", value = "Next Day", required = true)
	@NotNull
	private Boolean nextDay;

	@ApiParam(name = "digitalTaxCertificate", value = "Digital Tax Certificate", required = true)
	@NotNull
	private DtoDigitalTaxCertificate digitalTaxCertificate;

	@ApiParam(name = "transferStatus", value = "Transfer Status", required = true)
	@NotNull
	private EnumTransferStatus transferStatus;

	@ApiParam(name = "errorInfo", value = "Error Info", required = true)
	@NotNull
	private DtoErrorInfo errorInfo;

	@ApiParam(name = "entityId", value = "Entity Id", required = true)
	@NotNull
	private DtoEntityId entityId;

	@ApiParam(name = "refund", value = "Refund", required = true)
	@NotNull
	private DtoRefund refund;

	@ApiParam(name = "businessFlow", value = "Business Flow", required = true)
	@NotNull
	private EnumBusinessFlow businessFlow;

	@ApiParam(name = "origin", value = "Origin", required = true)
	@NotNull
	private EnumOrigin origin;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(final Date operationDate) {
		this.operationDate = operationDate;
	}

	public List<String> getConcept() {
		return concept;
	}

	public void setConcept(final List<String> concept) {
		this.concept = concept;
	}

	public DtoCommision getCommission() {
		return commission;
	}

	public void setCommission(final DtoCommision commission) {
		this.commission = commission;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(final Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(final String observations) {
		this.observations = observations;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(final String reference) {
		this.reference = reference;
	}

	public String getNumericReference() {
		return numericReference;
	}

	public void setNumericReference(final String numericReference) {
		this.numericReference = numericReference;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getReasonPayment() {
		return reasonPayment;
	}

	public void setReasonPayment(final String reasonPayment) {
		this.reasonPayment = reasonPayment;
	}

	public DtoSender getSender() {
		return sender;
	}

	public void setSender(final DtoSender sender) {
		this.sender = sender;
	}

	public DtoReceiver getReceiver() {
		return receiver;
	}

	public void setReceiver(final DtoReceiver receiver) {
		this.receiver = receiver;
	}

	public DtoFinancialValues getAmount() {
		return amount;
	}

	public void setAmount(final DtoFinancialValues amount) {
		this.amount = amount;
	}

	public DtoScheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(final DtoScheduler scheduler) {
		this.scheduler = scheduler;
	}

	public Boolean getNextDay() {
		return nextDay;
	}

	public void setNextDay(final Boolean nextDay) {
		this.nextDay = nextDay;
	}

	public DtoDigitalTaxCertificate getDigitalTaxCertificate() {
		return digitalTaxCertificate;
	}

	public void setDigitalTaxCertificate(final DtoDigitalTaxCertificate digitalTaxCertificate) {
		this.digitalTaxCertificate = digitalTaxCertificate;
	}

	public EnumTransferStatus getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(final EnumTransferStatus transferStatus) {
		this.transferStatus = transferStatus;
	}

	public DtoErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(final DtoErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public DtoEntityId getEntityId() {
		return entityId;
	}

	public void setEntityId(final DtoEntityId entityId) {
		this.entityId = entityId;
	}

	public DtoRefund getRefund() {
		return refund;
	}

	public void setRefund(final DtoRefund refund) {
		this.refund = refund;
	}

	public EnumBusinessFlow getBusinessFlow() {
		return businessFlow;
	}

	public void setBusinessFlow(final EnumBusinessFlow businessFlow) {
		this.businessFlow = businessFlow;
	}

	public EnumOrigin getOrigin() {
		return origin;
	}

	public void setOrigin(final EnumOrigin origin) {
		this.origin = origin;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoTransfer)) {
			return false;
		}
		final DtoTransfer castOther = (DtoTransfer) other;
		return new EqualsBuilder().append(id, castOther.id).append(operationDate, castOther.operationDate)
				.append(concept, castOther.concept).append(commission, castOther.commission)
				.append(expirationDate, castOther.expirationDate).append(observations, castOther.observations)
				.append(reference, castOther.reference).append(numericReference, castOther.numericReference)
				.append(shortName, castOther.shortName).append(reasonPayment, castOther.reasonPayment).append(sender, castOther.sender)
				.append(receiver, castOther.receiver).append(amount, castOther.amount).append(scheduler, castOther.scheduler)
				.append(nextDay, castOther.nextDay).append(digitalTaxCertificate, castOther.digitalTaxCertificate)
				.append(transferStatus, castOther.transferStatus).append(errorInfo, castOther.errorInfo)
				.append(entityId, castOther.entityId).append(refund, castOther.refund).append(businessFlow, castOther.businessFlow)
				.append(origin, castOther.origin).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(operationDate).append(concept).append(commission).append(expirationDate)
				.append(observations).append(reference).append(numericReference).append(shortName).append(reasonPayment).append(sender)
				.append(receiver).append(amount).append(scheduler).append(nextDay).append(digitalTaxCertificate).append(transferStatus)
				.append(errorInfo).append(entityId).append(refund).append(businessFlow).append(origin).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("operationDate", operationDate).append("concept", concept)
				.append("commission", commission).append("expirationDate", expirationDate).append("observations", observations)
				.append("reference", reference).append("numericReference", numericReference).append("shortName", shortName)
				.append("reasonPayment", reasonPayment).append("sender", sender).append("receiver", receiver).append("amount", amount)
				.append("scheduler", scheduler).append("nextDay", nextDay).append("digitalTaxCertificate", digitalTaxCertificate)
				.append("transferStatus", transferStatus).append("errorInfo", errorInfo).append("entityId", entityId)
				.append("refund", refund).append("businessFlow", businessFlow).append("origin", origin).toString();
	}

}
