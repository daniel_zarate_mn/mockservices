package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "balance", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "balance", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoBalance implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "currentBalance", value = "Current Balance", required = true)
	@NotNull
	private DtoMoney currentBalance;

	@ApiParam(name = "grantedBalance", value = "Granted Balance", required = true)
	@NotNull
	private DtoMoney grantedBalance;

	@ApiParam(name = "totalBalance", value = "TotalBalance", required = true)
	@NotNull
	private DtoMoney totalBalance;

	@ApiParam(name = "balance", value = "Balance", required = true)
	@NotNull
	private DtoMoney balance;

	@ApiParam(name = "pendingBalance", value = "Pending Balance", required = true)
	@NotNull
	private DtoMoney pendingBalance;

	@ApiParam(name = "cuttingBalance", value = "Cutting Balance", required = true)
	@NotNull
	private DtoMoney cuttingBalance;

	@ApiParam(name = "balanceAtDate", value = "Balance At Date", required = true)
	@NotNull
	private DtoMoney balanceAtDate;

	@ApiParam(name = "availableBalance", value = "Available Balance", required = true)
	@NotNull
	private DtoMoney availableBalance;

	public DtoMoney getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(final DtoMoney currentBalance) {
		this.currentBalance = currentBalance;
	}

	public DtoMoney getGrantedBalance() {
		return grantedBalance;
	}

	public void setGrantedBalance(final DtoMoney grantedBalance) {
		this.grantedBalance = grantedBalance;
	}

	public DtoMoney getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(final DtoMoney totalBalance) {
		this.totalBalance = totalBalance;
	}

	public DtoMoney getBalance() {
		return balance;
	}

	public void setBalance(final DtoMoney balance) {
		this.balance = balance;
	}

	public DtoMoney getPendingBalance() {
		return pendingBalance;
	}

	public void setPendingBalance(final DtoMoney pendingBalance) {
		this.pendingBalance = pendingBalance;
	}

	public DtoMoney getCuttingBalance() {
		return cuttingBalance;
	}

	public void setCuttingBalance(final DtoMoney cuttingBalance) {
		this.cuttingBalance = cuttingBalance;
	}

	public DtoMoney getBalanceAtDate() {
		return balanceAtDate;
	}

	public void setBalanceAtDate(final DtoMoney balanceAtDate) {
		this.balanceAtDate = balanceAtDate;
	}

	public DtoMoney getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(final DtoMoney availableBalance) {
		this.availableBalance = availableBalance;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoBalance)) {
			return false;
		}
		final DtoBalance castOther = (DtoBalance) other;
		return new EqualsBuilder().append(currentBalance, castOther.currentBalance).append(grantedBalance, castOther.grantedBalance)
				.append(totalBalance, castOther.totalBalance).append(balance, castOther.balance)
				.append(pendingBalance, castOther.pendingBalance).append(cuttingBalance, castOther.cuttingBalance)
				.append(balanceAtDate, castOther.balanceAtDate).append(availableBalance, castOther.availableBalance).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(currentBalance).append(grantedBalance).append(totalBalance).append(balance)
				.append(pendingBalance).append(cuttingBalance).append(balanceAtDate).append(availableBalance).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("currentBalance", currentBalance).append("grantedBalance", grantedBalance)
				.append("totalBalance", totalBalance).append("balance", balance).append("pendingBalance", pendingBalance)
				.append("cuttingBalance", cuttingBalance).append("balanceAtDate", balanceAtDate)
				.append("availableBalance", availableBalance).toString();
	}

}
