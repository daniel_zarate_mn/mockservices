package com.bbvabancomer.andrea.beans.transfer.enums;

public enum EnumBusinessFlow {

	INTERNATIONAL, INTERBANK, MOBILEMONEY, THIRD_PARTY, MY_ACCOUNTS, SERVICEPAYMENT, EXPRESS, PREPAID;

}
