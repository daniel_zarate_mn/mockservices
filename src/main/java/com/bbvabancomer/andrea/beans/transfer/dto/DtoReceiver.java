package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "receiver", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "receiver", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoReceiver implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "address", value = "Address", required = true)
	@NotNull
	private DtoAddress address;

	@ApiParam(name = "bank", value = "Bank", required = true)
	@NotNull
	private DtoBank bank;

	@ApiParam(name = "contract", value = "Contract", required = true)
	@NotNull
	private DtoContract contract;

	@ApiParam(name = "telephone", value = "Telephone", required = true)
	@NotNull
	private DtoTelephone telephone;

	@ApiParam(name = "email", value = "Email", required = true)
	@NotNull
	private String email;

	@ApiParam(name = "name", value = "Name", required = true)
	@NotNull
	private String name;

	public DtoAddress getAddress() {
		return address;
	}

	public void setAddress(final DtoAddress address) {
		this.address = address;
	}

	public DtoBank getBank() {
		return bank;
	}

	public void setBank(final DtoBank bank) {
		this.bank = bank;
	}

	public DtoContract getContract() {
		return contract;
	}

	public void setContract(final DtoContract contract) {
		this.contract = contract;
	}

	public DtoTelephone getTelephone() {
		return telephone;
	}

	public void setTelephone(final DtoTelephone telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoReceiver)) {
			return false;
		}
		final DtoReceiver castOther = (DtoReceiver) other;
		return new EqualsBuilder().append(address, castOther.address).append(bank, castOther.bank).append(contract, castOther.contract)
				.append(telephone, castOther.telephone).append(email, castOther.email).append(name, castOther.name).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(address).append(bank).append(contract).append(telephone).append(email).append(name)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("address", address).append("bank", bank).append("contract", contract)
				.append("telephone", telephone).append("email", email).append("name", name).toString();
	}

}
