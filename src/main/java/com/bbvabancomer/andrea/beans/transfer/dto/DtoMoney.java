package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "money", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "money", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoMoney implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "currency", value = "Currency", required = true)
	@NotNull
	private DtoCurrency currency;

	@ApiParam(name = "amount", value = "Amount", required = true)
	@NotNull
	private Double amount;

	public DtoCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(final DtoCurrency currency) {
		this.currency = currency;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(final Double amount) {
		this.amount = amount;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoMoney)) {
			return false;
		}
		final DtoMoney castOther = (DtoMoney) other;
		return new EqualsBuilder().append(currency, castOther.currency).append(amount, castOther.amount).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(currency).append(amount).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("currency", currency).append("amount", amount).toString();
	}

}
