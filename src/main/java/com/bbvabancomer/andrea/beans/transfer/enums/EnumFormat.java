package com.bbvabancomer.andrea.beans.transfer.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnumFormat {

	CHECKACCOUNT("X"), CREDITCARD("X"), EXPRESSACCOUNT("X"), NO_VALUE("");

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumFormat.class);

	private String key;

	EnumFormat(final String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static EnumFormat getEnumValue(final String key) {
		for (final EnumFormat format : values()) {
			if (format.key.equals(key)) {
				return format;
			}
		}
		LOGGER.warn("Enum no encontrado para el code [" + key + "]");
		return NO_VALUE;
	}

	public static String getName(final String key) {

		for (final EnumFormat format : values()) {
			if (format.name().equalsIgnoreCase(key)) {
				return format.name();
			}
		}

		LOGGER.warn("Enum no encontrado para el name [" + key + "]");
		return NO_VALUE.getKey();
	}

}
