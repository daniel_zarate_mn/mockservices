package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "commision", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "commision", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoCommision implements Serializable{

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "amount", value = "Amount", required = true)
	@NotNull
	private DtoMoney amount;

	@ApiParam(name = "creditLineCommWithdrawalCash", value = "Credit Line Comm With Drawal Cash", required = true)
	@NotNull
	private String creditLineCommWithdrawalCash;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public DtoMoney getAmount() {
		return amount;
	}

	public void setAmount(final DtoMoney amount) {
		this.amount = amount;
	}

	public String getCreditLineCommWithdrawalCash() {
		return creditLineCommWithdrawalCash;
	}

	public void setCreditLineCommWithdrawalCash(final String creditLineCommWithdrawalCash) {
		this.creditLineCommWithdrawalCash = creditLineCommWithdrawalCash;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoCommision)) {
			return false;
		}
		final DtoCommision castOther = (DtoCommision) other;
		return new EqualsBuilder().append(id, castOther.id).append(amount, castOther.amount)
				.append(creditLineCommWithdrawalCash, castOther.creditLineCommWithdrawalCash).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(amount).append(creditLineCommWithdrawalCash).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("amount", amount)
				.append("creditLineCommWithdrawalCash", creditLineCommWithdrawalCash).toString();
	}

}
