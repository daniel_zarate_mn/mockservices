package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "agencyAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "agencyAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAgencyAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "accountNumber", value = "Account Number", required = true)
	@NotNull
	private String accountNumber;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	@ApiParam(name = "indicatorJointAccount", value = "Indicator Joint Account", required = true)
	@NotNull
	private String indicatorJointAccount;

	@ApiParam(name = "isAditionalAccount", value = "Is Aditional Account", required = true)
	@NotNull
	private Boolean isAditionalAccount;

	@ApiParam(name = "region", value = "Region", required = true)
	@NotNull
	private DtoRegion region;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getIndicatorJointAccount() {
		return indicatorJointAccount;
	}

	public void setIndicatorJointAccount(final String indicatorJointAccount) {
		this.indicatorJointAccount = indicatorJointAccount;
	}

	public Boolean getIsAditionalAccount() {
		return isAditionalAccount;
	}

	public void setIsAditionalAccount(final Boolean isAditionalAccount) {
		this.isAditionalAccount = isAditionalAccount;
	}

	public DtoRegion getRegion() {
		return region;
	}

	public void setRegion(final DtoRegion region) {
		this.region = region;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoAgencyAccount)) {
			return false;
		}
		final DtoAgencyAccount castOther = (DtoAgencyAccount) other;
		return new EqualsBuilder().append(id, castOther.id).append(accountNumber, castOther.accountNumber)
				.append(shortName, castOther.shortName).append(indicatorJointAccount, castOther.indicatorJointAccount)
				.append(isAditionalAccount, castOther.isAditionalAccount).append(region, castOther.region).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(accountNumber).append(shortName).append(indicatorJointAccount)
				.append(isAditionalAccount).append(region).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("accountNumber", accountNumber).append("shortName", shortName)
				.append("indicatorJointAccount", indicatorJointAccount).append("isAditionalAccount", isAditionalAccount)
				.append("region", region).toString();
	}

}
