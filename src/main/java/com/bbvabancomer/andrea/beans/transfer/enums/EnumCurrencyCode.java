package com.bbvabancomer.andrea.beans.transfer.enums;

public enum EnumCurrencyCode {

	MXP, USD, EUR

}
