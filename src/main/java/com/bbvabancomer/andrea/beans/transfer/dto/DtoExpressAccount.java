package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.IAccount;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "expressAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "expressAccount", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoExpressAccount implements IAccount, Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "productBase", value = "Product Base", required = true)
	@NotNull
	private DtoProductBase productBase;

	@ApiParam(name = "id", value = "Id", required = true)
	@NotNull
	private String id;

	@ApiParam(name = "shortName", value = "Short Name", required = true)
	@NotNull
	private String shortName;

	@ApiParam(name = "cellphoneNumber", value = "Cellphone Number", required = true)
	@NotNull
	private String cellphoneNumber;

	@ApiParam(name = "cardNumber", value = "Card Number", required = true)
	@NotNull
	private String cardNumber;

	@ApiParam(name = "cellphoneCompany", value = "Cellphone Company", required = true)
	@NotNull
	private DtoCellphoneCompany cellphoneCompany;

	public DtoProductBase getProductBase() {
		return productBase;
	}

	public void setProductBase(final DtoProductBase productBase) {
		this.productBase = productBase;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getCellphoneNumber() {
		return cellphoneNumber;
	}

	public void setCellphoneNumber(final String cellphoneNumber) {
		this.cellphoneNumber = cellphoneNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public DtoCellphoneCompany getCellphoneCompany() {
		return cellphoneCompany;
	}

	public void setCellphoneCompany(final DtoCellphoneCompany cellphoneCompany) {
		this.cellphoneCompany = cellphoneCompany;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoExpressAccount)) {
			return false;
		}
		final DtoExpressAccount castOther = (DtoExpressAccount) other;
		return new EqualsBuilder().append(productBase, castOther.productBase).append(id, castOther.id)
				.append(shortName, castOther.shortName).append(cellphoneNumber, castOther.cellphoneNumber)
				.append(cardNumber, castOther.cardNumber).append(cellphoneCompany, castOther.cellphoneCompany).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(productBase).append(id).append(shortName).append(cellphoneNumber).append(cardNumber)
				.append(cellphoneCompany).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productBase", productBase).append("id", id).append("shortName", shortName)
				.append("cellphoneNumber", cellphoneNumber).append("cardNumber", cardNumber).append("cellphoneCompany", cellphoneCompany)
				.toString();
	}

}
