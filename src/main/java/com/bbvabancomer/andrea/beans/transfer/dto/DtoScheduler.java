package com.bbvabancomer.andrea.beans.transfer.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.bbvabancomer.andrea.beans.transfer.enums.EnumIterationDay;
import com.bbvabancomer.andrea.beans.transfer.enums.EnumLastModifyStatus;
import com.bbvabancomer.andrea.beans.transfer.enums.EnumSchedulerType;
import com.bbvabancomer.andrea.beans.transfer.enums.EnumTimeUnit;
import com.wordnik.swagger.annotations.ApiParam;

@XmlRootElement(name = "scheduler", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlType(name = "scheduler", namespace = "http://bbva.com/zic/mobiletransfers/V01")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoScheduler implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(name = "initDate", value = "Init Date", required = true)
	@NotNull
	private Date initDate;

	@ApiParam(name = "endDate", value = "End Date", required = true)
	@NotNull
	private Date endDate;

	@ApiParam(name = "timeUnit", value = "Time Unit", required = true)
	@NotNull
	private EnumTimeUnit timeUnit;

	@ApiParam(name = "iterationNumber", value = "Iteration Number", required = true)
	@NotNull
	private Integer iterationNumber;

	@ApiParam(name = "iterationDay", value = "Iteration Day", required = true)
	@NotNull
	private EnumIterationDay iterationDay;

	@ApiParam(name = "executionDate", value = "Execution Date", required = true)
	@NotNull
	private Date executionDate;

	@ApiParam(name = "expirationDate", value = "Expiration Date", required = true)
	@NotNull
	private Date expirationDate;

	@ApiParam(name = "lastOperationDate", value = "Last Operation Date", required = true)
	@NotNull
	private Date lastOperationDate;

	@ApiParam(name = "applicationDate", value = "Application Date", required = true)
	@NotNull
	private Date applicationDate;

	@ApiParam(name = "liquidationDate", value = "Liquidation Date", required = true)
	@NotNull
	private Date liquidationDate;

	@ApiParam(name = "returnDate", value = "Return Date", required = true)
	@NotNull
	private Date returnDate;

	@ApiParam(name = "sendTime", value = "Send Time", required = true)
	@NotNull
	private Date sendTime;

	@ApiParam(name = "approvalTime", value = "Approval Time", required = true)
	@NotNull
	private Date approvalTime;

	@ApiParam(name = "registerTime", value = "Register Time", required = true)
	@NotNull
	private Date registerTime;

	@ApiParam(name = "notificationTime", value = "Notification Time", required = true)
	@NotNull
	private Date notificationTime;

	@ApiParam(name = "type", value = "Type", required = true)
	@NotNull
	private EnumSchedulerType type;

	@ApiParam(name = "lastModifyDate", value = "Last Modify Date", required = true)
	@NotNull
	private Date lastModifyDate;

	@ApiParam(name = "lastModifyStatus", value = "Last Modify Status", required = true)
	@NotNull
	private EnumLastModifyStatus lastModifyStatus;

	public Date getInitDate() {

		return initDate;
	}

	public void setInitDate(final Date initDate) {

		this.initDate = initDate;
	}

	public Date getEndDate() {

		return endDate;
	}

	public void setEndDate(final Date endDate) {

		this.endDate = endDate;
	}

	public EnumTimeUnit getTimeUnit() {

		return timeUnit;
	}

	public void setTimeUnit(final EnumTimeUnit timeUnit) {

		this.timeUnit = timeUnit;
	}

	public Integer getIterationNumber() {

		return iterationNumber;
	}

	public void setIterationNumber(final Integer iterationNumber) {

		this.iterationNumber = iterationNumber;
	}

	public EnumIterationDay getIterationDay() {

		return iterationDay;
	}

	public void setIterationDay(final EnumIterationDay iterationDay) {

		this.iterationDay = iterationDay;
	}

	public Date getExecutionDate() {

		return executionDate;
	}

	public void setExecutionDate(final Date executionDate) {

		this.executionDate = executionDate;
	}

	public Date getExpirationDate() {

		return expirationDate;
	}

	public void setExpirationDate(final Date expirationDate) {

		this.expirationDate = expirationDate;
	}

	public Date getLastOperationDate() {

		return lastOperationDate;
	}

	public void setLastOperationDate(final Date lastOperationDate) {

		this.lastOperationDate = lastOperationDate;
	}

	public Date getApplicationDate() {

		return applicationDate;
	}

	public void setApplicationDate(final Date applicationDate) {

		this.applicationDate = applicationDate;
	}

	public Date getLiquidationDate() {

		return liquidationDate;
	}

	public void setLiquidationDate(final Date liquidationDate) {

		this.liquidationDate = liquidationDate;
	}

	public Date getReturnDate() {

		return returnDate;
	}

	public void setReturnDate(final Date returnDate) {

		this.returnDate = returnDate;
	}

	public Date getSendTime() {

		return sendTime;
	}

	public void setSendTime(final Date sendTime) {

		this.sendTime = sendTime;
	}

	public Date getApprovalTime() {

		return approvalTime;
	}

	public void setApprovalTime(final Date approvalTime) {

		this.approvalTime = approvalTime;
	}

	public Date getRegisterTime() {

		return registerTime;
	}

	public void setRegisterTime(final Date registerTime) {

		this.registerTime = registerTime;
	}

	public Date getNotificationTime() {

		return notificationTime;
	}

	public void setNotificationTime(final Date notificationTime) {

		this.notificationTime = notificationTime;
	}

	public EnumSchedulerType getType() {

		return type;
	}

	public void setType(final EnumSchedulerType type) {

		this.type = type;
	}

	public Date getLastModifyDate() {

		return lastModifyDate;
	}

	public void setLastModifyDate(final Date lastModifyDate) {

		this.lastModifyDate = lastModifyDate;
	}

	public EnumLastModifyStatus getLastModifyStatus() {

		return lastModifyStatus;
	}

	public void setLastModifyStatus(final EnumLastModifyStatus lastModifyStatus) {

		this.lastModifyStatus = lastModifyStatus;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DtoScheduler)) {
			return false;
		}
		final DtoScheduler castOther = (DtoScheduler) other;
		return new EqualsBuilder().append(initDate, castOther.initDate).append(endDate, castOther.endDate)
				.append(timeUnit, castOther.timeUnit).append(iterationNumber, castOther.iterationNumber)
				.append(iterationDay, castOther.iterationDay).append(executionDate, castOther.executionDate)
				.append(expirationDate, castOther.expirationDate).append(lastOperationDate, castOther.lastOperationDate)
				.append(applicationDate, castOther.applicationDate).append(liquidationDate, castOther.liquidationDate)
				.append(returnDate, castOther.returnDate).append(sendTime, castOther.sendTime).append(approvalTime, castOther.approvalTime)
				.append(registerTime, castOther.registerTime).append(notificationTime, castOther.notificationTime)
				.append(type, castOther.type).append(lastModifyDate, castOther.lastModifyDate)
				.append(lastModifyStatus, castOther.lastModifyStatus).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(initDate).append(endDate).append(timeUnit).append(iterationNumber).append(iterationDay)
				.append(executionDate).append(expirationDate).append(lastOperationDate).append(applicationDate).append(liquidationDate)
				.append(returnDate).append(sendTime).append(approvalTime).append(registerTime).append(notificationTime).append(type)
				.append(lastModifyDate).append(lastModifyStatus).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("initDate", initDate).append("endDate", endDate).append("timeUnit", timeUnit)
				.append("iterationNumber", iterationNumber).append("iterationDay", iterationDay).append("executionDate", executionDate)
				.append("expirationDate", expirationDate).append("lastOperationDate", lastOperationDate)
				.append("applicationDate", applicationDate).append("liquidationDate", liquidationDate).append("returnDate", returnDate)
				.append("sendTime", sendTime).append("approvalTime", approvalTime).append("registerTime", registerTime)
				.append("notificationTime", notificationTime).append("type", type).append("lastModifyDate", lastModifyDate)
				.append("lastModifyStatus", lastModifyStatus).toString();
	}

}
